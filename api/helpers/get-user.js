var _ = require('lodash');

module.exports = {


  friendlyName: 'Get User by either ABF or RDB number',


  description: 'Gets a user by either their ABF number, or their RDB number',


  inputs: {
    uniqueNum: {
      type: 'string',
      description: 'The ABF or RDB number of the user',
      required: false,
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    if(inputs.uniqueNum.length <= 0){
      return exits.success({result: false});
    }

    var userRDB = await User.findOne({where: {rdbNumber: inputs.uniqueNum}});

    if(!userRDB){
      if(isNaN(inputs.uniqueNum)){
        return exits.success({result: false});
      }

      //Try the ABF
      var abf = await Abf.findOne({uniqueNumber: inputs.uniqueNum}).populate('owner');

      if(!abf){
        return exits.success({result: false});
      }

      userRDB = abf.owner;
    }
    delete(userRDB.emailAddress);
    delete(userRDB.password);

    return exits.success({result: true, user: userRDB});
  }


};

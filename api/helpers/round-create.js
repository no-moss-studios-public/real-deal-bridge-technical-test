var _ = require('lodash');
var moment = require('moment');
moment().format();

module.exports = {


  friendlyName: 'Round Create',


  description:
  `Creates round`,


  inputs: {
    tournamentInstance: {
      type: 'ref',
      description: 'The tournament instance',
      required: true,
    },

    currTime: {
      required: false,
      type: 'string',
      description: 'Utilised for testing',
    }
  },


  exits: {

    success: {
      description: 'Returned all data / and populated round & tableInstances',
    },

    roundNotReady: {
      description: `The next round is not ready yet - due to endTime being more than 30 seconds away, and not all boards finished`,
      message: 'The next round is not ready yet - due to endTime being more than 30 seconds away, and not all boards finished'
    },

    notFound: {
      description: 'The specified resource was not found',
      message: 'The specified resource was not found'
    },

  },


  fn: async function (inputs, exits) {
    var currTime = moment.utc();
    if(inputs.currTime){
      currTime = moment.utc(inputs.currTime);
    }

    //Populate pairs fully
    var pairIds = inputs.tournamentInstance.pairs.map((pair) => pair.id);
    inputs.tournamentInstance.pairs = await Pair.find({id: pairIds}).populate('players', {omit: ['emailAddress', 'password']}).populate('substitutes').populate('transactions');
    //We are about to start a round
    //Check how many confirmed pairs we have
    var validPairs = 0;
    for(var i = 0; i < inputs.tournamentInstance.pairs.length; i++){
      if(inputs.tournamentInstance.pairs[i].player1Status === "confirmed" && inputs.tournamentInstance.pairs[i].player2Status === "confirmed"){
        validPairs++;
      }else{
        //Before pair Delete we MUST REFUND
        for(let refund of inputs.tournamentInstance.pairs[i].transactions){
          refund.status = 'refunded';
          var res;
          if(!refund.free){ //refund logic
            var tmpRefund = await Transaction.findOne({id: refund.id}).populate('player');
            //API call to playfab to trigger add tournamentInstance.cost
            res = await sails.helpers.refund.with({
              playfabId: tmpRefund.player.playfabId,
              cost: refund.credits,
              tournamentInstanceId: inputs.tournamentInstance.id,
            });
            //Error checking
            if(!res.response){
              console.log({code: 3029, message: res.message}, tmpRefund.player, tournamentInstance)
              //TODO!! Send email
              continue;
            }
            //Update Transaction with refundTransactionId that is returned
            await Transaction.update({id: refund.id}).set({
              refundId: res.message,
              status: refund.status,
              refundReason: 'Pair Not Confirmed',
            })
          }
          else{ //just update status
            await Transaction.update({id: refund.id}).set({
              status: refund.status,
              refundReason: 'Pair Not Confirmed',
            })
          }
        }
        await Pair.destroy({id: inputs.tournamentInstance.pairs[i].id});
      }
    }

    //Refresh our tournament (to remove old pairs)
    inputs.tournamentInstance = await TournamentInstance.findOne({id: inputs.tournamentInstance.id})
    .populate('basetournament')
    .populate('pairs')
    .populate('rounds')

    inputs.tournamentInstance = await sails.helpers.populateBaseTournament(inputs.tournamentInstance);
    var pairIds = inputs.tournamentInstance.pairs.map((pair) => pair.id);
    inputs.tournamentInstance.pairs = await Pair.find({id: pairIds}).populate('players', {omit: ['emailAddress', 'password']}).populate('substitutes').populate('transactions');

    //If we only have 1 pair in this tournament, it needs to be cancelled
    if(validPairs <= 1){
      await TournamentInstance.update({id: inputs.tournamentInstance.id}).set({cancelled: true});
      //De-register all pairs from this tournament
      console.log("Tournament cancelling");
      for(var i = 0; i < inputs.tournamentInstance.pairs.length; i++){
        //Before pair Delete we MUST REFUND

        console.log("Refunding " + inputs.tournamentInstance.pairs[i].transactions.length);

        for(let refund of inputs.tournamentInstance.pairs[i].transactions){
          refund.status = 'refunded';
          var res;
          if(!refund.free){ //refund logic
            var tmpRefund = await Transaction.findOne({id: refund.id}).populate('player');
            //API call to playfab to trigger add tournamentInstance.cost
            res = await sails.helpers.refund.with({
              playfabId: tmpRefund.player.playfabId,
              cost: refund.credits,
              tournamentInstanceId: inputs.tournamentInstance.id,
            });
            //Error checking
            if(!res.response){
              console.log({code: 3029, message: res.message}, tmpRefund.player, tournamentInstance)
              //TODO!! Send email
              continue;
            }
            //Update Transaction with refundTransactionId that is returned
            await Transaction.update({id: refund.id}).set({
              refundId: res.message,
              status: refund.status,
              refundReason: 'Tournament Cancelled',
            })
          }
          else{ //just update status
            await Transaction.update({id: refund.id}).set({
              status: refund.status,
              refundReason: 'Tournament Cancelled',
            })
          }
        }

        await Pair.destroy({id: inputs.tournamentInstance.pairs[i].id});
      }
      return exits.success({ code: 3019, message: 'This tournament was cancelled due to not enough players' });
    }

    var roundNum =  inputs.tournamentInstance.rounds.length + 1;

    //console.log("Create next round " + roundNum);

    //if not first round, start time is current UTC time + 30 seconds
    var startTime;
    if(roundNum === 1){
      startTime = moment.utc(inputs.tournamentInstance.startDatetime).toISOString(); //if first round, start time is what is on tournamentInstance
      var roundsRemaining = inputs.tournamentInstance.numRounds - inputs.tournamentInstance.rounds.length;
      var tournamentEndTime = moment.utc(inputs.tournamentInstance.startDatetime).add(((inputs.tournamentInstance.timeAllowedPerBoard*inputs.tournamentInstance.boardsPerRound) + 60)*roundsRemaining, 'seconds').toISOString(); //Changed to 60 seconds to correspond to cron
      await TournamentInstance.update({id: inputs.tournamentInstance.id}).set({proposedEndTime: tournamentEndTime});
    }else{
      //Start time should be the current UTC time + 30 seconds
      startTime = moment.utc(currTime).add(30, 'seconds').toISOString();

      var roundsRemaining = inputs.tournamentInstance.numRounds - inputs.tournamentInstance.rounds.length;
      var tournamentEndTime = moment.utc(startTime).add(((inputs.tournamentInstance.timeAllowedPerBoard*inputs.tournamentInstance.boardsPerRound) + 60)*roundsRemaining, 'seconds').toISOString(); //Changed to 60 seconds to correspond to cron
      await TournamentInstance.update({id: inputs.tournamentInstance.id}).set({proposedEndTime: tournamentEndTime});
    }

    var pairs = [];
    //Get our pairs in order of tables
    if(inputs.tournamentInstance.rounds.length > 0){
      var latestRound = inputs.tournamentInstance.rounds[inputs.tournamentInstance.rounds.length - 1];
      var tables = await TableInstance.find({owningRound: latestRound.id}).populate('pairsPlaying');
      for(var i = 0; i < tables.length; i++){
        tables[i] = await sails.helpers.sortTablePairs(tables[i]);
        for(var j = 0; j < tables[i].pairsPlaying.length; j++){
          var hasPlayers = tables[i].pairsPlaying[j].player1RDB !== "" || tables[i].pairsPlaying[j].player2RDB !== "";
          var hasOpenSubstitutions = false;

          if(!hasPlayers){
            var openSubstitutionsForPair = await SubstituteRequest.find({targetPair: tables[i].pairsPlaying[j].id, status: "active"});
            hasOpenSubstitutions = openSubstitutionsForPair.length > 0;
          }

          if(hasPlayers || hasOpenSubstitutions){
            //Only add pairs that have some actual players
            pairs.push(tables[i].pairsPlaying[j]);
          }else{
            //Remove this pair - it has no players in it
            await TournamentInstance.removeFromCollection(inputs.tournamentInstance.id, 'pairs', tables[i].pairsPlaying[j].id);
            await Pair.updateOne({id: tables[i].pairsPlaying[j].id}).set({tournamentInstance: null});
          }
        }
      }
    }else{
      pairs = inputs.tournamentInstance.pairs;
    }

    //Figure out how many tableInstances required taking ceil of number of pairs/2
    var maxTables = Math.ceil(pairs.length/2);
    //Create a new round with roundNumber == 1 and next start time 30 seconds away from current UTC time
    newRound = await Round.create({
      roundNumber: roundNum,
      maxTableNumber: maxTables,
      owningTournament: inputs.tournamentInstance.id,
      roundStartTime: startTime
    }).fetch();

    var tableInstance;

    //Populate TableInstances into Round, alongside the pairs within those instances.
    //First, do all the N/S pairs
    for(var i=0; i < pairs.length; i+=2){
      //These pairs should create their tables
      var tableInst = await TableInstance.create({
        tableNumber: (i / 2) + 1,
        owningRound: newRound.id,
        northSouthPairId: pairs[i].id,
      }).fetch();

      //Add reference to specific pair
      //console.log("Adding to " + tableInst.id);
      tableInstance = await TableInstance.addToCollection(tableInst.id, 'pairsPlaying', pairs[i].id);
      //Create boardsPerRound of BoardResult and attach all of them to tableInst
      for(var j=1; j<=inputs.tournamentInstance.boardsPerRound; j++){
        var boardRes = await BoardResult.create({
          owningTableInstance: tableInst.id,
          owningTournament: inputs.tournamentInstance.id,
          overallBoardIndex: j + (inputs.tournamentInstance.boardsPerRound * (roundNum - 1)) - 1
        }).fetch();
      }
    }

    var tables = await TableInstance.find({owningRound: newRound.id});

    //Second, do all the E/W pairs
    for(var i = 1; i < pairs.length; i+=2){
      var roundIndex = roundNum - 1;
      if(roundIndex > 1){
        roundIndex = 1;
      }

      //On the second pair, add to the correct table instance
      var tableIndex = (Math.floor(i / 2) + roundIndex) % tables.length;

      //console.log("Adding to " + tables[tableIndex].id);
      tableInstance = await TableInstance.addToCollection(tables[tableIndex].id, 'pairsPlaying', pairs[i].id);
    }

    newRound.tableInstances = await TableInstance.find({owningRound: newRound.id}).populate('boardResults').populate('pairsPlaying');

    //Populate pairs fully
    for(var i = 0; i < newRound.tableInstances.length; i++){
      var pairIds = newRound.tableInstances[i].pairsPlaying.map((pair) => pair.id);
      newRound.tableInstances[i].pairsPlaying = await Pair.find({id: pairIds}).populate('substitutes').populate('players', {omit: ['emailAddress', 'password']});
      newRound.tableInstances[i] = await sails.helpers.sortTablePairs(newRound.tableInstances[i]);
    }

    return exits.success({
      message: "New round, tableInstances and boardResults created",
      newRound,
    })
  }
}

var _ = require('lodash');
var moment = require('moment');

module.exports = {


  friendlyName: 'Complete all boards for a round',


  description: 'Complete all boards for a round',


  inputs: {
    roundId: {
      type: 'number',
      description: 'The ID of the round that will auto-complete',
      required: true,
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    //This will default to all uncompleted boards being passed out
    console.log("Autocompleting round " + inputs.roundId);

    var allTables = await TableInstance.find({owningRound: inputs.roundId}).populate('boardResults');

    //These are all the overall board indices that were updated
    var updatedBoardIndices = [];

    for(var i = 0; i < allTables.length; i++){
      //Find all the uncompleted boards for this table
      for(var j = 0; j < allTables[i].boardResults.length; j++){
        if(!allTables[i].boardResults[j].done){
          if(!updatedBoardIndices.includes(j)){
            updatedBoardIndices.push(j);
          }

          //Not complete!? - Set to be complete
          await sails.helpers.completeBoard(allTables[i].boardResults[j].id);
        }
      }
    }

    for(var i = 0; i < updatedBoardIndices.length; i++){
      var overallIndex = updatedBoardIndices[i];

      var relevantBoards = [];

      for(var j = 0; j < allTables.length; j++){
        //Add this board from each table
        relevantBoards.push(allTables[j].boardResults[overallIndex]);
      }

      //Now, update match points for these boards
      await sails.helpers.calculateMatchPoints(relevantBoards);
    }

    return exits.success();
  }


};

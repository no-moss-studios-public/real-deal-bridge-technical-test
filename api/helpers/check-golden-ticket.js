var _ = require('lodash');
var axios = require('axios');
var moment = require('moment');
moment().format();

module.exports = {


  friendlyName: 'Check whether a user has a golden ticket covering a specific time (true or false)',


  description: 'Check whether a user has an active golden ticket to cover a specific time, and return true or false',


  inputs: {
    playfabId: {
      type: 'string',
      description: 'The user\'s playfabId',
      required: true,
    },

    checkTime: {
      type: 'string',
      description: 'The time to check against',
      required: true,
    },
  },

  exits: {
    success: {
      description: 'All done.',
    },
  },

  fn: async function (inputs, exits) {
    
    var data = JSON.stringify({
      "PlayFabId": inputs.playfabId,
      "VirtualCurrency":"CR",
      "CustomTags":{"TagTest":"VALUE"}
    });

    var config = {
      method: 'post',
      url: `https://${process.env.PLAYFAB_TITLEID}.playfabapi.com/Admin/GetUserInventory`,
      headers: {
        'X-SecretKey': process.env.PLAYFAB_SECRETKEY,
        'Content-Type': 'application/json'
      },
      data : data
    };

    console.log("Checking golden ticket");

    await axios(config)
    .then(function (response) {

      if(response.data.data.Inventory && response.data.data.Inventory.length > 0){
        //Confirm we have an inventory response
        for(var i = 0; i < response.data.data.Inventory.length; i++){
          //Check each item in our inventory
          var item = response.data.data.Inventory[i];

          if(item.ItemClass === "GoldenTicket"){
            //The user has a golden ticket. Lets check the session start time against the golden ticket expiration time.
            if(item.Expiration >= inputs.checkTime){
              //This ticket covers the time we want to check, so it is valid
              return exits.success({
                response: true,
                message: 'Has golden ticket'
              });
            }
          }
        }
      }

      return exits.success({
        response: false,
        message: 'No golden ticket'
      })
    })
    .catch(function (error) {
      console.log(error);
      return exits.success({
        response: false,
        message: error
      })
    });

  }


};

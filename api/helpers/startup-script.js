var _ = require('lodash');
var moment = require('moment');
moment().format();

module.exports = {


  friendlyName: 'Get User by either ABF or RDB number',


  description: 'Gets a user by either their ABF number, or their RDB number',


  inputs: {
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {

    var usersToCreate = [];
    usersToCreate.push(
      {
        emailAddress: 'admin@realdealbridge.com',
        firstName: 'Real',
        lastName: 'Deal',
        combinedName: 'Real Deal',
        overThirteen: true,
        acceptedPrivacy: true,
        acceptedTOS: true,
        playfabId: '8B67F34759982040',
        rdbNumber: 0,
        password: await sails.helpers.passwords.hashPassword('Vks*@W%4'),
      });

    for(var i = 1; i <= 64; i++){
      usersToCreate.push({
        emailAddress: 'test@realdealbridge.com',
        firstName: 'TestBot' + i,
        lastName: 'TestBot' + i,
        combinedName: 'TestBot' + i + " TestBot" + i,
        overThirteen: true,
        acceptedPrivacy: true,
        acceptedTOS: true,
        playfabId: '8B67F34759982040',
        rdbNumber: i,
        password: await sails.helpers.passwords.hashPassword('' + i),
      })
    }

    var users = await User.createEach(usersToCreate).fetch();

    await Permissions.create({ //First user in users will be admin
        permission: 1,
        owningUser: users[0].id,
    });

    var newClub = await Club.create({ //Default Real Deal Bridge Club
        clubName: 'Real Deal Bridge Club',
        profileText: 'A club for sessions run by Real Deal Bridge',
        clubDetails: 'A club for sessions run by Real Deal Bridge',
        default: true,
        status: 'active'
    }).fetch();

    var testingClub = await Club.create({
        clubName: 'TB Bridge Club',
        profileText: 'The Official TBBC',
        clubDetails: 'The Official TBBC',
        default: false,
        status: 'active'
    }).fetch();

    await Club.addToCollection(newClub.id, 'owners', users[0].id); //Adding admin as owner
    await Club.addToCollection(newClub.id, 'owners', users[0].id); //Adding admin as member
    await Club.addToCollection(testingClub.id, 'owners', users[0].id); //Adding admin as owner
    await Club.addToCollection(testingClub.id, 'members') //Adding all users as members including admin
    .members(users.map((element) => {
      return element.id;
    }));

    await Live.create({status: true});
    await DataCall.create({weeklyExport: moment.utc().toISOString(), monthlyExport: moment.utc().toISOString()});

    return exits.success([users, newClub]);
  }
};

var _ = require('lodash');
var moment = require('moment');
moment().format();
const fastcsv = require('fast-csv');
const fs = require('fs');

module.exports = {


  friendlyName: 'Get User by either ABF or RDB number',


  description: 'Gets a user by either their ABF number, or their RDB number',


  inputs: {

    email: {
      required: false,
      type: 'string',
      isEmail: true,
      description: 'The email address for the account, e.g. m@example.com. Used for testing purpose, otherwise will pull from this.req.user.emailAddress',
    },

    startDatetime: {
      type: 'string',
      required: true,
      description: "Set this according to filter"
    },

    endDatetime: {
      type: 'string',
      required: true,
      description: "Set this according to filter"
    }
  },


  exits: {
      success: {
      },

      invalid: {
      }

  },


  fn: async function (inputs, exits) {

    //Get all tournamentInstances between the datetimes specified & populate everything
    var filter = {};
    filter.proposedEndTime = {'>=': moment.utc(inputs.startDatetime).subtract('20', 'm').toISOString(), '<=': moment.utc(inputs.endDatetime).subtract('20', 'm').toISOString()};

    console.log("Exporting from " + inputs.startDatetime + " to " + inputs.endDatetime);

    var tournamentInstances = await TournamentInstance.find({
      where: filter,
      omit: ['board', 'boardData']
    })
    .populate('basetournament')
    .populate('pairs')
    .populate('owningClubs');

    var data = await sails.helpers.clubExportData(inputs.startDatetime, inputs.endDatetime);

    //Create CSV with members above
    var fileName = 'exportClubs-' + moment.utc().toISOString() + ".csv";

    return new Promise(function(resolve, reject) {
      var existsAlready = fs.existsSync(fileName);
      var ws = fs.createWriteStream(fileName, {flags:'a'});
      var csvStream = fastcsv.write(data, {
          headers: !existsAlready,
          includeEndRowDelimiter: true,
      });
      csvStream
        .on('finish', function(err){
          if(err){
            reject(new Error("something went wrong with writing"))
          }
          resolve('done');
        })
        .pipe(fs.createWriteStream(fileName, {flags:'a'}));
    })
    .then( async () => { //Send the email here, only after the original CSV has been processed
      //Send email with CSV attached
      email = inputs.email;
      var options = {
        from: 'support@realdealbridge.com',
        to: email,
        subject: 'Real Deal Bridge Club Sessions Export',
        template: 'export-club-sessions-csv',
        context: {
          startdate: inputs.startDatetime,
          enddate: inputs.endDatetime,
        },
        attachments: [
          {
            path: fileName
          }
        ]
      }
      var x = await sails.helpers.sendEmail(options)
      if(!x.success){
        throw new Error("Email failed to send");
      }
      return exits.success();
    })
    .catch( (err) => { //Send
      console.log(err);
      return exits.invalid(err);
    })
    .finally(()=>{ //Finished up, delete file
      fs.unlinkSync(fileName);
    })
  }
};

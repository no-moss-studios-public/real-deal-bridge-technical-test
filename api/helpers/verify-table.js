var _ = require('lodash');
var moment = require('moment');

module.exports = {


  friendlyName: 'Verify that a table  exists and return tableInstance',


  description: 'Verify that a table  exists and return tableInstance',


  inputs: {
    tableInstance: {
      type: 'ref',
      description: 'The table we are validating. Requires owningRound to be populated',
      required: true,
    },

    blockIfRunOut: {
      type: 'boolean',
      description: 'Whether to fail if time has run out',
      defaultsTo: true,
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

    tableNotFound: {
    },

    tableNotAttachedToInstance:{
    },

    timeRanOut:{
    },

  },


  fn: async function (inputs, exits) {

    //Check the board exists
    if(!inputs.tableInstance){
      return exits.tableNotFound();
    }

    var tournamentInstance = await TournamentInstance.findOne({id: inputs.tableInstance.owningRound.owningTournament}).populate('basetournament');
    tournamentInstance = await sails.helpers.populateBaseTournament(tournamentInstance);
    if(!tournamentInstance){
      return exits.tableNotAttachedToInstance();
    }
    inputs.tableInstance.owningRound.owningTournament = tournamentInstance;

    //Check if current time is < end_time if false then tournament should have ended
    if(!moment.utc().isBefore(moment.utc(tournamentInstance.proposedEndTime)) && inputs.blockIfRunOut){
      return exits.timeRanOut();
    }

    return exits.success(inputs.tableInstance);
  }


};

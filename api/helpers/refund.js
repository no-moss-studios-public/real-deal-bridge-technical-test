var _ = require('lodash');
var axios = require('axios');

module.exports = {


  friendlyName: 'Add Virtual Currency (REFUND)',


  description: 'Add Virtual Currency (REFUND)',


  inputs: {
    playfabId: {
      type: 'string',
      description: 'The user\'s playfabId',
      required: true,
    },

    cost: {
      type: 'number',
      description: 'The cost to be added to account',
      required: false,
      defaultsTo: 0
    },
    
    tournamentInstanceId: {
      type: 'number',
      description: 'tournamentInstance associated with this purchase',
      required: false,
      defaultsTo: -1
    },

    adminId: {
      type: 'number',
      description: 'tournamentInstance associated with this purchase',
      required: false,
      defaultsTo: -1
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    
    if(!inputs.cost || inputs.cost === 0){
      return exits.success({
        response: true,
        message: "free-value"
      });
    }

    var data = JSON.stringify({
      "Amount":inputs.cost,
      "PlayFabId": inputs.playfabId,
      "VirtualCurrency":"CR",
      "CustomTags":{
        "TournamentInstanceId": inputs.tournamentInstanceId,
        "adminId": inputs.adminId
      }
    });

    var config = {
      method: 'post',
      url: `https://${process.env.PLAYFAB_TITLEID}.playfabapi.com/Admin/AddUserVirtualCurrency`,
      headers: { 
        'X-SecretKey': process.env.PLAYFAB_SECRETKEY, 
        'Content-Type': 'application/json'
      },
      data : data
    };

    await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return exits.success({
        response: true,
        message: response.headers["x-requestid"]
      });
    })
    .catch(function (error) {
      console.log(error);
      return exits.success({
        response: false,
        message: error
      });
    });
    
  }


};

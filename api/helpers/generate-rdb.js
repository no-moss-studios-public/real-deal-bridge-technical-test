var _ = require('lodash');

module.exports = {


  friendlyName: 'Get User by either ABF or RDB number',


  description: 'Gets a user by either their ABF number, or their RDB number',


  inputs: {
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    var uniqueRDBFound = false;
    var rdb;

    while(!uniqueRDBFound){
      rdb = (100000 + Math.floor(Math.random() * 900000));

      var isABF = await sails.helpers.isValidAbf(rdb);
      if(isABF){
        //Can't use this!
        continue;
      }

      var user = await User.findOne({rdbNumber: rdb});

      if(user){
        //Not unique
        continue;
      }

      uniqueRDBFound = true;
    }

    return exits.success({result: true, rdb: rdb});
  }
};

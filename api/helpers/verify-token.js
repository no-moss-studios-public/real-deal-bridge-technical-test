var jwt = require('jsonwebtoken')
var moment = require('moment');

module.exports = {
	friendlyName: 'Verify JWT',
	description: 'Verify a JWT token.',
	inputs: {
		req: {
			type: 'ref',
			friendlyName: 'Request',
			description: 'A reference to the request object (req).',
			required: true
		}
	},
	exits: {
		invalid: {
			description: 'Invalid token or no authentication present.',
		},
		banned: {
			description: 'User is banned',
		},
		oldToken: {
			description: 'Token is old'
		},
		serverNotLive: {
			description: 'Server is not live'
		}
	},
	fn: async function(inputs, exits) {
		var req = inputs.req

		var liveStatus = await Live.findOne({id: 1});

		// first check for a cookie (web client)
		if (req.signedCookies.sailsjwt) {
			// if there is something, attempt to parse it as a JWT token
			return jwt.verify(req.signedCookies.sailsjwt, process.env.JWT_SECRET, async function(err, payload) {
				// if there's an error verifying the token (e.g. it's invalid or expired), no go
				if (err || !payload.user) return exits.invalid()
				req.user = await User.findOne({id: payload.user.id}).populate('abfNumber').populate('adminPermissions');
				if(req.user.isBanned){
					return exits.banned();
				}
				if(moment.utc(payload.jwtIssueTime).isBefore(moment.utc(req.user.jwtIssueTime))){
					return exits.oldToken();
				}

				//Check if server boot out flag is set in DB (set through migrations - so should ALWAYS be in DB)
				if(!liveStatus.status){
					//Check if admin
					for(var permission of req.user.adminPermissions){
						//Admins have all permissions
						if(permission.permission === 1){
							return exits.success(req.user);
						}
					}
					return exits.serverNotLive();
				}
				return exits.success(req.user);
			})
		}
        // no? then check for a JWT token in the header
        // console.log('in verify-token', req.headers);
        // console.log('in verify-token authorization', req.headers.authorization);
		if (req.headers['authorization']) {
			// if one exists, attempt to get the header data
			var token = req.headers['authorization'].split('Bearer ')[1]
			// if there's nothing after "Bearer", no go
			if (!token) return exits.invalid()
			// if there is something, attempt to parse it as a JWT token
			return jwt.verify(token, process.env.JWT_SECRET, async function(err, payload) {
				if (err || !payload.user) return exits.invalid()
				req.user = await User.findOne({id: payload.user.id}).populate('abfNumber').populate('adminPermissions');
				if(req.user.isBanned){
					return exits.banned();
				}
				if(moment.utc(payload.jwtIssueTime).isBefore(moment.utc(req.user.jwtIssueTime))){
					return exits.oldToken();
				}
				//Check if server boot out flag is set in DB (set through migrations - so should ALWAYS be in DB)
				if(!liveStatus.status){
					//Check if admin
					for(var permission of req.user.adminPermissions){
						//Admins have all permissions
						if(permission.permission === 1){
							return exits.success(req.user);
						}
					}
					return exits.serverNotLive();
				}
				return exits.success(req.user);
			})
		}
		// if neither a cookie nor auth header are present, then there was no attempt to authenticate
		return exits.invalid()
	}
}

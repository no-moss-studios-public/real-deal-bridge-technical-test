var _ = require('lodash');

module.exports = {


  friendlyName: 'Complete a board',


  description: 'Complete a specific board',


  inputs: {
    relevantBoardInst: {
      type: 'ref',
      description: 'An array of relevant board instances that need to update Match Points',
      required: true,
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

    scoreNotFound: {
    },

  },


  fn: async function (inputs, exits) {
    //Populate scores for each boardInstance
    var scores = [];
    var northSouthScores = [];
    var eastWestScores = [];
    var northSouthNumerical = [];
    var northSouthAverages = [];
    var eastWestNumerical = [];
    var eastWestAverages = [];

    console.log("Calculating match points");

    for(let boardInst of inputs.relevantBoardInst){
      tmp = await Score.findOne({owningBoard: boardInst.id})
      //console.log("Adding " + tmp.id);
      if(!tmp){
        //Abort if there is a board missing a score
        console.log("Not all boards have scores - missing score for board " + boardInst.id);
        return exits.success({});
      }
      scores.push(tmp);
      northSouthScores.push(tmp.northSouthScore);
      eastWestScores.push(tmp.eastWestScore);

      if(tmp.northSouthAverage === "-"){
        northSouthNumerical.push(tmp.northSouthScore);
      }else{
        northSouthAverages.push(tmp.northSouthAverage);
      }

      if(tmp.eastWestAverage === "-"){
        eastWestNumerical.push(tmp.eastWestScore);
      }else{
        eastWestAverages.push(tmp.eastWestAverage);
      }
    }

    //Calculate the max score available
    var maxScoreAvailable = (scores.length - 1) * 2;
    //console.log(maxScoreAvailable);
    var maxNeubergAvailableNorthSouth, maxNeubergAvailableEastWest;
    //if(northSouthNumerical.length <= 0){
    maxNeubergAvailableNorthSouth = maxScoreAvailable;
    //}else{
    //  maxNeubergAvailableNorthSouth = ((scores.length / northSouthNumerical.length) * (((northSouthNumerical.length - 1) * 2) + 1)) - 1;
    //}

    //if(eastWestNumerical.length <= 0){
    maxNeubergAvailableEastWest = maxScoreAvailable;
    //}else{
    //  maxNeubergAvailableEastWest = ((scores.length / eastWestNumerical.length) * (((eastWestNumerical.length - 1) * 2) + 1)) - 1;
    //}

    //Now, figure out the number of wins, draws and losses for each pair (for NS and EW)
    var northSouthStandardMPs = [];
    var eastWestStandardMPs = [];

    for(var i = 0; i < scores.length; i++){
      //console.log(scores[i].id + ": ");
      //console.log("north:");
      if(scores[i].northSouthAverage === "-"){
        //Compare against all, numerically
        var wins = 0;
        //Draws starts at -1, because we'll compare against ourself at some point
        var draws = -1;
        var losses = 0;

        for(var j = 0; j < northSouthNumerical.length; j++){
          //console.log("North Comparing " + scores[i].northSouthScore + " and " + northSouthNumerical[j]);
          if(scores[i].northSouthScore > northSouthNumerical[j]){
            wins++;
          }else if(scores[i].northSouthScore < northSouthNumerical[j]){
            losses++;
          }else{
            draws++;
          }
        }

        //console.log("W: " + wins + " D: " + draws + " L: " + losses);
        northSouthStandardMPs.push((wins * 2) + draws);
      }else{
        //Give an average
        if(scores[i].northSouthAverage === "A-"){
          northSouthStandardMPs.push(0.4 * maxScoreAvailable);
        }else if(scores[i].northSouthAverage === "A"){
          northSouthStandardMPs.push(0.5 * maxScoreAvailable);
        } else if(scores[i].northSouthAverage === "A+"){
          northSouthStandardMPs.push(0.6 * maxScoreAvailable);
        }
        //console.log("Is Neuberging: " + northSouthStandardMPs[i]);
      }

      //console.log("east:");
      if(scores[i].eastWestAverage === "-"){
        //Compare against all, numerically
        var wins = 0;
        //Draws starts at -1, because we'll compare against ourself at some point
        var draws = -1;
        var losses = 0;

        for(var j = 0; j < eastWestNumerical.length; j++){
          //console.log("East Comparing " + scores[i].eastWestScore + " and " + eastWestNumerical[j]);
          if(scores[i].eastWestScore > eastWestNumerical[j]){
            wins++;
          }else if(scores[i].eastWestScore < eastWestNumerical[j]){
            losses++;
          }else{
            draws++;
          }
        }

        //console.log("W: " + wins + " D: " + draws + " L: " + losses);
        eastWestStandardMPs.push((wins * 2) + draws);
      }else{
        //Give an average
        if(scores[i].eastWestAverage === "A-"){
          eastWestStandardMPs.push(0.4 * maxScoreAvailable);
        }else if(scores[i].eastWestAverage === "A"){
          eastWestStandardMPs.push(0.5 * maxScoreAvailable);
        } else if(scores[i].eastWestAverage === "A+"){
          eastWestStandardMPs.push(0.6 * maxScoreAvailable);
        }
        //console.log("Is Neuberging: " + eastWestStandardMPs[i]);
      }
    }



    //Now, we have the standard MPs that each set has achieved
    var results = [];
    //Calculate the Neuberg MPs

    for(var i = 0; i < scores.length; i++){
      var northSouthNeubergMPs;
      var eastWestNeubergMPs;
      if(scores[i].northSouthAverage === "-"){
        //We need to calculate the Neuberg
        // Neuberg is ((N / S) * (X + 1)) - 1
        var neuberg = ((scores.length / northSouthNumerical.length) * (northSouthStandardMPs[i] + 1)) - 1;
        northSouthNeubergMPs = neuberg;
      }else{
        //We already have the score. We can just add and move on
        northSouthNeubergMPs = northSouthStandardMPs[i];
      }

      if(scores[i].eastWestAverage === "-"){
        //We need to calculate the Neuberg
        // Neuberg is ((N / S) * (X + 1)) - 1
        var neuberg = ((scores.length / eastWestNumerical.length) * (eastWestStandardMPs[i] + 1)) - 1;
        eastWestNeubergMPs = neuberg;
      }else{
        //We already have the score. We can just add and move on
        eastWestNeubergMPs = eastWestStandardMPs[i];
      }

      //console.log("NS Neuberg:" + northSouthNeubergMPs);
      //console.log("EW Neuberg:" + eastWestNeubergMPs);

      //Now, we have all our MPs! Lets update!
      var northSouthPercentage = northSouthNeubergMPs / maxNeubergAvailableNorthSouth;
      var eastWestPercentage = eastWestNeubergMPs / maxNeubergAvailableEastWest;

      var tmp = await Score.updateOne({id: scores[i].id}).set({
        northSouthMatchPoints: northSouthNeubergMPs,
        northSouthMatchPointsMax: maxNeubergAvailableNorthSouth,
        eastWestMatchPoints: eastWestNeubergMPs,
        eastWestMatchPointsMax: maxNeubergAvailableEastWest,
      });
      results.push(tmp);
    }

    //And there you have it! So... easy...!???!?!??!?!?!??
    return exits.success(results);
  }


};

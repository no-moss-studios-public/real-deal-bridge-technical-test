var _ = require('lodash');

module.exports = {


  friendlyName: 'Is Session Director',


  description: 'Checks whether a user is a session director',


  inputs: {
    userRDB: {
      type: 'string',
      description: 'The RDB of the user to check',
      required: true,
    },

    tournamentInstanceId: {
      type: 'number',
      description: 'The tournament instance ID to chek',
      required: true,
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    var user = await User.findOne({rdbNumber: inputs.userRDB});

    if(!user){
      return exits.success(false);
    }

    var tournamentInstance = await TournamentInstance.findOne({id: inputs.tournamentInstanceId}).populate('owningClubs');
    if(!tournamentInstance){
      return exits.success(false);
    }

    //First, check if we own any of the clubs for this tournament
    for(var club of tournamentInstance.owningClubs){
      var isClubOwner = await sails.helpers.isClubOwner(inputs.userRDB, club.id);

      if(isClubOwner){
        return exits.success(true);
      }
    }

    //Next check if session director for the tournament instance specifically
    //TODO - This

    return exits.success(false);
  }


};

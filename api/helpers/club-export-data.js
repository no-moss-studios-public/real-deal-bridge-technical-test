var _ = require('lodash');
var moment = require('moment');
moment().format();
const fastcsv = require('fast-csv');
const fs = require('fs');

module.exports = {


  friendlyName: 'Get User by either ABF or RDB number',


  description: 'Gets a user by either their ABF number, or their RDB number',


  inputs: {
    startDatetime: {
      type: 'string',
      required: true,
      description: "Set this according to filter"
    },

    endDatetime: {
      type: 'string',
      required: true,
      description: "Set this according to filter"
    }
  },


  exits: {
      success: {
      },

      invalid: {
      }

  },


  fn: async function (inputs, exits) {

    //Get all tournamentInstances between the datetimes specified & populate everything
    var filter = {};
    filter.proposedEndTime = {'>=': moment.utc(inputs.startDatetime).subtract('20', 'm').toISOString(), '<=': moment.utc(inputs.endDatetime).subtract('20', 'm').toISOString()};

    console.log("Exporting from " + inputs.startDatetime + " to " + inputs.endDatetime);

    var tournamentInstances = await TournamentInstance.find({
      where: filter,
      omit: ['board', 'boardData']
    })
    .populate('basetournament')
    .populate('pairs')
    .populate('owningClubs');

    var data = [] //Used for CSV

    for(var i = 0; i < tournamentInstances.length; i++){
      //Populate details from base tournament
      tournamentInstances[i].basetournament = _.omit(tournamentInstances[i].basetournament, 'board');
      var currInstanceWithoutBasetournament = _.omit(tournamentInstances[i], 'basetournament');
      tournamentInstances[i] = _.mergeWith(
        {}, tournamentInstances[i].basetournament, currInstanceWithoutBasetournament,
        (a, b) => b === null ? a : undefined
      );
      //Populate pairs fully
      var pairIds = tournamentInstances[i].pairs.map((pair) => pair.id);
      var pairs = await Pair.find({id: pairIds}).populate('players').populate('substitutes').populate('transactions').populate('tableInstances');
      tournamentInstances[i].pairs = pairs;

      //Go through each PAIR
      for(var j = 0; j < pairs.length; j++){
        //Go through each tableInstance
        var runningMPs = 0;
        var runningMPsMax = 0;
        var runningPercentage = 0;
        var numBoards = 0
        var direction = 'EW';

        //console.log("For pair: " + pairs[j].player1RDB + " and " + pairs[j].player2RDB);

        for(var k = 0; k < pairs[j].tableInstances.length; k++){
          //pairs[j].tableInstances[k].northSouthPairId gives us pairId for NS, we can use the remaining one for SW
          //Go through each boardResult & Score to calculate percentage score (AVERAGE of (match points / match points max) across all their scores)
          var boardsWithScores = await BoardResult.find({owningTableInstance: pairs[j].tableInstances[k].id}).populate('score')
          //For each boardwithscores calculate (match points / match points max) (depending on if they are NS or SW)

          for(var b of boardsWithScores){
            if(pairs[j].id === pairs[j].tableInstances[k].northSouthPairId){ //This pair is NS
              direction = 'NS'
            }
            if(b.score && direction === 'EW'){
              if(b.score.eastWestMatchPointsMax > 0){
                //console.log("EW MPs: " + b.score.eastWestMatchPoints);
                runningMPs += b.score.eastWestMatchPoints;
                runningMPsMax += b.score.eastWestMatchPointsMax;
              }else{
                //console.log("No MPs!");
              }
              numBoards += 1;
            }
            else if(b.score && direction === 'NS'){
              if(b.score.northSouthMatchPointsMax > 0){
                //console.log("NS MPs: " + b.score.northSouthMatchPoints);
                runningMPs += b.score.northSouthMatchPoints;
                runningMPsMax += b.score.northSouthMatchPointsMax;
              }else{
                //console.log("No MPs!");
              }
              numBoards += 1;
            }
            else{
              //What happens here?
              //runningPercentage += 0
              //numBoards += 1; //Should this even be counted???
              //console.log("We have no direction!?");
            }
          }
        }
        if(numBoards > 0){
          //console.log("Totals - MPs: " + runningMPs + " and max: " + runningMPsMax);
          runningPercentage = runningMPs / runningMPsMax; //Final percentage
          //console.log("Final percentage: " + runningPercentage);
        }else{
          //console.log("Pair had no boards!");
          runningPercentage = 0;
        }

        var playerOne = await User.findOne({rdbNumber: pairs[j].player1RDB});
        var transactionOne = {credits: 0, free: true, status: "N/A", refundReason: "N/A"}
        var player1Abf = '';
        if(playerOne){
          var transactions = await Transaction.find({player: playerOne.id, tournamentInstance: tournamentInstances[i].id});
          if(transactions.length > 0){
            transactionOne = transactions[transactions.length - 1];
          }
          player1Abf = await User.findOne({rdbNumber: pairs[j].player1RDB}).populate('abfNumber');
          if(player1Abf && player1Abf.abfNumber && player1Abf.abfNumber.length > 0){
            player1Abf = player1Abf.abfNumber[0].uniqueNumber;
          }else{
            player1Abf = "";
          }
        }

        var playerTwo = await User.findOne({rdbNumber: pairs[j].player1RDB});
        var transactionTwo = {credits: 0, free: true, status: "N/A", refundReason: "N/A"}
        var player2Abf = '';
        if(playerTwo){
          var transactions = await Transaction.find({player: playerTwo.id, tournamentInstance: tournamentInstances[i].id});
          if(transactions.length > 0){
            transactionTwo = transactions[transactions.length - 1];
          }
          player2Abf = await User.findOne({rdbNumber: pairs[j].player2RDB}).populate('abfNumber');
          if(player2Abf && player2Abf.abfNumber && player2Abf.abfNumber.length > 0){
            player2Abf = player2Abf.abfNumber[0].uniqueNumber;
          }else{
            player2Abf = "";
          }
        }

        var tmp = {
          'Date': moment.utc(tournamentInstances[i].proposedEndTime).add('30', 'm').format('DD/MM/YYYY'),
          'Time': moment.utc(tournamentInstances[i].proposedEndTime).add('30', 'm').format('h:mm:ss A'),
          'Club': tournamentInstances[i].owningClubs[0].clubName,
          'RDB Club ID': tournamentInstances[i].owningClubs[0].id, //Caveat only first club at the moment
          'ABF Club ID': tournamentInstances[i].owningClubs[0].abfNumber,
          'Session Name': tournamentInstances[i].name,
          'Session ID': tournamentInstances[i].id,
          'Boards': tournamentInstances[i].boardsPerRound*tournamentInstances[i].numRounds, //Total number of boards
          'Tables': pairs[j].tableInstances.length, //Tables that the pair played in
          'Direction': direction,
          'Player 1 RDB': pairs[j].player1RDB,
          'Player 1 ABF': player1Abf,
          'Player 2 RDB': pairs[j].player2RDB,
          'Player 2 ABF': player2Abf,
          'Player 1 Credits Charged': transactionOne.credits,
          'Player 1 Credits Free': transactionOne.free,
          'Player 1 Credits Status': transactionOne.status,
          'Player 1 Credits Message': transactionOne.refundReason,
          'Player 2 Credits Charged': transactionTwo.credits,
          'Player 2 Credits Free': transactionTwo.free,
          'Player 2 Credits Status': transactionTwo.status,
          'Player 2 Credits Message': transactionTwo.refundReason,
          'Score': runningPercentage * 100,
        }
        data.push(tmp);
      }
    }

    return exits.success(data);
  }
};

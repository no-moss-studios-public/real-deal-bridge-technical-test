var _ = require('lodash');
var axios = require('axios');

module.exports = {


  friendlyName: 'Check balance against a value return true or false',


  description: 'Add Virtual Currency (REFUND)',


  inputs: {
    playfabId: {
      type: 'string',
      description: 'The user\'s playfabId',
      required: true,
    },

    cost: {
      type: 'number',
      description: 'The cost to be added to account',
      required: false,
      defaultsTo: 0
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {

    if(!inputs.cost || inputs.cost === 0){
      return exits.success({
        response: true,
        message: 'Have enough credits'
      });
    }
    
    var data = JSON.stringify({
      "Amount":inputs.cost,
      "PlayFabId": inputs.playfabId,
      "VirtualCurrency":"CR",
      "CustomTags":{"TagTest":"VALUE"}
    });

    var config = {
      method: 'post',
      url: `https://${process.env.PLAYFAB_TITLEID}.playfabapi.com/Admin/GetUserInventory`,
      headers: { 
        'X-SecretKey': process.env.PLAYFAB_SECRETKEY, 
        'Content-Type': 'application/json'
      },
      data : data
    };

    await axios(config)
    .then(function (response) {      
      console.log('inside check-balance', response.data);
      if(response.data.data.VirtualCurrency['CR'] < inputs.cost){
        return exits.success({
          response: true,
          message: 'Not enough credits'
        });
      }
      return exits.success({
        response: true,
        message: 'Have enough credits'
      })
    })
    .catch(function (error) {
      console.log(error);
      return exits.success({
        response: false,
        message: error
      })
    });
    
  }


};

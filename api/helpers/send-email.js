let nodemailer = require('nodemailer');
let aws = require('aws-sdk');
var hbs = require('nodemailer-express-handlebars');

module.exports = {


  friendlyName: 'Send email',


  description: 'Send email',


  inputs: {
    details: {
      type: 'ref',
      description: 'The details for sending email',
      required: true,
    },
  },


  exits: {

    success: {
      description: 'All done.',
    },

    invalid: {
      description: 'Something went wrong',
    }

  },


  fn: async function (inputs, exits) {

    // create Nodemailer SES transporter
    let transporter = nodemailer.createTransport({
      debug: true,
      SES: new aws.SES({
        apiVersion: '2010-12-01'
      })
    });

    transporter.use('compile', hbs({
      viewEngine: {
        extname: '.hbs', // handlebars extension
        partialsDir: './views/templates',
        layoutsDir: './views/templates',
        defaultLayout: '',
      },
      viewPath: './views/templates',
      extName: '.hbs'
    }))


    try{
      await transporter.sendMail(inputs.details);
      return exits.success({success: true})
    }
    catch (err){
      return exits.success({success: false, message: err})
    }

  }

};

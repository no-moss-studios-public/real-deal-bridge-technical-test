var _ = require('lodash');

module.exports = {


  friendlyName: 'Is Valid ABF',


  description: 'Checks whether an ABF number is a valid ABF number or not',


  inputs: {
    number: {
      type: 'number',
      description: 'The RDB of the user to check',
      required: true,
    },
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    var num = inputs.number;

    if(num < 10){
      return exits.success(false);
    }
    if(num > 1200000){
      return exits.success(false);
    }

    var bz = Math.floor(num / 10000000);
    var nk = Math.floor(num - (bz - 100000000));
    var az = Math.floor(num / 1000000);
    nk = Math.floor(num - (az * 10000000));
    var b = Math.floor(nk / 100000);
    nk = Math.floor(nk - (b * 100000));
    var c = Math.floor(nk / 10000);
    nk = Math.floor(nk - (c * 10000));
    var d = Math.floor(nk / 1000);
    nk = Math.floor(nk - (d * 1000));
    var e = Math.floor(nk / 100);
    nk = Math.floor(nk - (e * 100));
    var f = Math.floor(nk / 10);
    var g = Math.floor(nk - (f * 10));

    var tst = (bz * 8) + (az * 7) + (b * 6) + (c * 5) + (d * 4) + (e * 3) + (f * 2);

    tst = tst % 11;

    if(tst > 1){
      tst = 11 - tst;
    }

    if(g != tst){
      return exits.success(false);
    }

    return exits.success(true);
  }


};

var _ = require('lodash');

module.exports = {


  friendlyName: 'Convert bri',


  description: 'Convert bri to N E S W data',


  inputs: {
    bri: {
      type: 'ref',
      description: 'The bri file (binary data)',
      required: true
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    
    const requiredBytesPerDeal = 128;

    var asciiRes = inputs.bri.toString('ascii')
    
    resObj = [];

    for(var i = 0; i < asciiRes.length/requiredBytesPerDeal; i++){
      var dealNum = []
      for(var j = 0; j < requiredBytesPerDeal; j+=2){
        var num = asciiRes[(i * requiredBytesPerDeal) + j] + asciiRes[(i * requiredBytesPerDeal) + j+1]
        dealNum.push(parseInt(num));
      }
      var deal = new BRI_DEAL(dealNum);
      resObj.push(deal);
    }

    /* resObj = [
      BRI_DEAL {
        N: [13 cards(numbers)],
        E: [13 cards(numbers)],
        S: [13 cards(numbers)],
        W: [13 cards(numbers)],
      },
      BRI_DEAL {
        N: [13 cards(numbers)],
        E: [13 cards(numbers)],
        S: [13 cards(numbers)],
        W: [13 cards(numbers)],
      }
    ]
    */

    return exits.success(resObj);
  }


};

const requiredCardsPerHand = 13;

class BRI_DEAL {
  constructor(data){
    //Parse the 128 bytes to N E S W
    this.north = data.slice(0, requiredCardsPerHand);
    this.east = data.slice(requiredCardsPerHand, requiredCardsPerHand*2);
    this.south = data.slice(requiredCardsPerHand*2, requiredCardsPerHand*3);
    var westArr = []
    for(var i=1; i <= 52; i++){
      westArr.push(i);
    }
    westArr = _.difference(westArr, this.north);
    westArr = _.difference(westArr, this.east);
    westArr = _.difference(westArr, this.south);
    this.west = westArr;
  } 
};

var _ = require('lodash');

module.exports = {


  friendlyName: 'Calculate the score for a board',


  description: 'Calculate and return the score object for a board',


  inputs: {
    boardInstanceId: {
      type: 'number',
      description: 'A board instance'
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    var board = await BoardResult.findOne({id: inputs.boardInstanceId}).populate('bidsPlaced').populate('cardsPlayed').populate('claims');

    if(!board){
      return exits.success(false);
    }

    var bidFound = false;
    var numPasses = 0;
    var lastRealBid = false;
    var declarer = 0;
    var isDoubled = false;
    var isRedoubled = false;
    //First, lets step through the bids and find out what the contract is supposed to be
    for(var i = 0; i < board.bidsPlaced.length; i++){
      if(bidFound){
        break;
      }

      switch(board.bidsPlaced[i].bidType){
        case 9:
          //Double
          numPasses = 0;
          isDoubled = true;
          break;
        case 99:
          //Redouble
          numPasses = 0;
          isRedoubled = true;
          break;
        case 999:
          //Pass
          numPasses++;
          if(lastRealBid){
            if(numPasses === 3){
              //We've found our bid
              bidFound = true;
            }
          }else if(numPasses === 4){
            //This board passed out
            bidFound = true;
          }
          break;
        default:
          //This is a new valid bid
          numPasses = 0;
          isDoubled = false;
          isRedoubled = false;
          lastRealBid = board.bidsPlaced[i];
          break;
      }
    }

    //We have found our bid
    //If we passed out, we can finish up here
    if((!lastRealBid) && bidFound){
      var score = {
        result: 'passed',
        contract: 'passed',
        declarer: -1,
        lead: -1,
        owningBoard: board.id,
        northSouthScore: 0,
        eastWestScore: 0,
        boardIndex: board.overallBoardIndex,
      }

      console.log("Board score passed out");
      return exits.success(score);
    }

    //If not, lets find our declarer and our opening lead
    var biddingTeam = lastRealBid.playerIndex % 2;
    var declarer = -1;
    for(var i = 0; i < board.bidsPlaced.length; i++){
      if(board.bidsPlaced[i].bidType == lastRealBid.bidType){
        if(board.bidsPlaced[i].playerIndex % 2 == biddingTeam){
          //This was the first bid of the right type placed by this team
          declarer = board.bidsPlaced[i].playerIndex;
          break;
        }
      }
    }

    var openingLead = (declarer + 1) % 4;

    //Now, lets calculate our contract
    var contract = lastRealBid.bidRank;
    var trumpSuit = -1;
    switch(lastRealBid.bidType){
      case 1:
        //Clubs
        contract += "C";
        trumpSuit = 3;
        break;
      case 2:
        //Diamonds
        contract += "D";
        trumpSuit = 2;
        break;
      case 3:
        //Hearts
        contract += "H";
        trumpSuit = 1;
        break;
      case 4:
        //Spades
        contract += "S";
        trumpSuit = 0;
        break;
      case 5:
        //No Trumps
        contract += "NT";
        trumpSuit = -1;
        break;
    }


    //Add double/redouble to the contract
    if(isDoubled){
      contract += "x";
    }

    if(isRedoubled){
      contract += "x";
    }

    //Next, lets step through and count the tricks won by each side
    var nsTricks = 0;
    var ewTricks = 0;

    var leadCard;

    for(var i = 0; i < board.cardsPlayed.length; i+=4){
      if(i == 0){
        leadCard = board.cardsPlayed[0].cardPlayed;
      }

      //We are starting a new trick
      var ledSuit;

      var winningCard;
      var winningCardSuit;
      var winningCardRank;

      var cardsPlayedThisTrick = 0;

      for(var j = 0; j < 4; j++){
        var cardIndex = i + j;

        if(cardIndex >= board.cardsPlayed.length){
          //There has most likely been a claim, leading to an incomplete trick
          break;
        }

        var card = board.cardsPlayed[cardIndex];

        var cardSuit = Math.floor((card.cardPlayed - 1) / 13);
        var cardRank = (card.cardPlayed - 1) % 13;

        //If first card of the trick
        if(j == 0){
          ledSuit = cardSuit;
          winningCard = card;
          winningCardSuit = cardSuit;
          winningCardRank = cardRank;
          continue;
        }

        //If not first card, check if it's winning so far
        if(cardSuit == trumpSuit){
          if(winningCardSuit == trumpSuit){
            if(cardRank < winningCardRank){
              //It's an overtrump
              winningCard = card;
              winningCardSuit = cardSuit;
              winningCardRank = cardRank;
            }
          }else{
            //It's a trump
            winningCard = card;
            winningCardSuit = cardSuit;
            winningCardRank = cardRank;
          }
        }else if(cardSuit == ledSuit){
          //We're in the right suit at least
          if(winningCardSuit == trumpSuit){
            //The winning card is a trump, and we ain't gonna beat that
          }
          else if(cardRank < winningCardRank){
            //We've beaten the winning card NOT in trumps and IN suit
            winningCard = card;
            winningCardSuit = cardSuit;
            winningCardRank = cardRank;
          }
        }

        if(j === 3){
          //If this is a completed trick
          //console.log("Trick won by card " + winningCard.cardPlayed + " played by " + winningCard.playerIndex);

          var winningTeam = winningCard.playerIndex % 2;

          //console.log("Trick won by " + (winningTeam == 0 ? "NS" : "EW"));

          if(winningTeam === 0){
            nsTricks++;
          }else{
            ewTricks++;
          }
        }
      }
    }

    //Finally, lets add the claim information
    for(var i = 0; i < board.claims.length; i++){
      var claim = board.claims[i];

      if(!claim.claimAccepted){
        continue;
      }

      if(claim.playerIndex % 2 == 0){
        //Claim was by NS
        nsTricks += claim.tricksClaimed;
        //Give EW all the other tricks
        ewTricks = (13 - nsTricks);
      }else{
        //Claim was by EW
        ewTricks += claim.tricksClaimed;
        //Give NS all the other tricks
        nsTricks = (13 - ewTricks);
      }
    }

    //Now, check for an incomplete board
    if(ewTricks + nsTricks !== 13 || !bidFound){
      //This board is not complete

      //Ensuring incomplete bidding doesn't finalise lead/declarer
      if(!bidFound){
          openingLead = -1;
          declarer = -1;
      }

      var score = {
          result: "incomplete",
          contract: "incomplete",
          northSouthScore: 0,
          eastWestScore: 0,
          owningBoard: board.id,
          lead: openingLead,
          declarer: declarer,
          northSouthAverage: "A",
          eastWestAverage: "A",
          nsTricks: nsTricks,
          ewTricks: ewTricks,
          boardIndex: board.overallBoardIndex,
          leadCard: leadCard,
      }


      console.log("Board score incomplete, with " + (ewTricks + nsTricks) + " tricks, and " + board.cardsPlayed.length + " cards played");

      return exits.success(score);
    }

    //Lets calculate the vulnerable position
    var vulnerableNumber = (board.overallBoardIndex % 16) + 1;
    var vulnerablePosition;
    switch(vulnerableNumber){
      case 1:
      case 8:
      case 11:
      case 14:
        //No vulnerability
        vulnerablePosition = -1;
        break;
      break;
      case 2:
      case 5:
      case 12:
      case 15:
        //NS vulnerability
        vulnerablePosition = 0;
        break;
      break;
      case 3:
      case 6:
      case 9:
      case 16:
        //EW vulnerability
        vulnerablePosition = 1;
        break;
      break;
      default:
        //All vulnerability
        vulnerablePosition = 2;
        break;
    }

    //Finally, lets calculate the score (we have a completed board)
    //First, calculate our result string
    var trickCount;
    var isVulnerable;
    if(lastRealBid.playerIndex % 2 === 0){
      trickCount = nsTricks - 6;
      isVulnerable = (vulnerablePosition === 0 || vulnerablePosition === 2);
    }else{
      trickCount = ewTricks - 6;
      isVulnerable = (vulnerablePosition === 1 || vulnerablePosition === 2);
    }

    var resultString = contract;

    //Add declarer to the contract
    switch(declarer){
      case 0:
        resultString += "N";
        break;
      case 1:
        resultString += "E";
        break;
      case 2:
        resultString += "S";
        break;
      case 3:
        resultString += "W";
        break;
    }

    if(trickCount > lastRealBid.bidRank){
      resultString += "+" + (trickCount - lastRealBid.bidRank);
    }else if(trickCount < lastRealBid.bidRank){
      resultString += "-" + (lastRealBid.bidRank - trickCount);
    }else{
      resultString += "=";
    }

    //Now calculate our score number
    var bidderScore = 0;

    if(trickCount >= lastRealBid.bidRank){
      //We made our contract
      var overtricks = trickCount - lastRealBid.bidRank;
      console.log("Trick Count: " + trickCount);
      console.log("Overtricks: " + overtricks);

      var contractPoints = 0;
      var overtrickPoints = 0;
      var bonusPoints = 0;

      switch(lastRealBid.bidType){
        case 1:
        case 2:
          contractPoints = lastRealBid.bidRank * 20;
          overtrickPoints = overtricks * 20;
          break;
        case 3:
        case 4:
          contractPoints = lastRealBid.bidRank * 30;
          overtrickPoints = overtricks * 30;
        break;
        case 5:
          contractPoints = (lastRealBid.bidRank * 30) + 10;
          overtrickPoints = overtricks * 30;
        break;
      }

      if(isRedoubled){
        contractPoints *= 4;
        overtrickPoints = overtricks * (isVulnerable ? 400 : 200);

        //Insult bonus
        bonusPoints += 100;
      }else if(isDoubled){
        contractPoints *= 2;
        overtrickPoints = overtricks * (isVulnerable ? 200 : 100);

        //Insult bonus
        bonusPoints += 50;
      }

      if(lastRealBid.bidRank == 7){
        //Grand slam
        bonusPoints += isVulnerable ? 2000 : 1300;
      }else if(lastRealBid.bidRank == 6){
        //Slam
        bonusPoints += isVulnerable ? 1250 : 800;
      }else if(contractPoints < 100){
        //Part score
        bonusPoints += 50;
      }else{
        //Game bonus
        bonusPoints += isVulnerable ? 500 : 300;
      }

      bidderScore = contractPoints + overtrickPoints + bonusPoints;
    }
    else{
      // We didn't make our contract
      var undertricks = lastRealBid.bidRank - trickCount;

      var newScore = 0;
      if(!isDoubled){
        newScore = undertricks * (isVulnerable ? -100 : -50);
      }else{
        for(var i = 1; i <= undertricks; i++){
          if(i === 1){
            if(isRedoubled){
              newScore -= isVulnerable ? 400 : 200;
            }else if(isDoubled){
              newScore -= isVulnerable ? 200 : 100;
            }
          }
          else if(i < 4 && !isVulnerable){
            newScore -= isRedoubled ? 400 : 200;
          }
          else{
            newScore -= isRedoubled ? 600 : 300;
          }

          console.log("For " + i + " newScore is " + newScore);
        }
      }

      bidderScore = newScore;
    }

    //Now, we have calculated our score
    var nsScore = (lastRealBid.playerIndex % 2 === 0) ? bidderScore : (-1 * bidderScore);
    var ewScore = (lastRealBid.playerIndex % 2 === 1) ? bidderScore : (-1 * bidderScore);

    var score = {
      result: resultString,
      northSouthScore: nsScore,
      eastWestScore: ewScore,
      owningBoard: board.id,
      contract: contract,
      declarer: declarer,
      lead: openingLead,
      nsTricks: nsTricks,
      ewTricks: ewTricks,
      boardIndex: board.overallBoardIndex,
      leadCard: leadCard,
    }

    return exits.success(score);
  }

};

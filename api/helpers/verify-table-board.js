var _ = require('lodash');
var moment = require('moment');

module.exports = {


  friendlyName: 'Verify that a table and board exists and return tableInstance and boardInstance',


  description: 'Verify that a table and board exists and return tableInstance and boardInstance',


  inputs: {
    tableInstance: {
      type: 'ref',
      description: 'The table instance. Requires owningRound to be populated',
      required: true,
    },

    boardInstance: {
      type: 'ref',
      description: 'The board instance',
      required: true,
    },

    allowExpired: {
      type: 'boolean',
      required: false
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

    tableNotFound: {
    },

    tableNotAttachedToInstance:{
    },

    timeRanOut:{
    },

    boardIndexNotValid:{
    },

    boardInstanceNotFound:{
    },

  },


  fn: async function (inputs, exits) {

    //Check the board exists
    if(!inputs.tableInstance){
      return exits.tableNotFound();
    }

    var tournamentInstance = await TournamentInstance.findOne({id: inputs.tableInstance.owningRound.owningTournament})
    .populate('basetournament');
    tournamentInstance = await sails.helpers.populateBaseTournament(tournamentInstance);

    if(!tournamentInstance){
      return exits.tableNotAttachedToInstance();
    }
    inputs.tableInstance.owningRound.owningTournament = tournamentInstance;

    //Check if current time is < end_time if false then tournament should have ended
    if(!moment.utc().isBefore(moment.utc(tournamentInstance.proposedEndTime))){
      if(!inputs.allowExpired){
        return exits.timeRanOut();
      }
    }
    if(!inputs.boardInstance){
      return exits.boardInstanceNotFound();
    }

    return exits.success([inputs.tableInstance, inputs.boardInstance]);
  }


};

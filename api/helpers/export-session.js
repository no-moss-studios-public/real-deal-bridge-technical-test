var _ = require('lodash');
var moment = require('moment');
moment().format();
const fastcsv = require('fast-csv');
const fs = require('fs');

module.exports = {


  friendlyName: 'Get Session Data',


  description: 'Gets the session data in a format ready to be emailed',


  inputs: {

    tournamentInstanceId: {
      type: 'number',
      description: 'The tournament instance we\'re collecting the data for'
    }
  },


  exits: {
      success: {
      }

  },


  fn: async function (inputs, exits) {
    //Find scores for tournamentInstanceId
    var boardsWithScores = await BoardResult.find({owningTournament: inputs.tournamentInstanceId}).populate('score').populate('owningTableInstance').populate('cardsPlayed');
    var data = []
    var declarerEnum = {
      0: "N",
      1: "E",
      2: "S",
      3: "W"
    }

    //Loop through boardsWithScores to add Pair information into it.
    for(let b of boardsWithScores){
      var tblInst = await TableInstance.findOne({id: b.owningTableInstance.id}).populate('pairsPlaying');

      //Get NS and EW players
      var northPlayer, northPlayerABF, southPlayer, southPlayerABF, eastPlayer, eastPlayerABF, westPlayer, westPlayerABF;
      for(let p of tblInst.pairsPlaying){
        if(p.id === tblInst.northSouthPairId){
          northPlayer = p.player1RDB;
          northPlayerABF = await sails.helpers.getUserAbf(p.player1RDB);
          southPlayer = p.player2RDB;
          southPlayerABF = await sails.helpers.getUserAbf(p.player2RDB);
        }
        else{
          eastPlayer = p.player1RDB
          eastPlayerABF = await sails.helpers.getUserAbf(p.player1RDB);
          westPlayer = p.player2RDB
          westPlayerABF = await sails.helpers.getUserAbf(p.player2RDB);
        }
      }

      //tricks depends on who declarer is (NS or EW)
        var tricks;
      if(b.score){
        if(b.declarer === 0 || b.declarer === 2){ //North or South
          tricks = b.score.nsTricks;
        }
        else{
          tricks = b.score.ewTricks;
        }
      }else{
        tricks = "Unable to calculate tricks";
      }

      var contract;
      if(b.score){
        contract = b.score.contract;
      }else{
        contract = "Unable to return contract";
      }

      var ledCard;
      if(b.cardsPlayed.length <= 0){
        ledCard = "";
      }else{
        ledCard = b.cardsPlayed[0].cardPlayed;

        var cardSuit = Math.floor((ledCard - 1) / 13);
        var cardRank = (ledCard - 1) % 13;

        switch(14 - cardRank){
          case 14:
            ledCard = "A";
          break;
            case 13:
            ledCard = "K";
          break;
            case 12:
            ledCard = "Q";
          break;
            case 11:
            ledCard = "J";
          break;
            default:
            ledCard = "" + (14 - cardRank);
        }

        switch(cardSuit){
          case 0:
            ledCard += "S";
            break;
          case 1:
            ledCard += "H";
            break;
          case 2:
            ledCard += "D";
            break;
          case 3:
            ledCard += "C";
            break;
        }
      }

      var tmp = {
        'Board #': b.overallBoardIndex + 1,
        'North Player RDB#': northPlayer,
        'North Player ABF#': northPlayerABF,
        'South Player RDB#': southPlayer,
        'South Player ABF#': southPlayerABF,
        'East Player RDB#': eastPlayer,
        'East Player ABF#': eastPlayerABF,
        'West Player RDB#': westPlayer,
        'West Player ABF#': westPlayerABF,
        'Contract': contract,
        'Declarer': declarerEnum[b.declarer],
        'Led Card': ledCard,
        'Tricks': tricks,
      }

      data.push(tmp);
    }

    return exits.success(data);
  }
};

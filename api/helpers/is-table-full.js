var _ = require('lodash');

module.exports = {


  friendlyName: 'Is Table Full',


  description: 'Checks whether a table is full or not',


  inputs: {
    tableInstance: {
      type: 'ref',
      description: 'The table instance to check',
      required: true,
    },
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    //Does this table have the right number of pairs
    if(inputs.tableInstance.pairsPlaying.length != 2){
      return exits.success(false);
    }

    //For each pair, does the table have the right number of players
    for(var i = 0; i < inputs.tableInstance.pairsPlaying.length; i++){
      var pair = await Pair.findOne({id: inputs.tableInstance.pairsPlaying[i].id}).populate('players', {omit: ['emailAddress', 'password']});
      if(pair.players.length != 2){
        return exits.success(false);
      }
    }

    return exits.success(true);
  }


};

var _ = require('lodash');
var moment = require('moment');

module.exports = {
  friendlyName: 'Sort a table pair',


  description: 'Sort a table pairs so that [0] = northsouth and [1] = eastwest',


  inputs: {
    tableInstance: {
      type: 'ref',
      description: 'The table instance',
      required: true,
    },

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    if(!inputs.tableInstance){
      console.log("no");
      return exits.success(inputs.tableInstance);
    }

    if(!inputs.tableInstance.northSouthPairId){
      return exits.success(inputs.tableInstance);
    }

    //Sort out junk pairs
    if(inputs.tableInstance.pairsPlaying.length > 2){
      var realPairs = [];

      for(var i = 0; i < inputs.tableInstance.pairsPlaying.length; i++){
        if(inputs.tableInstance.pairsPlaying[i].tournamentInstance){
          realPairs.push(inputs.tableInstance.pairsPlaying[i]);
        }
      }

      inputs.tableInstance.pairsPlaying = realPairs;
    }

    if(inputs.tableInstance.pairsPlaying.length == 2){
      if(inputs.tableInstance.pairsPlaying[0].id != inputs.tableInstance.northSouthPairId){
        //Swap them
        var tmp = inputs.tableInstance.pairsPlaying[0];
        inputs.tableInstance.pairsPlaying[0] = inputs.tableInstance.pairsPlaying[1];
        inputs.tableInstance.pairsPlaying[1] = tmp;
      }
    }

    return exits.success(inputs.tableInstance);
  }


};

var _ = require('lodash');

module.exports = {


  friendlyName: 'Get User ABF',


  description: 'Gets a user\'s ABF number from their RDB number. If they don\'t have one, return an empty string',


  inputs: {
    rdbNumber: {
      type: 'string',
      description: 'The RDB number of the user',
      required: false,
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    if(inputs.rdbNumber.length <= 0){
      return exits.success("");
    }

    var user = await User.findOne({rdbNumber: inputs.rdbNumber}).populate('abfNumber');

    if(!user){
      return exits.success("");
    }

    if(user.abfNumber.length < 1){
      return exits.success("");
    }

    return exits.success(user.abfNumber[0].uniqueNumber);
  }
};

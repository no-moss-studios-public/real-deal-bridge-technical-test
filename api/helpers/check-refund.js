var _ = require('lodash');

module.exports = {


  friendlyName: 'Check Refund',


  description: 'Checks whether a user should get a refund for a tournament, and processes it if so',


  inputs: {
    userId: {
      type: 'number',
      description: 'The ID of the user we\'re getting',
      required: true,
    },

    tournamentInstanceId: {
      type: 'number',
      description: 'The ID of the tournament to be looking through',
      required: true,
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    var bids = await Bid.find({player: inputs.userId, tournament: inputs.tournamentInstanceId});
    var cards = await Card.find({player: inputs.userId, tournament: inputs.tournamentInstanceId});

    if(bids.length == 0 && cards.length == 0){
      //This user has played no bids or cards - grant them a refund!
      var transactions = await Transaction.find({player: inputs.userId, tournamentInstance: inputs.tournamentInstanceId, status: 'paid'});
      for(var i = 0; i < transactions.length; i++){
        var refund = transactions[i];
        refund.status = 'refunded';

        if(!refund.free){ //refund logic
          var tmpRefund = await Transaction.findOne({id: refund.id}).populate('player');
          //API call to playfab to trigger add tournamentInstance.cost
          res = await sails.helpers.refund.with({
            playfabId: tmpRefund.player.playfabId,
            cost: refund.credits,
            tournamentInstanceId: inputs.tournamentInstanceId,
          });
          //Error checking
          if(!res.response){
            console.log({code: 3029, message: "Error refunding"});
            //TODO!! Send email
            continue;
          }
          //Update Transaction with refundTransactionId that is returned
          await Transaction.update({id: refund.id}).set({
            refundId: res.message,
            status: refund.status,
            refundReason: 'No Bids or Cards Played by Player',
          })
        }
        else{ //just update status
          await Transaction.update({id: refund.id}).set({
            status: refund.status,
            refundReason: 'No Bids or Cards Played by Player',
          })
        }
      }
    }

    return exits.success(false);
  }


};

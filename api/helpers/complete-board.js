var _ = require('lodash');
var moment = require('moment');

module.exports = {


  friendlyName: 'Complete a board',


  description: 'Complete a specific board',


  inputs: {
    instanceId: {
      type: 'number',
      description: 'The board instance ID',
      required: true,
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    var boardInstance = await BoardResult.findOne({id: inputs.instanceId}).populate('owningTableInstance');

    //Calculate the score for a board
    var score = await sails.helpers.calculateScore(boardInstance.id);
    //Delete any scores that already exist for this board
    await Score.destroy({owningBoard: boardInstance.id});
    var scoreObj = await Score.create(score).fetch();

    await BoardResult.updateOne({id: inputs.instanceId}).set({
      done: true,
      score: scoreObj.id,
    });

    // console.log("Setting board complete");
    //Get all the equivalent boards
    var allEquivalentBoards = await BoardResult.find({owningTournament: boardInstance.owningTournament, overallBoardIndex: boardInstance.overallBoardIndex});
    //Update match points
    await sails.helpers.calculateMatchPoints(allEquivalentBoards);

    var tableInstance = await TableInstance.findOne({id: boardInstance.owningTableInstance.id}).populate('boardResults').populate('pairsPlaying').populate('owningRound');
    tableInstance = await sails.helpers.sortTablePairs(tableInstance);

    var tournamentInstance = await TournamentInstance.findOne({id: tableInstance.owningRound.owningTournament}).populate('basetournament');
    tournamentInstance = await sails.helpers.populateBaseTournament(tournamentInstance);

    //Check if this is the last round
    if(tableInstance.owningRound.roundNumber === tournamentInstance.numRounds){
      console.log("Is final round");

      var allTablesInst = await TableInstance.find({owningRound: tableInstance.owningRound.id})
      .populate('boardResults');

      //loop over ALL tableInstances
      var flag = true;

      for (var [key, val] of Object.entries(allTablesInst)) {
        if(val.boardResults[val.boardResults.length-1].done !== true){
          //check if last board.done = true
          flag = false;
          break;
        }
      }

      //If flag is true, means that all boardResults are done -> set tournamentInstance.proposedEndTime = currTime
      // Only if currTime isBefore end_time
      var currTime = moment.utc();

      if(flag && currTime.isBefore(moment.utc(tournamentInstance.proposedEndTime))){
        console.log("All boards complete for session");
        await TournamentInstance.updateOne({id: tournamentInstance.id}).set({
          proposedEndTime: currTime.format()
        });
      }
    }

    return exits.success();
  }


};

var _ = require('lodash');

module.exports = {


  friendlyName: 'Get Tournament Instance',


  description: 'Get a tournament instance by id, and merge the result with its parent tournament',


  inputs: {
    tournament: {
      type: 'ref',
      description: 'The tournament. MUST have basetournament populated!',
      required: true,
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    //NEW implementation where we're just passed the tournament object
    if(inputs.tournament === null){
      return exits.success(null);
    }

    inputs.tournament.basetournament = _.omit(inputs.tournament.basetournament, 'board');
    var tournament = inputs.tournament;

    var currInstanceWithoutBasetournament = _.omit(tournament, 'basetournament');
    tournament = _.mergeWith(
      {}, tournament.basetournament, currInstanceWithoutBasetournament,
      (a, b) => b === null ? a : undefined
    );

    return exits.success(tournament);
  }


};

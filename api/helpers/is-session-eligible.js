var _ = require('lodash');

module.exports = {


  friendlyName: 'Is Session Eligible',


  description: 'Checks whether a user is eligible to register for a session (e.g. is a member of a club that owns the session)',


  inputs: {
    userRDB: {
      type: 'string',
      description: 'The RDB of the user to check',
      required: true,
    },

    tournamentInstanceId: {
      type: 'number',
      description: 'The tournament instance ID to chek',
      required: true,
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    var user = await User.findOne({rdbNumber: inputs.userRDB});

    if(!user){
      return exits.success(false);
    }

    var tournamentInstance = await TournamentInstance.findOne({id: inputs.tournamentInstanceId}).populate('owningClubs');
    if(!tournamentInstance){
      return exits.success(false);
    }

    if(tournamentInstance.open){
      return exits.success(true);
    }else{
      //First, check if we own any of the clubs for this tournament
      for(var club of tournamentInstance.owningClubs){
        var isClubMember = await sails.helpers.isClubMember(inputs.userRDB, club.id);

        if(isClubMember){
          return exits.success(true);
        }
      }
    }

    //Next check if session director for the tournament instance specifically
    //TODO - This

    return exits.success(false);
  }


};

var _ = require('lodash');
var moment = require('moment');
moment().format();

module.exports = {


  friendlyName: 'Round Done',


  description:
  `Checks all current (ongoing) tournamentInstance rounds and finishes round if either last
  boardResults is done for that round OR current time is less than 30 seconds away from next round start time
  if !allBoardsComplete && allBoardsShouldBeComplete - complete board for that round (which can trigger tournament changing proposedEndTime and finishing the tournament)
  if !allboardsComplete && !allBoardsShouldBeComplete - not ready for next round
  else create call round-create`,


  inputs: {
    currTime: {
      required: false,
      type: 'string',
      description: 'Utilised for testing'
    }
  },


  exits: {

    success: {
      description: 'Returned all data / and populated round & tableInstances',
    },

    roundNotReady: {
      description: `The next round is not ready yet - due to endTime being more than 30 seconds away, and not all boards finished`,
      message: 'The next round is not ready yet - due to endTime being more than 30 seconds away, and not all boards finished'
    },

    notFound: {
      description: 'The specified resource was not found',
      message: 'The specified resource was not found'
    },

  },


  fn: async function (inputs, exits) {
    var tournamentStartTime;
    var endCheckTime;
    var currTime;

    if(inputs.currTime){
      currTime = moment.utc(inputs.currTime); //This is incredibly useful for testing purposes
      tournamentStartTime = moment.utc(inputs.currTime).add(5, 'minutes');
      endCheckTime = moment.utc(inputs.currTime).subtract(5, 'minutes');
    }else{
      currTime = moment.utc();
      tournamentStartTime = moment.utc().add(5, 'minutes');
      endCheckTime = moment.utc().subtract(5, 'minutes');
    }

    //Get ALL tournamentInstances in progress
    var inProgress = await TournamentInstance.find()
    .where({
      startDatetime: {'<=': tournamentStartTime.toISOString()},
      proposedEndTime: {'>=': endCheckTime.toISOString()}
    })
    .populate('rounds')
    .populate('pairs')
    .populate('basetournament')
    .populate('substitutionRequests')
    .populate('transactions');

    for(var c = 0; c < inProgress.length; c++){
      inProgress[c].basetournament = _.omit(inProgress[c].basetournament, 'board');
      var currInstanceWithoutBasetournament = _.omit(inProgress[c], ['basetournament', 'boardData']);
      var tmpRes = _.mergeWith(
        {}, inProgress[c].basetournament, currInstanceWithoutBasetournament,
        (a, b) => b === null ? a : undefined
      )
      inProgress[c] = tmpRes;

      //For each one we will go through this check
      var flag = true;
      var allBoardsComplete = true;
      var anyBoardsComplete = false;

      //Case of having rounds
      if(inProgress[c].rounds.length !== 0){
        flag = false;

        //Check if most recent round is done by going through all tableInstances in round and checking if last boardResults is done
        var mostRecentRound = inProgress[c].rounds[inProgress[c].rounds.length-1];
        var allTableInstances = await TableInstance.find({owningRound: mostRecentRound.id}).populate('boardResults').populate('pairsPlaying');

        var doneCheck = true;
        for(let x of allTableInstances){
          //console.log("Current table instance and last board done result", x, x.boardResults[x.boardResults.length-1].done);
          if(x.boardResults.length <= 0){
            console.log("ERROR: Attempting to check round done where we have no board results??");
            console.log("Something has gone wrong here!");
          }
          else if(x.boardResults[x.boardResults.length-1].done == false){
            console.log("Table " + x.id + " not done");
            allBoardsComplete = false;

            var isFullTable = await sails.helpers.isTableFull(x);
            if(!isFullTable){
              console.log("But it's not a full table");
            }else{
              doneCheck = false;
            }
          }else{
            anyBoardsComplete = true;
          }
        }

        if(doneCheck){
          flag = true;
        }

        if(!anyBoardsComplete){
          flag = false;
        }

        //Check if current time is past the start time for the round
        //Previous startTime adding projected time for that round
        var nextRoundStartTime = moment.utc(mostRecentRound.roundStartTime).add(inProgress[c].timeAllowedPerBoard*inProgress[c].boardsPerRound, 'seconds');
        if(nextRoundStartTime.diff(currTime, 'seconds') < 0){
          flag = true;
        }

        //If either currentTime is > 60 seconds away, OR all boards are not completed then next round is not ready
        if(!flag){
          console.log("Next round will not ready, either current time is NOT less than 60 seconds away from next round start time or most recent round is NOT done")
          continue;
        }

        //Complete all our boards
        if(!allBoardsComplete){
          //We haven't completed all boards for the round and the round is supposed to have ended - Go through and complete them
          console.log("Completing allBoardsForRound for ", inProgress[c].rounds[inProgress[c].rounds.length - 1].id)
          await sails.helpers.completeAllBoardsForRound(inProgress[c].rounds[inProgress[c].rounds.length - 1].id);
        }

        //If this is the final round we DON'T create new round
        if(inProgress[c].rounds.length === inProgress[c].numRounds){
          console.log("This was the final round - tournament is done");

          //Set all our active substitution requests to be inactive now
          await SubstituteRequest.update({targetTournament: inProgress[c].id}).set({status: 'inactive'});

          //Process refunds for people who were in a pair for this tournament but never played a bid or card
          //Get each user playing in a tournament
          for(var i = 0; i < inProgress[c].transactions.length; i++){
            var transaction = await Transaction.findOne({id: inProgress[c].transactions[i].id}).populate('player');

            //Don't perform a refund for a transaction with no player (this can happen for some of our tests)
            if(transaction.player){
              await sails.helpers.checkRefund(transaction.player.id, inProgress[c].id);
            }
          }

          continue;
        }
      }

      //Create a new round!
      var res = await sails.helpers.roundCreate.with({ tournamentInstance: inProgress[c], currTime: currTime.toISOString()})
      console.log("result is", res)
    }
    return exits.success();
  }
}

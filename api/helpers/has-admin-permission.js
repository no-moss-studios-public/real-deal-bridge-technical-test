var _ = require('lodash');

module.exports = {


  friendlyName: 'Is Admin',


  description: 'Checks whether a user is an admin',


  inputs: {
    userRDB: {
      type: 'string',
      description: 'The RDB of the user to check',
      required: true,
    },

    permission: {
      type: 'number',
      description: 'The permission to check. See Permissions model for list',
      required: true,
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    var user = await User.findOne({rdbNumber: inputs.userRDB}).populate('adminPermissions');

    if(!user){
      return exits.success(false);
    }

    for(var permission of user.adminPermissions){
      //Admins have all permissions
      if(permission.permission === 1){
        return exits.success(true);
      }

      if(permission.permission === inputs.permission){
        return exits.success(true);
      }
    }

    //Check whether the user is an admin or not
    //For now, everybody is one!!
    return exits.success(false);
  }


};

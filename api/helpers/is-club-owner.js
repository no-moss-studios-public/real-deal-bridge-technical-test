var _ = require('lodash');

module.exports = {


  friendlyName: 'Is Club Owner',


  description: 'Checks whether a user is an owner of a club',


  inputs: {
    userRDB: {
      type: 'string',
      description: 'The RDB of the user to check',
      required: true,
    },

    clubId: {
      type: 'number',
      description: 'The club ID we\'re checking against',
      required: true,
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    var user = await User.findOne({rdbNumber: inputs.userRDB});

    if(!user){
      return exits.success(false);
    }

    var club = await Club.findOne({id: inputs.clubId}).populate('owners');

    if(!club){
      return exits.success(false);
    }

    for(var owner of club.owners){
      if(owner.rdbNumber === inputs.userRDB){
        return exits.success(true);
      }

      /*
      else{
        console.log(owner.rdbNumber + " does not match " + inputs.userRDB);
      }
      */
    }

    return exits.success(false);
  }


};

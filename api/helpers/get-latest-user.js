var _ = require('lodash');

module.exports = {


  friendlyName: 'Get the latest user for a specific base user',


  description: 'Gets the latest user to be playing for a user (e.g. if they have any substitutes, return the latest substitute)',


  inputs: {
    userId: {
      type: 'number',
      description: 'The ID of the user we\'re getting',
      required: true,
    },

    tournamentInstanceId: {
      type: 'number',
      description: 'The ID of the tournament to be looking through',
      required: true,
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    var user = await User.findOne({where: {id: inputs.userId}, omit: ['password', 'emailAddress']});
    return exits.success(user);

    //We can tweak this if we want a more complicated substitute implementation
    /*
    var query =
      {
        tournamentInstance: inputs.tournamentInstanceId,
        or: [
            {player1RDB: user.rdbNumber},
            {player2RDB: user.rdbNumber}
        ],
      };

    var pair = await Pair.findOne(query).populate('substitutes');

    if(!pair){
      return exits.success(false);
    }

    if(pair.player1RDB === user.rdbNumber){

    }

    var latestPlayer = user;

    for(var i = 0; i < pair.substitutes.length; i++){
      pair.substitutes[i].substitutingUser = await User.findOne({where: {id: pair.substitutes[i].substitutingUser}, omit: ['password']});
      pair.substitutes[i].substitutedUser = await User.findOne({where: {id: pair.substitutes[i].substitutedUser}, omit: ['password']});

      if(pair.substitutes[i].substitutingUser.id == latestPlayer.id){
        //This is a substitute for the given user
        latestPlayer = pair.substitutes[i].substitutedUser;
      }
    }

    return exits.success(latestPlayer);
    */
  }


};

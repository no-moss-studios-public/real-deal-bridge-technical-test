module.exports = {

    attributes: {

      //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
      //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
      //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

      clubName: {
        type: 'string',
        required: true,
        example: 'Newcastle Bridge Club',
      },

      profileText: {
        type: 'string',
        required: false,
        columnType: 'text',
        example: 'We are the coolest Bridge Club',
      },

      clubImage: {
        type: 'ref',
        columnType: 'mediumblob',
        required: false,
        description: "The blob data for the image",
      },

      clubDetails: {
        type: 'string',
        required: false,
        description: "The details of the club",
        columnType: 'text'
      },

      status: {
        type: 'string',
        required: true,
        description: 'pending, active, inactive',
      },

      default: {
        type: 'boolean',
        required: false,
        defaultsTo: false,
        description: "Is this club a default club? If it is any new registered user will automatically join as a member"
      },

      abfNumber: {
        type: 'string',
        required: false,
        allowNull: true,
        description: "ABF Number of club",
      },

      //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
      //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
      //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


      //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
      //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
      //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

      //Many-to-many relationship with User table
      members: {
        collection: 'user',
        via: 'clubMembersOf',
        required: false,
      },

      //Many-to-many relationship with User table
      owners: {
        collection: 'user',
        via: 'clubOwnersOf',
        required: false,
      },

      //Many-to-many relationship with User table
      pending: {
        collection: 'user',
        via: 'clubPendingFor',
        required: false,
      },

      //Many-to-many relationship with User table
      mutedUsers: {
        collection: 'user',
        via: 'clubMutedIn',
        required: false,
      },

    },

  };

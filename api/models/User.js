module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
    emailAddress: {
      type: 'string',
      required: true,
      isEmail: true,
      maxLength: 200,
      example: 'mary.sue@example.com'
    },

    password: {
      type: 'string',
      required: true,
      description: 'Securely hashed representation of the user\'s login password.',
      protect: true,
      example: '2$28a8eabna301089103-13948134nad'
    },

    firstName: {
      type: 'string',
      required: true,
      description: 'First name of the user\'s name.',
      maxLength: 120,
      example: 'Mary'
    },

    lastName: {
      type: 'string',
      required: true,
      description: 'Last name of the user\'s',
      maxLength: 120,
      example: 'McHenst'
    },

    combinedName: {
      type: 'string',
      required: false,
      description: 'Combined name of the user',
      maxLength: 240,
      example: 'Mary McHenst'
    },

    overThirteen: {
      type: 'boolean',
      required: true,
      description: 'Stores if the user is over 13 years old'
    },

    dateOfBirth: {
      type: 'string',
      required: false,
      description: 'YYYY-MM-DD HH:mm:ss format of datetime storing the date of birth of user',
      defaultsTo: '1900-01-01 00:00:00',
      example: '2020-05-05 10:05:58'
    },

    rdbNumber: {
      type: 'string',
      required: true,
      unique: true,
      description: 'Real Deal Number of User',
    },

    acceptedTOS: {
      type: 'boolean',
      required: true,
      description: 'Stores if the user has accepted the terms of service'
    },

    acceptedPrivacy: {
      type: 'boolean',
      required: true,
      description: 'Stores if the user has accepted the privacy policy'
    },

    isBanned: {
      type: 'boolean',
      required: false,
      description: "Is true if the user is banned",
    },

    isMuted: {
      type: 'boolean',
      defaultsTo: false,
      description: "Is true if the user is globally muted",
    },

    bio: {
      type: 'string',
      required: false,
      description: 'A bio for the user',
    },

    resetPasswordToken: {
      type: 'string',
      required: false,
      description: 'Token which will be used in password forget flow, since its a JWT it will contain datetime expiry which can be used to check',
      allowNull: true
    },

    playfabId: {
      type: 'string',
      required: false,
      description: 'Unique playfabId used for payments',
      allowNull: true,
    },

    jwtIssueTime: {
      type: 'string',
      required: false,
      description: 'Will be used to enable invalidation of old login functionality',
      allowNull: true,
    },

    willSubstitute: {
      type: 'boolean',
      required: false,
      defaultsTo: true,
      description: 'Whether this user is marked for substitution'
    },


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    //One-to-one relationship with abf table
    abfNumber: {
      collection: 'abf',
      via: 'owner',
      required: false,
    },

    //One-to-one relationship with Profile Picture table
    profilePicture: {
      collection: 'profilepicture',
      via: 'owner',
      required: false,
    },

    //Many-to-many relationship with Club table
    clubMembersOf: {
      collection: 'club',
      via: 'members',
      required: false,
    },

    //Many-to-many relationship with Club table
    clubOwnersOf: {
      collection: 'club',
      via: 'owners',
      required: false,
    },

    //Many-to-many relationship with Club table
    clubPendingFor: {
      collection: 'club',
      via: 'pending',
      required: false,
    },

    //Many-to-many relationship with Club table
    clubMutedIn: {
      collection: 'club',
      via: 'mutedUsers',
      required: false,
    },

    //Add a reference to Permission (1:1) one User to 1 Permission
    adminPermissions: {
      collection: 'permissions',
      via: 'owningUser',
    },

    //Add a reference to Report (1:M) one User to many Reports
    reports: {
      collection: 'report',
      via: 'reportedUser',
    },

    //Add a collection to store muted users (M:M)
    mutedUsers:{
      collection: 'user'
    },

  },

};

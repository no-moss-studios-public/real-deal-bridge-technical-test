/**
 * LoginController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
module.exports = {

  friendlyName: 'Get User Profile',


  description: 'Get the user data for a user',


  extendedDescription:
`This action looks up the user for a given RDB number. Then, if it exists, returns it`,


  inputs: {
    rdbNum: {
      description: 'The RDB number for this user',
      type: 'string',
      required: true
    },
  },

  exits: {

    success: {
      description: 'The requesting user agent has been successfully logged in.',
    },

    badNumber: {
      description: `The provided Real Deal number doesn't match any user`,
      responseType: 'unauthorized',
      message: 'User does not exist'
    },

  },


  fn: async function (inputs, exits) {

    var omit = ['password'];

    var canSeeEmail = false;
    if(inputs.rdbNum == this.req.user.rdbNumber){
      canSeeEmail = true;
    }
    var isAdmin = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 1);
    if(isAdmin){
      canSeeEmail = true;
    }

    var clubUser = await User.findOne({where: {rdbNumber: inputs.rdbNum}}).populate('clubMembersOf').populate('clubPendingFor');
    var clubs = [];
    for(var i = 0; i < clubUser.clubMembersOf.length; i++){
      clubs.push(clubUser.clubMembersOf[i]);
    }
    for(var i = 0; i < clubUser.clubPendingFor.length; i++){
      clubs.push(clubUser.clubPendingFor[i]);
    }

    for(var i = 0; i < clubs.length; i++){
      var clubDirector = await sails.helpers.isClubOwner(this.req.user.rdbNumber, clubs[i].id);

      if(clubDirector){
        canSeeEmail = true;
      }
    }

    if(!canSeeEmail){
      omit.push('emailAddress');
    }

    var userRecord = await User.findOne({
      where: {rdbNumber: inputs.rdbNum},
      omit: omit,
    }).populate("abfNumber")
    .populate("adminPermissions")
    .populate("clubMembersOf")
    .populate("clubPendingFor")
    .populate("clubOwnersOf")
    .populate("clubMutedIn");

    if(!userRecord){
      return exits.badNumber({
        code: 1005,
        message: "User not found"
      });
    }

    return exits.success(
      userRecord
    );
  }

};

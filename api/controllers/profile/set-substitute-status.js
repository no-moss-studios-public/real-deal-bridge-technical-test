/**
 * LoginController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
module.exports = {

  friendlyName: 'Set substitute status',


  description: 'Set the substitute status for a user',


  extendedDescription:
`Set the mute status for a user`,


  inputs: {
    substituteStatus: {
      description: 'The new substitute status for the user',
      type: 'boolean',
      required: true
    }
  },

  exits: {

    success: {
      description: 'The substitute status has been updated',
    },

  },


  fn: async function (inputs, exits) {
    console.log("Marking user " + this.req.user.id + " for substitution: " + inputs.substituteStatus);

    await User.update({id: this.req.user.id}).set({
      willSubstitute: inputs.substituteStatus,
    });

    return exits.success({
      code: 200,
      message: "User substitute status successfully updated",
    });
  }

};

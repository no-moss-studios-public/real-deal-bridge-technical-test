module.exports = {

  friendlyName: 'Set mute status',


  description: 'Set the mute status for a user',


  extendedDescription:
`Set the mute status for a user`,


  inputs: {
    rdbNum: {
      description: 'The RDB number for the target user',
      type: 'string',
      required: true
    },

    muteStatus: {
      description: 'The new mute status for the user',
      type: 'boolean',
      required: true
    }
  },

  exits: {

    success: {
      description: 'The user has been muted',
    },

    badNumber: {
      description: `The provided Real Deal number doesn't match any user`,
      responseType: 'unauthorized',
      message: 'User does not exist'
    },

    unauthorized: {
      description: 'You are not authorized to set this permission',
      responseType: 'unauthorized',
      message: 'You are not authorized to set this permission',
    },

  },


  fn: async function (inputs, exits) {

    var canMuteUsers = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 1);

    if(!canMuteUsers){
        return exits.unauthorized({code: 1006, message: 'You are not authorized for this'});
    }

    var userRecord = await User.findOne({
      where: {rdbNumber: inputs.rdbNum},
      omit: ['password'],
    });

    if(!userRecord){
        return exits.badNumber({
          code: 1005,
          message: "User not found"
        });
    }

    await User.update({rdbNumber: inputs.rdbNum}).set({
      isMuted: inputs.muteStatus,
    });

    userRecord = await User.findOne({
      where: {rdbNumber: inputs.rdbNum},
      omit: ['password', 'emailAddress'],
    }).populate('adminPermissions');

    return exits.success({
      code: 200,
      message: "User mute status successfully updated",
      user: userRecord,
    });
  }

};

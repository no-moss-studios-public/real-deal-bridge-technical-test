/**
 * LoginController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
module.exports = {

  friendlyName: 'Set ban status',


  description: 'Set the ban status for a user',


  extendedDescription:
`Set the ban status for a user`,


  inputs: {
    rdbNum: {
      description: 'The RDB number for the target user',
      type: 'string',
      required: true
    },

    banStatus: {
      description: 'The new ban status for the user',
      type: 'boolean',
      required: true
    }
  },

  exits: {

    success: {
      description: 'The requesting user agent has been successfully logged in.',
    },

    badNumber: {
      description: `The provided Real Deal number doesn't match any user`,
      responseType: 'unauthorized',
      message: 'User does not exist'
    },

    unauthorized: {
      description: 'You are not authorized to set this permission',
      responseType: 'unauthorized',
      message: 'You are not authorized to set this permission',
    },

  },

/*
  Desired Functionality:
  - If a non-admin attempts to hit this endpoint, return code `1006`
  - If we attempt to ban/unban a non-existent user, return code `1005`
  - If we successfully ban/unban a user, we should change their `isBanned` status, before returning code `200`
  - In addition, if a user is banned, all reports that have been filed by them should be removed (to prevent report spamming from banned users).
*/

  fn: async function (inputs, exits) {
    return exits.success({
      code: 200,
      message: "User ban status successfully updated",
    });
  }

};

/**
 * LoginController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
module.exports = {

  friendlyName: 'Find User',


  description: 'Finds a user',


  inputs: {
    query: {
      description: 'The query string',
      type: 'string',
      required: true
    },
  },

  exits: {

    success: {
      description: 'The requesting user agent has been successfully logged in.',
    },

  },


  fn: async function (inputs, exits) {
    var users = [];

    if((inputs.query.match(new RegExp(' ', "g")) || []).length == inputs.query.length){
      //This string is just spaces
      return exits.success({
        users: users
      });
    }

    //First, finds user with specific RDB or ABF
    var result = await sails.helpers.getUser(inputs.query);
    if(result.result){
      users.push(result.user);

      return exits.success({
        users: users
      });
    }

    //Next, include users whose first name or last name matches the query
    users = await User.find({
      where: {
        or: [
          {combinedName: { contains: inputs.query }},
          {firstName: { contains: inputs.query }},
          {lastName: { contains: inputs.query }}
        ]
      },
      omit: ['emailAddress', 'password']
    }).limit(20);

    return exits.success({
      users: users
    });
  }

};

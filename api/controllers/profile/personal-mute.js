module.exports = {

  friendlyName: 'Personal Mute',


  description: 'Set the mute status for a user',


  extendedDescription:
`Set the personal mute status for a user`,


  inputs: {
    rdbNum: {
      description: 'The RDB number for the target user',
      type: 'string',
      required: true
    },

    muteStatus: {
      description: 'The new mute status for the user',
      type: 'boolean',
      required: true
    }
  },

  exits: {

    success: {
      description: 'The requesting user agent has been successfully logged in.',
    },

    badNumber: {
      description: `The provided Real Deal number doesn't match any user`,
      responseType: 'unauthorized',
      message: 'User does not exist'
    },

    unauthorized: {
      description: 'You are not authorized to set this permission',
      responseType: 'unauthorized',
      message: 'You are not authorized to set this permission',
    },

  },


  fn: async function (inputs, exits) {
    var userRecord = await sails.helpers.getUser(this.req.user.rdbNumber);
    userRecord = userRecord.user;

    var targetUser = await sails.helpers.getUser(inputs.rdbNum);
    if(targetUser.result){
      targetUser = targetUser.user;
    }else{
      return exits.badNumber({
        code: 1005,
        message: "User not found"
      });
    }

    if(inputs.muteStatus){
      //Mute a user
      await User.addToCollection(userRecord.id, 'mutedUsers', targetUser.id);

      return exits.success({
        message: 'Successfully muted user',
      });
    }else{
      //Unmute a user
      await User.removeFromCollection(userRecord.id, 'mutedUsers', targetUser.id);

      return exits.success({
        message: 'Successfully unmuted user',
      });
    }
  }

};

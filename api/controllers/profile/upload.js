/**
 * LoginController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var fs = require('fs');
var FileReader = require('filereader');
var File = require('File');
var FileAPI = require('file-api');

module.exports = {

  friendlyName: 'Upload Profile Picture',


  description: 'Upload a profile picture for the given user',


  extendedDescription:
'This endpoint uploads an image for a user with given number',


  inputs: {

    rdbNum: {
      description: 'The Real Deal number of the user',
      type: 'string',
      required: true
    },

  },


  exits: {

    success: {
      description: 'The requesting user agent has been successfully logged in.',
    },

    badNumber: {
      description: `The provided Real Deal number doesn't match any user`,
      responseType: 'unauthorized',
      message: 'User does not exist'
    },

    noFile: {
      description: `File wasn't passed with API call`,
      responseType: 'unauthorized',
      message: `File wasn't passed with API call`
    },

    tooManyFiles: {
      description: 'Too many files passed with API call',
      responseType: 'unauthorized',
      message: 'Too many files!'
    },

    tooLarge: {
      description: 'File was too large',
      responseType: 'unauthorized',
      message: 'File too large for upload'
    },

    notImage: {
      description: 'File was not a valid type',
      responseType: 'unauthorized',
      message: 'File was not an image',
    },

  },


  fn: async function (inputs, exits) {

    var userRecord = await User.findOne({
      rdbNumber: inputs.rdbNum
    });

    var isAdmin = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 1);

    if(!userRecord){
      return exits.badNumber({
        code: 1005,
        message: "User not found"
      });
    }

    if(!isAdmin && this.req.user.id != userRecord.id){
      return exits.badNumber({
        code: 1006,
        message: "You're not allowed to do this",
      })
    }

    //If we have a valid user, then we're good to upload the image
    this.req.file('image').upload({ maxBytes: 6000000 }, function (err, uploadedFiles) {
      if (err){
        console.log(err);
        return exits.tooLarge({
          code: 2003,
          message: "File is too large"
        });
      }

      if(uploadedFiles.length <= 0){
        return exits.noFile({
          code: 2004,
          message: "No file uploaded"
        });
      }

      if(uploadedFiles.length > 1){
        for(var i = 0; i < uploadedFiles.length; i++){
            fs.unlinkSync(uploadedFiles[i].fd);
        }
        return exits.tooManyFiles({
          code: 2006,
          message: "Too many files uploaded"
        });
      }

      var image = uploadedFiles[0];

      if(image.type !== 'image/jpeg'){

        //Delete the file we uploaded
        fs.unlinkSync(image.fd);

        return exits.notImage({
          code: 2005,
          message: "Filetype incorrect"
        });
      }

      //Read the image
      const reader = new FileReader();
      reader.readAsDataURL(new File(image.fd));

      reader.addEventListener('load', async function(event) {
        await ProfilePicture.create({
          owner: userRecord.id,
          image: event.target.nodeBufferResult,
        });

        //Delete the file we uploaded
        fs.unlinkSync(image.fd);

        return exits.success({
          message: 'Image uploaded successfully',
        });
      });

      reader.addEventListener('error', () => {
          //Delete the file we uploaded
          fs.unlinkSync(image.fd);

          return exits.notImage({
            code: 2007,
            message: "Unknown read error"
          });
      });
    });

  }

};

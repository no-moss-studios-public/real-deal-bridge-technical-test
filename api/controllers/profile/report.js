/**
 * LoginController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
module.exports = {

  friendlyName: 'Report User',


  description: 'Report a user',


  inputs: {
    uniqueNumber:{
      description: 'The RDB number or ABF number for the target user',
      type: 'string',
      required: true
    },

    reportReason: {
      description: 'The reason for the report',
      type: 'string',
      required: false
    },
  },

  exits: {

    success: {
      description: 'The requesting user agent has been successfully logged in.',
    },

    badNumber: {
      description: `The provided Real Deal number doesn't match any user`,
      responseType: 'unauthorized',
      message: 'User does not exist'
    },

  },


  fn: async function (inputs, exits) {
    var userRecord = await sails.helpers.getUser(inputs.uniqueNumber);

    if(!userRecord){
        return exits.badNumber({
          code: 1005,
          message: "User not found"
        });
    }

    var report = await Report.create({
      reportReason: inputs.reportReason,
      reportedUser: userRecord.user.id,
      reportingUser: this.req.user.id,
    }).fetch();


    return exits.success({
      code: 200,
      message: "User successfully reported",
      report: report,
    });
  }

};

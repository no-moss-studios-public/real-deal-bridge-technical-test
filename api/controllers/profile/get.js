/**
 * LoginController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  friendlyName: 'Get Profile Picture',


  description: 'Get the profile picture of a given user',


  extendedDescription:
`This action looks up the user for a given RDB number. Then, if that user exists, it gets the URL for the most recently uploaded image for that user, if it exists.`,


  inputs: {
    rdbNum: {
      description: 'The RDB number for this user',
      type: 'string',
      required: true
    },

    maxSize: {
      description: 'The maximum resolution for a square image',
      type: 'number',
      required: false
    }
  },

  exits: {

    success: {
      description: 'The requesting user agent has been successfully logged in.',
    },

    badNumber: {
      description: `The provided Real Deal number doesn't match any user`,
      responseType: 'unauthorized',
      message: 'User does not exist'
    },

    noImage:{
      description: `The user with the given Real Deal number doesn't have an associated image`,
      responseType: 'unauthorized',
      message: 'User has no image'
    },

  },


  fn: async function (inputs, exits) {
    console.log("Attempting to fetch image");

    var maxSize = 256;
    if(inputs.maxSize){
      maxSize = inputs.maxSize;
    }

    var userRecord = await User.findOne({
      rdbNumber: inputs.rdbNum
    })

    if(!userRecord){
      return exits.badNumber({
        code: 1005,
        message: "User not found"
      });
    }

    var imageRecord = await ProfilePicture.find({
      where: { owner: userRecord.id },
      sort: 'updatedAt DESC',
      limit: 1
    });

    if(imageRecord.length <= 0){
      return exits.noImage({
        code: 2001,
        message: "User has no image"
      });
    }

    //Scale the image to the appropriate max size
    try{
      var image = await sharp(imageRecord[0].image)
      .resize((maxSize, maxSize))
      .toFormat('jpeg')
      .toBuffer();

      return exits.success(
        image
      );
    }catch(err){
      console.log("Caught error: " + err);
      return exits.badNumber({code: 2001, message: "That club has an invalid profile picture"});
    }
  }

};

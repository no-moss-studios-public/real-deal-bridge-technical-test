/**
 * LoginController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
module.exports = {

  friendlyName: 'Remove admin permission',


  description: 'Remove an admin permission for a user',


  extendedDescription:
`Remove an admin permission for a user! See the Permissions model for permission information.`,


  inputs: {
    rdbNum: {
      description: 'The RDB number for the target user',
      type: 'string',
      required: true
    },

    adminPermission: {
      description: 'The permission being removed',
      type: 'number',
      required: true
    }
  },

  exits: {

    success: {
      description: 'The permission was removed',
    },

    badNumber: {
      description: `The provided Real Deal number doesn't match any user`,
      responseType: 'unauthorized',
      message: 'User does not exist'
    },

    unauthorized: {
      description: 'You are not authorized to set this permission',
      responseType: 'unauthorized',
      message: 'You are not authorized to set this permission',
    },

  },


  fn: async function (inputs, exits) {

    var canAddPermissions = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 1);

    if(!canAddPermissions){
        return exits.unauthorized({code: 1006, message: 'You are not authorized to set this permission'});
    }

    var userRecord = await User.findOne({
      where: {rdbNumber: inputs.rdbNum},
      omit: ['password'],
    });

    if(!userRecord){
        return exits.badNumber({
          code: 1005,
          message: "User not found"
        });
    }

    await Permissions.destroy({
      permission: inputs.adminPermission,
      owningUser: userRecord.id,
    });

    userRecord = await User.findOne({
      where: {rdbNumber: inputs.rdbNum},
      omit: ['password', 'emailAddress'],
    }).populate('adminPermissions');

    return exits.success({
      code: 200,
      user: userRecord,
    });
  }

};

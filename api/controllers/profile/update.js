/**
 * LoginController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  friendlyName: 'Update Profile',


  description: 'Update a users profile information',


  extendedDescription:
    'Updates a specified users information as given',


  inputs: {

    rdbNum: {
      description: 'The Real Deal number of the user to update',
      type: 'string',
      required: true
    },

    firstName: {
      description: 'The new first name',
      type: 'string',
      required: false,
    },

    lastName: {
      description: 'The new last name',
      type: 'string',
      required: false,
    },

    bio: {
      description: 'The new bio',
      type: 'string',
      required: false,
    },

    email: {
      description: 'The new email',
      type: 'string',
      required: false,
    },

    playfabId: {
      description: 'Playfab Unique ID',
      type: 'string',
      required: false,
    }

  },

  exits: {

    success: {
      description: 'The requesting user agent has been successfully logged in.',
    },

    badNumber: {
      description: `The provided Real Deal number doesn't match any user`,
      responseType: 'unauthorized',
      message: 'User does not exist'
    },

    unauthorized: {
      description: 'You are not authorized to set this permission',
      responseType: 'unauthorized',
      message: 'You are not authorized to set this permission',
    },

    maxUsersAssociatedWithEmail: {
      description: 'There are already 2 emails associated with same email',
      responseType: 'unauthorized',
    },

  },


  fn: async function (inputs, exits) {

    var isAdmin = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 1);

    if(!isAdmin && inputs.rdbNum != this.req.user.rdbNumber){
        return exits.unauthorized({code: 1006, message: 'You are not authorized for this'});
    }

    var user = await User.findOne({rdbNumber: inputs.rdbNum});

    if(!user){
      return exits.unauthorized({code: 1005, message: 'User not found'});
    }

    var itemsToUpdate = {};
    if(inputs.firstName){
      itemsToUpdate.firstName = inputs.firstName;
    }else{
      itemsToUpdate.firstName = user.firstName;
    }
    if(inputs.lastName){
      itemsToUpdate.lastName = inputs.lastName;
    }else{
      itemsToUpdate.lastName = user.lastName;
    }

    if(inputs.firstName || inputs.lastName){
      itemsToUpdate.combinedName = itemsToUpdate.firstName + " " + itemsToUpdate.lastName;
    }

    if(inputs.bio){
      itemsToUpdate.bio = inputs.bio;
    }
    if(inputs.email){
      itemsToUpdate.emailAddress = inputs.email;

      //Check that we don't already have 2 users with the same email
      var usersWithSameEmail = await User.find({
        where: {emailAddress: inputs.email.toLowerCase()},
        select: ['rdbNumber']
      });

      if(usersWithSameEmail.length >= 2){
        return exits.maxUsersAssociatedWithEmail({
          code: 1000,
          message: 'There are already 2 emails associated with same email'
        });
      }
    }
    if(inputs.playfabId){
      itemsToUpdate.playfabId = inputs.playfabId;
    }

    await User.update({rdbNumber: inputs.rdbNum}).set(itemsToUpdate);

    userRecord = await User.findOne({
      where: {rdbNumber: inputs.rdbNum},
      omit: ['password'],
    }).populate('adminPermissions').populate('abfNumber');

    return exits.success({
      code: 200,
      message: "User profile successfully updated",
      user: userRecord,
    });
  }

};

/**
 * PublicController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
 const fs = require('fs');
 const jsonfile = require('jsonfile');

module.exports = {

  friendlyName: 'Test Public response',


  description: '',


  inputs: {

  },


  exits: {

  },

  fn: async function () {
    // console.log(this.req);
    // All done.

    return {
      'success':'true',
      'ping':'pong',
      'info': sails.config.info,
      'date': this.req.headers.Date,
      'dateWithoutCapitalD': this.req.headers.date,
      'dateWithoutHeader': this.req.date,
      'dateWithoutHeaderandCapital': this.req.Date,
      'request': this.req.ip,
    };
  }
};

var _ = require('lodash');
var moment = require('moment');
moment().format();
const fastcsv = require('fast-csv');
const fs = require('fs');

module.exports = {

  friendlyName: 'Export club details & session details',

  description: 'Export club details & session details by creating CSV file, and sending it to email provided (only if they are an admin)',

  inputs: {

    email: {
      required: false,
      type: 'string',
      isEmail: true,
      description: 'The email address for the account, e.g. m@example.com. Used for testing purpose, otherwise will pull from this.req.user.emailAddress',
    },

    startDatetime: {
      type: 'string',
      required: true,
      description: "Set this according to filter"
    },

    endDatetime: {
      type: 'string',
      required: true,
      description: "Set this according to filter"
    }

  },

  exits: {

    success: {
      description: '',
    },

    invalid: {
      responseType: 'unauthorized',
    }
  },

  fn: async function (inputs, exits) {

    //Check if we are authorized
    var isAdmin = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 1);

    if(!isAdmin){
      return exits.invalid({code: 3009, message: "You cannot do this!"});
    }

    await sails.helpers.exportClubs(inputs.email, inputs.startDatetime, inputs.endDatetime)
    .intercept({code: 'invalid'}, (err) => {return {'invalid': {code: 1009, message: 'Email failed to send', err: err}}});

    return exits.success({code: 200, message: "sent email to " + inputs.email})
  }

};

var _ = require('lodash');

module.exports = {


  friendlyName: 'Resolve Report',


  description: 'Resolve a report',


  inputs: {
    reportId: {
      type: 'number',
      description: "The id of the report to resolve",
    }
  },

  exits: {
    success: {
      description: 'Returned all data',
    },

    unauthorized: {
      responseType: 'unauthorized',
      description: 'User forbidden',
    },
  },

  fn: async function (inputs, exits) {
    var canViewReports = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 1);

    if(!canViewReports){
      return exits.unauthorized({code: 1006, message: 'You can\'t do this'});
    }

    var report = await Report.findOne({id: inputs.reportId});
    if(!report){
      return exits.unauthorized({code: 1004, message: 'Report not found'});
    }

    report = await Report.updateOne({id: inputs.reportId}).set({
      resolved: true,
    })

    return exits.success({
      message: "Report updated",
      report: report
    });
  }

};

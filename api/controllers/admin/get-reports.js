var _ = require('lodash');

module.exports = {


  friendlyName: 'Get Reports',


  description: 'Get all unresolved reports',


  inputs: {

  },

  exits: {
    success: {
      description: 'Returned all data',
    },

    unauthorized: {
      responseType: 'unauthorized',
      description: 'User forbidden',
    },
  },

  fn: async function (inputs, exits) {
    var canViewReports = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 1);

    if(!canViewReports){
      return exits.unauthorized({code: 1006, message: 'You can\'t do this'});
    }

    var reports = await Report.find({resolved: false}).populate('reportedUser').populate('reportingUser');

    return exits.success({
      message: "Reports returned",
      reports: reports
    });

  }

};

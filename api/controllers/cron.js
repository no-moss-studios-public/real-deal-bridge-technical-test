/**
 * PrivateController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
 var moment = require('moment');
 moment().format();

module.exports = {

  friendlyName: 'Cron called by Lambda',


  description: 'Executes roundDone()',


  inputs: {

  },


  exits: {
    success: {

    },
  },

  fn: async function (exits) {
    console.log("Cron fired");

    await sails.helpers.roundDone();

    var email = process.env.EXPORT_EMAIL;
    var weeklyEmailDays = 7;
    var monthlyEmailDays = 30;

    var currTime = moment.utc();
    var data = await DataCall.findOne({id: 1});
    //Check our weekly data export
    var lastWeeklyCheck = moment.utc(data.weeklyExport);
    var nextWeeklyCheck = moment.utc(data.weeklyExport).add(weeklyEmailDays, 'days');
    if(nextWeeklyCheck.diff(currTime, 'seconds') < 0){
      //We need to do a new weekly export
      await DataCall.update({id: 1}).set({weeklyExport: nextWeeklyCheck.toISOString()});
      await sails.helpers.exportClubs(email, lastWeeklyCheck.toISOString(), nextWeeklyCheck.toISOString());
      console.log("Weekly email sent");
    }else{
      //Only do one at a time to not overload the poor server

      //Check our monthly data export
      var lastMonthlyCheck = moment.utc(data.monthlyExport);
      var nextMonthlyCheck = moment.utc(data.monthlyExport).add(monthlyEmailDays, 'days');

      if(nextMonthlyCheck.diff(currTime, 'seconds') < 0){
        //We need to do a new monthly export
        await DataCall.update({id: 1}).set({monthlyExport: nextMonthlyCheck.toISOString()});
        await sails.helpers.exportClubs(email, lastMonthlyCheck.toISOString(), nextMonthlyCheck.toISOString());
        console.log("Monthly email sent");
      }
    }

    return {
      "message": "Lambda fired successfully"
    }

  }
};

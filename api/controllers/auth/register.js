/**
 * RegisterController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var jwt = require('jsonwebtoken')
var _ = require('lodash');

module.exports = {

  friendlyName: 'Signup',

  description: 'Sign up for a new user account.',

  extendedDescription:
  `This creates a new user record in the database, signs in the requesting user agent
  by modifying its [session](https://sailsjs.com/documentation/concepts/sessions), and
  (if emailing with Mailgun is enabled) sends an account verification email.

  If a verification email is sent, the new user's account is put in an "unconfirmed" state
  until they confirm they are using a legitimate email address (by clicking the link in
  the account verification message.)`,

  inputs: {

    emailAddress: {
      required: true,
      type: 'string',
      isEmail: true,
      description: 'The email address for the new account, e.g. m@example.com.',
      extendedDescription: 'Must be a valid email address.',
    },

    password: {
      required: true,
      type: 'string',
      maxLength: 200,
      example: 'passwordlol',
      description: 'The unencrypted password to use for the new account.'
    },

    firstName:  {
      required: true,
      type: 'string',
      example: 'Frida',
      description: 'The user\'s first name.',
    },

    lastName:  {
      required: true,
      type: 'string',
      example: 'Kahlo',
      description: 'The user\'s last name.',
    },

    overThirteen: {
      required: true,
      type: 'boolean',
      description: 'Stores if the user is over 13 years old'
    },

    dateOfBirth: {
      type: 'string',
      required: false,
      description: 'YYYY-MM-DD HH:mm:ss format of datetime storing the date of birth of user',
    },

    abfNumber: {
      type: 'number',
      required: false,
      description: 'ABF or equivalent number',
    },

    country: {
      type: 'string',
      required: false,
      description: 'Country of origin for the unique number'
    },

    acceptedPrivacy: {
      required: true,
      type: 'boolean',
      description: 'The user is has not accepted Privacy agreement'
    },

    acceptedTOS: {
      required: true,
      type: 'boolean',
      description: 'The user is has not accepted TOS agreement'
    },

    sendEmail: {
      defaultsTo: true,
      type: 'boolean',
      description: 'Whether to send the welcome email or not'
    }

  },

  exits: {

    success: {
      description: 'New user account was created successfully.',
    },

    invalid: {
      responseType: 'unauthorized',
      description: 'The provided fullName, password and/or email address are invalid.',
      extendedDescription: 'If this request was sent from a graphical user interface, the request '+
      'parameters should have been validated/coerced _before_ they were sent.'
    },

    rdbAlreadyInUse: {
      description: 'Try again, the RDB generated is already in use',
      responseType: 'unauthorized',
    },

    unacceptedValuesForRegistration: {
      description: 'The user has not fulfilled requirements such as being over thirteen, accepted privacy or TOS',
      responseType: 'unauthorized',
    },

    abfAlreadyInUse: {
      description: 'The abf number already exists',
      responseType: 'unauthorized',
    },

    maxUsersAssociatedWithEmail: {
      description: 'There are already 2 emails associated with same email',
      responseType: 'unauthorized',
    }

  },

  fn: async function (inputs, exits) {
    var liveStatus = await Live.findOne({id: 1});
    if(!liveStatus.status){
      return exits.badCombo({code: 901, message: "Server is down for maintainance"});
    }

    if(inputs.overThirteen != true){
      return exits.unacceptedValuesForRegistration({
        code: 1001,
        message: 'The user is under the age requirements'
      })
    }

    if(inputs.acceptedPrivacy != true){
      return exits.unacceptedValuesForRegistration({
        code: 1001,
        message: 'The user has not accepted TOS'
      })
    }

    if(inputs.acceptedTOS != true){
      return exits.unacceptedValuesForRegistration({
        code: 1001,
        message: 'The user has not accepted privacy policy'
      })
    }

    //Check if Abf number is unique else throw abfAlreadyInUse
    if(inputs.abfNumber){
      abf = await Abf.findOne({
        uniqueNumber: inputs.abfNumber
      });
      if(abf != undefined){
        return exits.abfAlreadyInUse({
          code: 1002,
          message: 'A user has already registered with this ABF number'
        })
      }
    }

    var dob = "1900-01-01 00:00:00";
    if(inputs.dateOfBirth) dob =  inputs.dateOfBirth;

    //Checking if users exist in the database with same email
    var usersWithSameEmail = await User.find({
      where: {emailAddress: inputs.emailAddress.toLowerCase()},
      select: ['rdbNumber']
    });

    sails.log('There are %d users with same email.  Check it out:', usersWithSameEmail.length, usersWithSameEmail);

    emailIsUnique = true

    //There are users with the same email in the database, if greater than or equal to 2 throw maxUsersAssociatedWithEmail exit
    if(usersWithSameEmail.length >= 2){
      return exits.maxUsersAssociatedWithEmail({
        code: 1000,
        message: 'There are already 2 emails associated with same email'
      });
    }

    else if(usersWithSameEmail.length != 0){
      emailIsUnique = false
    }

    var result = await sails.helpers.generateRdb();
    var rdb = result.rdb;

    var combinedName = " ";
    if(inputs.firstName && inputs.lastName){
      combinedName = inputs.firstName + " " + inputs.lastName;
    }

    var newUserRecord = await User.create(_.extend({
      emailAddress: inputs.emailAddress,
      firstName: inputs.firstName,
      lastName: inputs.lastName,
      combinedName: combinedName,
      overThirteen: inputs.overThirteen,
      acceptedPrivacy: inputs.acceptedPrivacy,
      acceptedTOS: inputs.acceptedTOS,
      dateOfBirth: dob,
      bio: "A bridge player",
      rdbNumber: rdb,
      password: await sails.helpers.passwords.hashPassword(inputs.password),
      willSubstitute: true,
    }))
    .intercept({ code: 'E_UNIQUE' }, ()=> {return {'rdbAlreadyInUse': {code: 1003, message: 'Something went wrong, please try again'} }})
    .intercept({name: 'UsageError'}, 'invalid')
    .fetch();

    //Check again if abfNumber provided, this is guaranteed unique from check above.
    if(inputs.abfNumber){
      await Abf.create({
        uniqueNumber: inputs.abfNumber,
        // Set the User's Primary Key to associate the Abf with the User.
        owner: newUserRecord.id
      })
    }

    var clubs = await Club.find({ //Returns all default clubs
      default: true,
    });

    await User.addToCollection(newUserRecord.id, 'clubMembersOf') // Map function converting array of dictionary into array of their id's
    .members(clubs.map((element) => { //Adding the new registered user into these clubs
      return element.id;
    }));

    //Pull the full user, with populated records as required
    newUserRecord = await User.findOne({id: newUserRecord.id})
    .populate('abfNumber')
    .populate('adminPermissions')
    .populate('clubMembersOf')
    .populate('mutedUsers', {omit: ['emailAddress', 'password']});

    //Delete password before sending response
    delete newUserRecord.password;

    var strippedUser = await User.findOne({id: newUserRecord.id}).omit(['password']).populate('abfNumber');

    // after creating a user record, log them in at the same time by issuing their first jwt token and setting a cookie
    // The JWT stores the User object
    var token = jwt.sign({user: strippedUser}, process.env.JWT_SECRET, {expiresIn: sails.config.custom.jwtExpires})

    this.res.cookie('sailsjwt', token, {
      signed: true,
      // domain: '.yourdomain.com', // always use this in production to whitelist your domain
      maxAge: sails.config.custom.jwtExpires
    })

    //Send our email to our new user
    if(inputs.sendEmail){
      var options = {
        from: 'support@realdealbridge.com',
        to: newUserRecord.emailAddress,
        subject: 'Welcome to Real Deal Bridge!',
        template: 'welcome-email',
        context: {
            name: newUserRecord.firstName,
            rdbNumber: newUserRecord.rdbNumber
        }
      }

      var sent = await sails.helpers.sendEmail(options);
      if(!sent.success){
        console.log("Email validation failed to send for user: " + newUserRecord.id);
        console.log("Error: " + sent.message);
      }else{
        console.log("Email validation sent for user: " + newUserRecord.id);
      }
    }

    //Return relevant success message and rdbNumber
    return exits.success({
      message: 'new User created',
      emailIsUnique: emailIsUnique,
      user: newUserRecord,
      token
    })
  }

};

/**
 * RegisterController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var jwt = require('jsonwebtoken')
var _ = require('lodash');
var moment = require('moment');
moment().format();
let nodemailer = require('nodemailer');
let aws = require('aws-sdk');
var hbs = require('nodemailer-express-handlebars');

module.exports = {

  friendlyName: 'Signup',

  description: 'Sign up for a new user account.',

  extendedDescription:
  `This creates a new user record in the database, signs in the requesting user agent
  by modifying its [session](https://sailsjs.com/documentation/concepts/sessions), and
  (if emailing with Mailgun is enabled) sends an account verification email.

  If a verification email is sent, the new user's account is put in an "unconfirmed" state
  until they confirm they are using a legitimate email address (by clicking the link in
  the account verification message.)`,

  inputs: {

    p1: {
      required: true,
      type: 'string',
      maxLength: 200,
      example: 'passwordlol',
      description: 'The unencrypted password to use for the new account.'
    },

    p2: {
      required: true,
      type: 'string',
      maxLength: 200,
      example: 'passwordlol',
      description: 'The unencrypted password to use for the new account.'
    },

  },

  exits: {

    success: {
      description: 'New user account was created successfully.',
    },

    invalid: {
      responseType: 'unauthorized',
      description: 'The provided fullName, password and/or email address are invalid.',
      extendedDescription: 'If this request was sent from a graphical user interface, the request '+
      'parameters should have been validated/coerced _before_ they were sent.'
    },

  },

  fn: async function (inputs, exits) {

    //Parse referrer URL to get token
    var token = this.req.get('referrer');
    token = token.split("=")[1]

    if (!token) return exits.invalid({code: 401, message: "Unsupplied or incorrect JWT"})

    var user;
    // if there is something, attempt to parse it as a JWT token
    jwt.verify(token, process.env.JWT_RESET_SECRET, function(err, payload) {
      if (err || !payload.user) return exits.invalid({code: 401, message: "Unsupplied or incorrect JWT"})
      user = payload.user;
    })
    
    user = await User.findOne({id: user});

    if(user.resetPasswordToken !== token){
      return exits.invalid({code: 401, message: "Unsupplied or incorrect JWT"})
    }

    //We now update the PW and resetPasswordToken
    user = await User.updateOne({id: user.id}).set({
      password: await sails.helpers.passwords.hashPassword(inputs.p1),
      resetPasswordToken: ''
    })

    //Send confirmation email
    // create Nodemailer SES transporter
    let transporter = nodemailer.createTransport({
      debug: true,
      SES: new aws.SES({
        apiVersion: '2010-12-01'
      })
    });

    transporter.use('compile', hbs({
      viewEngine: {
        extname: '.hbs', // handlebars extension
        partialsDir: './views/templates',
        layoutsDir: './views/templates',
        defaultLayout: '',
      },
      viewPath: './views/templates',
      extName: '.hbs'
    }))

    //Format and send email
    transporter.sendMail({
        from: 'support@realdealbridge.com',
        to: user.emailAddress,
        subject: 'Your password has been successfully reset',
        template: 'reset-password-email',
        context: {
          name: user.firstName
        }
    }, (err, info) => {
      if(err){
        console.log(err);
        return exits.invalid({code: 1009, message: 'Email failed to send', err})
      }
      console.log(info.envelope);
      console.log(info.messageId);
    });

    //Return relevant success message and rdbNumber
    return this.res.redirect('/')

  }

};

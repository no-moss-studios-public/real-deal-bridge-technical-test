/**
 * LoginController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var jwt = require('jsonwebtoken')
var moment = require('moment');

module.exports = {

  friendlyName: 'Login',


  description: 'Log in using the provided email and password combination.',


  extendedDescription:
`This action attempts to look up the user record in the database with the
specified email address.  Then, if such a user exists, it uses
bcrypt to compare the hashed password from the database with the provided
password attempt.`,


  inputs: {

    uniqueNum: {
      description: 'The ABF number or rdbNumber',
      type: 'string',
      required: true
    },

    password: {
      description: 'The unencrypted password to try in this attempt, e.g. "passwordlol".',
      type: 'string',
      required: true
    },

    rememberMe: {
      description: 'Whether to extend the lifetime of the user\'s session.',
      extendedDescription:
`Note that this is NOT SUPPORTED when using virtual requests (e.g. sending
requests over WebSockets instead of HTTP).`,
      type: 'boolean'
    }

  },


  exits: {

    success: {
      description: 'The requesting user agent has been successfully logged in.',
    },

    badCombo: {
      description: `The provided Real Deal Number/ABF Number and password combination does not
      match any user in the database.`,
      responseType: 'unauthorized',
      message: 'The provided Real Deal Number/ABF Number and password combination are incorrect'
    }

  },


  fn: async function (inputs, exits) {
    var userRecord = await User.findOne({
      rdbNumber: inputs.uniqueNum
    });

    var abf = null;
    if(!userRecord){
      if(!isNaN(inputs.uniqueNum)){
        abf = await Abf.findOne({
          uniqueNumber: inputs.uniqueNum
        })
      }


      if(!abf){
        return exits.badCombo({
          code: 1005,
          message: 'Unique num doesn\'t match abf or rdb of any user'
        });
      }
      abf = await Abf.findOne({
        uniqueNumber: inputs.uniqueNum
      }).populate('owner');
      userRecord = abf.owner;
      if(!userRecord){
        return exits.badCombo({
          code: 1005,
          message: 'Unique num doesn\'t match abf or rdb of any user'
        });
      }
    }

    // If the password doesn't match, then also exit thru "badCombo".
    await sails.helpers.passwords.checkPassword(inputs.password, userRecord.password)
    .intercept({ code: 'incorrect' }, ()=> {return {'badCombo': {code: 1006, message: 'The provided Real Deal Number/ABF Number and password combination are incorrect'} }});

    if(userRecord.isBanned){
      return exits.badCombo({code: 1008, message: "User is banned" });
    }

    var isAdmin = await sails.helpers.hasAdminPermission(userRecord.rdbNumber, 1);

    var liveStatus = await Live.findOne({id: 1});
    if(!liveStatus.status && !isAdmin){
      return exits.badCombo({code: 901, message: "Server is down for maintainance"});
    }

    var jwtIssueTime = moment.utc().toISOString();

    //Update updatedAt time to now
    userRecord = await User.updateOne({id: userRecord.id})
    .set({
      updatedAt: new Date().getTime(),
      jwtIssueTime: jwtIssueTime //Set the current time as last issued token time
    });

    //Pull the full user, with populated records as required
    userRecord = await User.findOne({id: userRecord.id})
    .populate('abfNumber')
    .populate('adminPermissions')
    .populate('mutedUsers', {omit: ['emailAddress', 'password']})
    .populate('clubMembersOf')
    .populate('clubOwnersOf')
    .populate('clubMutedIn');

    var strippedUser = await User.findOne({id: userRecord.id}).omit(['password']).populate('abfNumber');

    //Delete password before sending response
    delete userRecord.password;

    // if no errors were thrown, then grant them a new token
    // set these config vars in config/local.js, or preferably in config/env/production.js as an environment variable
    // The JWT stores the User object
    var token = jwt.sign({user: strippedUser, jwtIssueTime: jwtIssueTime}, process.env.JWT_SECRET, {expiresIn: sails.config.custom.jwtExpires})

		// set a cookie on the client side that they can't modify unless they sign out (just for web apps)
		this.res.cookie('sailsjwt', token, {
			signed: true,
			// domain: '.yourdomain.com', // always use this in production to whitelist your domain
			maxAge: sails.config.custom.jwtExpires
		})
		// provide the token to the client in case they want to store it locally to use in the header (eg mobile/desktop apps)

    return exits.success({
      message: 'User logged in',
      user: userRecord,
      mutedUsers: userRecord.mutedUsers,
      token
    })
  }

};

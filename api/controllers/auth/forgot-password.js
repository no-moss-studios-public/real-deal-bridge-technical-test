// RDB/ABF and then email (handle case for 2 users associated with same email too)
// var token = jwt.sign({user: _.omit(newUserRecord, ['clubMembersOf'])}, process.env.JWT_SECRET, {expiresIn: sails.config.custom.jwtExpires})
var jwt = require('jsonwebtoken')
var _ = require('lodash');
var moment = require('moment');
moment().format();
let nodemailer = require('nodemailer');
let aws = require('aws-sdk');
var hbs = require('nodemailer-express-handlebars');

module.exports = {

  friendlyName: 'Forgot Password',

  description: 'Check if RDB/ABF or email exists then create JWT and store in User with expiry date of 1 day and email user the link which will feed to reset-password',


  inputs: {

    email: {
      required: false,
      type: 'string',
      description: 'The email address for the account, e.g. m@example.com.',
    },


    number: {
      type: 'number',
      required: false,
      description: 'ABF or RDB number',
    },

  },

  exits: {

    success: {
      description: 'New user account was created successfully.',
    },

    invalid: {
      responseType: 'unauthorized',
      description: 'The provided fullName, password and/or email address are invalid.',
      extendedDescription: 'If this request was sent from a graphical user interface, the request '+
      'parameters should have been validated/coerced _before_ they were sent.'
    },

  },

  fn: async function (inputs, exits) {

    if(!inputs.number && !inputs.email){
      return exits.invalid({
        code: 1007,
        message: 'Must supply either ABF/RDB or email'
      })
    }

    var user;
    var email;
    if(inputs.number){ //Number check flow
      user = await User.findOne({rdbNumber: inputs.number});
      if(!user){
        var tmp = await Abf.findOne({uniqueNumber: inputs.number}).populate('owner')
        if(!tmp || !tmp.owner){
          return exits.invalid({
            code: 1005,
            message: 'Unique num doesn\'t match abf or rdb of any user'
          })
        }
        user = tmp.owner;
      }
      email = user.emailAddress;
      //We should have user by here
    }
    else{ //Email check flow
      user = await User.find({emailAddress: inputs.email});
      if(user.length === 0){
        return exits.invalid({
          code: 1005,
          message: 'Email doesn\'t match abf or rdb of any user'
        })
      }
      email = user[0].emailAddress;
    }

    var template=''
    var context;

    if(Array.isArray(user)){
      var urlArray = []
      //Loop through max 2 users
      for(let u of user){
        var token = jwt.sign({user: u.id}, process.env.JWT_RESET_SECRET, {expiresIn: process.env.JWT_RESET_EXPIRY})
        urlArray.push(process.env.BASEURL + '/auth/reset-password?code=' + token);
        //Add to the DB
        await User.update({id: u.id}).set({
          resetPasswordToken: token
        })
      }
      if(user.length === 2){
        template = 'forgot-password-email-multiple'
        context = {
          name: user[0].firstName,
          rdb1: user[0].rdbNumber,
          url1: urlArray[0],
          rdb2: user[1].rdbNumber, //can't use email if user only has 1 email right now.
          url2: urlArray[1],
        }
      }
      else{
        template = 'forgot-password-email'
        context = {
          name: user[0].firstName,
          rdb: user[0].rdbNumber,
          url: urlArray[0],
        }
      }
    }
    else{
      var token = jwt.sign({user: user.id}, process.env.JWT_RESET_SECRET, {expiresIn: process.env.JWT_RESET_EXPIRY}) //1 day expiry (defined in .env)
      //Add to the DB
      await User.update({id: user.id}).set({
        resetPasswordToken: token
      })
      template = 'forgot-password-email';
      context = {
        name: user.firstName,
        rdb: user.rdbNumber,
        url: process.env.BASEURL + '/auth/reset-password?code=' + token
      }
    }

    // create Nodemailer SES transporter
    let transporter = nodemailer.createTransport({
      debug: true,
      SES: new aws.SES({
        apiVersion: '2010-12-01'
      })
    });

    transporter.use('compile', hbs({
      viewEngine: {
        extname: '.hbs', // handlebars extension
        partialsDir: './views/templates',
        layoutsDir: './views/templates',
        defaultLayout: '',
      },
      viewPath: './views/templates',
      extName: '.hbs'
    }))

    //Format and send email with url: 'http://{BASE_URL}/auth/reset-password?token=' + token
    transporter.sendMail({
        from: 'support@realdealbridge.com',
        to: email,
        subject: 'Your Real Deal Bridge account information',
        template: template,
        context: context
    }, (err, info) => {
      if(err){
        console.log(err);
        return exits.invalid({code: 1009, message: 'Email failed to send', err})
      }
      console.log(info.envelope);
      console.log(info.messageId);
    });

    //Return relevant success message and rdbNumber
    return exits.success({
      message: "sent email to " + email,
    })
  }

};

module.exports = {


    friendlyName: 'Set Club Mute Status',


    description: 'Set the club mute status of a user',


    inputs: {
      userId: {
        type: 'number',
        required: true,
        description: 'The id of the user being muted',
      },

      muteStatus: {
        type: 'boolean',
        required: true,
        description: 'The new mute status of the user'
      },

      clubId: {
        type: 'number',
        required: true,
        description: 'The club we are muting in',
      }
    },


    exits: {

      success: {
        description: 'Mute status updated',
      },

      invalid: {
        responseType: 'unauthorized',
      }

    },


    fn: async function (inputs, exits) {
      var isAdmin = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 1);
      var isClubOwner = await sails.helpers.isClubOwner(this.req.user.rdbNumber, inputs.clubId);

      if(!isAdmin && !isClubOwner){
        return exits.invalid({code: 1001, message: "You are not allowed to do this!"});
      }

      if(inputs.muteStatus){
        await Club.addToCollection(inputs.clubId, 'mutedUsers', inputs.userId);
      }else{
        await Club.removeFromCollection(inputs.clubId, 'mutedUsers', inputs.userId);
      }

      return exits.success({
        code: 200,
        message: 'Mute status updated',
      });
    }

  };

module.exports = {


  friendlyName: 'Remove a club owner from this club',


  description: 'Remove a club owner',


  inputs: {

    rdb: {
      required: true,
      type: 'string',
      description: 'The user you are adding as an owner',
    },

    clubId: {
      required: true,
      type: 'number',
      description: 'ID of specific club'
    },

  },


  exits: {

    success: {
      description: 'Returned all data',
    },

    badNumber: {
      description: `The provided id doesn't match any thing in the database`,
      responseType: 'unauthorized',
      message: 'ID is not valid'
    },

    notYourself: {
      description: `You cannot remove yourself as an owner`,
      responseType: 'unauthorized',
      message: 'You cannot remove yourself as an owner'
    },

    unauthorized: {
      description: 'You are not authorized to create this tournament',
      responseType: 'unauthorized',
      message: 'You are not authorized to create this tournament',
    },

    validationFailed: {
      description: 'Validation for this request has failed',
      responseType: 'unauthorized',
      message: 'That user is not an owner of that club',
    },

  },


  fn: async function (inputs, exits) {
    var tmp = await Club.findOne({id: inputs.clubId})
    .populate('members', {
      omit: ['password', 'emailAddress']
    })
    .populate('owners', {
      omit: ['password', 'emailAddress']
    });

    if(!tmp){
      return exits.badNumber({code: 1005, message: "The provided id doesn't match any thing in the database"});
    }

    if(inputs.rdb === this.req.user.rdbNumber){
      return exits.notYourself({code: 4002, message: 'You cannot remove yourself as an owner'});
    }

    var user = await User.findOne({rdbNumber: inputs.rdb});
    if(!user){
      return exits.badNumber({code: 3000, message: 'Supplied RDB doesn\'t exist'});
    }

    var isClubOwner = await sails.helpers.isClubOwner(this.req.user.rdbNumber, inputs.clubId);
    var isAdmin = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 1);

    if(!isClubOwner && !isAdmin){
      return exits.unauthorized({code: 4000, message: 'You are not authorized to remove owners from that club'});
    }

    var ownerFound = false;
    for(var owner of tmp.owners){
      if(owner.rdbNumber === inputs.rdb){
        ownerFound = true;
        break;
      }
    }

    if(!ownerFound){
        return exits.validationFailed({code: 4003, message: 'That user is not an owner of that club'});
    }

    await Club.removeFromCollection(inputs.clubId, 'owners', user.id);

    return exits.success({
      message: 'Successfully removed owner from club',
    });
  }

};

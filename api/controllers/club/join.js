module.exports = {


  friendlyName: 'Join a specific club',


  description: 'Join a specific club',


  inputs: {

    clubId: {
      required: true,
      type: 'number',
      description: 'ID of specific club'
    },

  },


  exits: {

    success: {
      description: 'Returned all data',
    },

    badNumber: {
      description: `The provided id doesn't match any thing in the database`,
      responseType: 'unauthorized',
      message: 'ID is not valid'
    },

  },


  fn: async function (inputs, exits) {

    var tmp = await Club.findOne({id: inputs.clubId})
    .populate('members', {
      omit: ['password', 'emailAddress']
    })
    .populate('pending', {
      omit: ['password', 'emailAddress']
    });

    if(!tmp){
      return exits.badNumber({code: 1005, message: "The provided id doesn't match any thing in the database"});
    }

    await Club.addToCollection(inputs.clubId, 'pending', this.req.user.id);

    return exits.success({
      code: 200,
      message: 'Successfully added user with id ' + this.req.user.id + ' RDB ' + this.req.user.rdbNumber + ' to club with ID ' + inputs.clubId
    });
  }

};

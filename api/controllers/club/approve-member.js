module.exports = {


  friendlyName: 'Approve a pending member for a club',


  description: 'Approve a club member',


  inputs: {

    rdb: {
      required: true,
      type: 'string',
      description: 'The user you are approving as a member',
    },

    clubId: {
      required: true,
      type: 'number',
      description: 'ID of specific club'
    },

  },


  exits: {

    success: {
      description: 'Returned all data',
    },

    unauthorized: {
      description: 'You are not authorized to approve this member',
      responseType: 'unauthorized',
      message: 'You are not authorized to approve this member',
    },

  },

  //This function approves a member who has applied to join a club
    //If the approving user is not an owner of this club or an admin, then return 4000 response code
    //If the applying user (passed in inputs.rdb) doesn't exist, then return 3000 response code
    //If the applying user (passed in inputs.rdb) has not applied for this club, then return 4003 response code
    //Otherwise, the user application is successful. Remove them from the 'pending' collection for the club, and add them to the 'members' collection for the club, and return response code 200

  //Tests for this functionality have been built, and can be found at `test/integration/controllers/club/club-approve.test.js`

  fn: async function (inputs, exits) {
    return exits.success({
      code: 200,
      message: 'Successfully approved member for club',
    });
  }

};

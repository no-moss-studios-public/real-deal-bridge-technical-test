var moment = require('moment');

module.exports = {


  friendlyName: 'Add a club owner to this club',


  description: 'Add a club owner',


  inputs: {

    rdb: {
      required: true,
      type: 'string',
      description: 'The user you are adding as an owner',
    },

    clubId: {
      required: true,
      type: 'number',
      description: 'ID of specific club'
    },

  },


  exits: {

    success: {
      description: 'Returned all data',
    },

    badNumber: {
      description: `The provided id doesn't match any thing in the database`,
      responseType: 'unauthorized',
      message: 'ID is not valid'
    },

    unauthorized: {
      description: 'You are not authorized to create this tournament',
      responseType: 'unauthorized',
      message: 'You are not authorized to create this tournament',
    },

    validationFailed: {
      description: 'Validation for this request has failed',
      responseType: 'unauthorized',
      message: 'That user is already an owner of that club',
    },

  },


  fn: async function (inputs, exits) {
    var isClubOwner = await sails.helpers.isClubOwner(this.req.user.rdbNumber, inputs.clubId);

    var canAddToAnyClub = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 1);

    if(!isClubOwner && !canAddToAnyClub){
      return exits.unauthorized({code: 4000, message: 'You are not authorized to add owners to that club'});
    }

    var tmp = await Club.findOne({id: inputs.clubId})
    .populate('members', {
      omit: ['password', 'emailAddress']
    })
    .populate('owners', {
      omit: ['password', 'emailAddress']
    })

    if(!tmp){
      return exits.badNumber({code: 1005, message: "The provided id doesn't match any thing in the database"});
    }

    var user = await sails.helpers.getUser(inputs.rdb);
    if(!user.result){
      return exits.badNumber({code: 3000, message: 'Supplied RDB doesn\'t exist'});
    }

    user = user.user;


    for(var owner of tmp.owners){
      if(owner.rdbNumber === user.rdbNumber){
        return exits.validationFailed({code: 4001, message: 'That user is already an owner of that club'});
      }
    }

    await Club.addToCollection(inputs.clubId, 'owners', user.id);

    //See if this person is already a member
    var clubMemberRdbs = tmp.members.map((member) => member.rdbNumber);
    if(!clubMemberRdbs.includes(inputs.rdb)){
      await Club.addToCollection(inputs.clubId, 'members', user.id);
    }

    return exits.success({
      code: 200,
      message: 'Successfully added owner to club',
    });
  }

};

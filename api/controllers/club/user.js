module.exports = {


    friendlyName: 'Get clubs that user is owner of and member of',


    description: 'Get clubs that user is owner of and member of',


    inputs: {


    },


    exits: {

      success: {
        description: 'Returned all data',
      },

    },


    fn: async function (inputs, exits) {

      var tmp = await User.findOne({id: this.req.user.id})
      .populate('clubMembersOf')
      .populate('clubOwnersOf')

      //Populate the members of these clubs
      for(var i = 0; i < tmp.clubMembersOf.length; i++){
        var club = await Club.findOne({id: tmp.clubMembersOf[i].id}).populate('members', {omit: ['password', 'emailAddress']});
        tmp.clubMembersOf[i].memberCount = club.members.length;
      }

      for(var i = 0; i < tmp.clubOwnersOf.length; i++){
        var club = await Club.findOne({id: tmp.clubOwnersOf[i].id}).populate('members', {omit: ['password', 'emailAddress']});
        tmp.clubOwnersOf[i].memberCount = club.members.length;
      }

      return exits.success({
        message: "For user with ID " + this.req.user.id + " and RDB " + this.req.user.rdbNumber + " results are",
        clubMembersOf: tmp.clubMembersOf,
        clubOwnersOf: tmp.clubOwnersOf
      })

    }

  };

module.exports = {


  friendlyName: 'Remove a club member from this club',


  description: 'Remove a club member',


  inputs: {

    rdb: {
      required: true,
      type: 'string',
      description: 'The user you are removing as a member',
    },

    clubId: {
      required: true,
      type: 'number',
      description: 'ID of specific club'
    },

  },


  exits: {

    success: {
      description: 'Returned all data',
    },

    badNumber: {
      description: `The provided id doesn't match any thing in the database`,
      responseType: 'unauthorized',
      message: 'ID is not valid'
    },

    notYourself: {
      description: `You cannot remove yourself as a member`,
      responseType: 'unauthorized',
      message: 'You cannot remove yourself as an member'
    },

    unauthorized: {
      description: 'You are not authorized to create this tournament',
      responseType: 'unauthorized',
      message: 'You are not authorized to create this tournament',
    },

    validationFailed: {
      description: 'Validation for this request has failed',
      responseType: 'unauthorized',
      message: 'That user is not a member of that club',
    },

  },


  fn: async function (inputs, exits) {
    var tmp = await Club.findOne({id: inputs.clubId})
    .populate('members', {
      omit: ['password', 'emailAddress']
    })
    .populate('owners', {
      omit: ['password', 'emailAddress']
    })
    .populate('pending', {
      omit: ['password', 'emailAddress']
    });

    if(!tmp){
      return exits.badNumber({code: 1005, message: "The provided id doesn't match any thing in the database"});
    }

    if(inputs.rdb === this.req.user.rdbNumber){
      return exits.notYourself({code: 4002, message: 'You cannot remove yourself as a member'});
    }

    var user = await User.findOne({rdbNumber: inputs.rdb});
    if(!user){
      return exits.badNumber({code: 3000, message: 'Supplied RDB doesn\'t exist'});
    }

    var isClubOwner = await sails.helpers.isClubOwner(this.req.user.rdbNumber, inputs.clubId);
    var isAdmin = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 1);
    if(!isClubOwner && !isAdmin){
      return exits.unauthorized({code: 4000, message: 'You are not authorized to remove members from that club'});
    }

    var memberFound = false;
    for(var member of tmp.members){
      if(member.rdbNumber === inputs.rdb){
        memberFound = true;
        break;
      }
    }

    var pendingFound = false;
    for(var pending of tmp.pending){
      if(pending.rdbNumber === inputs.rdb){
        pendingFound = true;
        break;
      }
    }

    if(memberFound){
      await Club.removeFromCollection(inputs.clubId, 'members', user.id);

      return exits.success({
        message: 'Successfully removed member from club',
      });
    }else if(pendingFound){
      await Club.removeFromCollection(inputs.clubId, 'pending', user.id);

      return exits.success({
        message: 'Successfully removed member from club',
      });
    }
    else{
      return exits.validationFailed({code: 4003, message: 'That user is not an member of that club'});
    }
  }

};

var fs = require('fs');
var FileReader = require('filereader');
var File = require('File');
var moment = require('moment');
moment().format();

module.exports = {


  friendlyName: 'Update Club',


  description: 'Updates a club',


  inputs: {
    clubId: {
        type: 'number',
        required: true,
        description: 'The ID of the club to update',
    },

    clubName: {
      type: 'string',
      required: false,
      description: 'Name of ',
      example: 'Newcastle Bridge Tournament',
    },

    profileText: {
      type: 'string',
      required: false,
      columnType: 'text',
      example: 'We are the coolest Bridge Club',
    },

    clubDetails: {
      type: 'string',
      required: false,
      description: "The details of the club",
      columnType: 'text',
    },

    abfNumber: {
      type: 'string',
      required: false,
      description: "ABF Club Number for Club"
    }

  },


  exits: {

    success: {
      description: 'New tournament was created successfully.',
    },

    tooManyFiles: {
      description: 'Too many files passed with API call',
      responseType: 'unauthorized',
      message: 'Too many files!'
    },

    tooLarge: {
      description: 'File was too large',
      responseType: 'unauthorized',
      message: 'File too large for upload'
    },

    unsupportedType: {
      description: 'File was not a valid type',
      responseType: 'unauthorized',
      message: 'File was not a binary file',
    },

    unauthorized: {
      description: 'unauthorized',
      responseType: 'unauthorized',
    },

  },


  fn: async function (inputs, exits) {
    var newClub = await Club.findOne({id: inputs.clubId});
    if(!newClub){
      return exits.unauthorized({code: 1006, message: 'This club doesn\'t exist'});
    }

    var canEditClub = await sails.helpers.isClubOwner(this.req.user.rdbNumber, newClub.id);
    var isAdmin = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 1);

    if(!canEditClub && !isAdmin){
      return exits.unauthorized({code: 1007, message: 'You are not authorized to do this'});
    }

    var fieldsToUpdate = {};

    if(inputs.clubName){
      fieldsToUpdate.clubName = inputs.clubName;
    }
    if(inputs.profileText){
      fieldsToUpdate.profileText = inputs.profileText;
    }
    if(inputs.clubDetails){
      fieldsToUpdate.clubDetails = inputs.clubDetails;
    }
    if(inputs.abfNumber){
      fieldsToUpdate.abfNumber = inputs.abfNumber;
    }

    await Club.update({id: newClub.id}).set(fieldsToUpdate);

    var newClub = await Club.findOne({id: newClub.id});

    //We've created a club, time to verify image (if exists) and then add to newClub
    this.req.file('image').upload({ maxBytes: 6000000 }, function (err, uploadedFiles) {
      if (err){
        console.log(err);
        return exits.tooLarge({code: 2003, message: "File is too large"});
      }

      if(uploadedFiles.length <= 0){ //Exit as success here!
        return exits.success({
          message: 'Club successfully updated',
          newClub
        });
      }

      if(uploadedFiles.length > 1){
        for(var i = 0; i < uploadedFiles.length; i++){
            fs.unlinkSync(uploadedFiles[i].fd);
        }
        return exits.tooManyFiles({code: 2006, message: "Too many files uploaded"});
      }

      var image = uploadedFiles[0];

      if(image.type !== 'image/jpeg'){
        //Delete the file we uploaded
        fs.unlinkSync(image.fd);
        return exits.unsupportedType({code: 2005, message: "Filetype incorrect"});
      }

      //Read the image
      const reader = new FileReader();
      reader.readAsDataURL(new File(image.fd));

      reader.addEventListener('load', async function(event) {
        newClub = await Club.update({id: newClub.id}).set({
          clubImage: event.target.nodeBufferResult
        }).fetch();

        //Delete the file we uploaded
        fs.unlinkSync(image.fd);

        return exits.success({
          message: 'New club successfully updated with image attached',
          newClub: newClub[0]
        });
      });

      reader.addEventListener('error', () => {
          //Delete the file we uploaded
          fs.unlinkSync(image.fd);
          return exits.unsupportedType({code: 2007, message: "Unknown read error"});
      });
    });

  }
};

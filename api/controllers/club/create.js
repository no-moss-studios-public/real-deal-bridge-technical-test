var fs = require('fs');
var FileReader = require('filereader');
var File = require('File');
var moment = require('moment');
moment().format();

module.exports = {


  friendlyName: 'Create tournament',


  description: 'Creates a tournament',


  inputs: {

    clubName: {
      type: 'string',
      required: true,
      description: 'Name of ',
      example: 'Newcastle Bridge Tournament'
    },

    profileText: {
      type: 'string',
      required: false,
      columnType: 'text',
      example: 'We are the coolest Bridge Club',
      defaultsTo: ''
    },

    clubDetails: {
      type: 'string',
      required: false,
      description: "The details of the club",
      columnType: 'text',
      defaultsTo: ''
    },

    default: {
      type: 'boolean',
      required: false,
      defaultsTo: false,
      description: "Is this club a default club? If it is any new registered user will automatically join as a member"
    },

    abfNumber: {
      type: 'string',
      required: false,
      defaultsTo: '',
      description: "ABF Club Number for Club"
    }

  },


  exits: {

    success: {
      description: 'New tournament was created successfully.',
    },

    tooManyFiles: {
      description: 'Too many files passed with API call',
      responseType: 'unauthorized',
      message: 'Too many files!'
    },

    tooLarge: {
      description: 'File was too large',
      responseType: 'unauthorized',
      message: 'File too large for upload'
    },

    unsupportedType: {
      description: 'File was not a valid type',
      responseType: 'unauthorized',
      message: 'File was not a binary file',
    },

    unauthorized: {
      description: 'unauthorized',
      responseType: 'unauthorized',
    },

  },


  fn: async function (inputs, exits) {

    var defaultStatus = false;
    var pendingStatus = 'pending';

    if(inputs.default){
      var canAddToAnyClub = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 1); //Check if user requesting this club is an admin
      if(!canAddToAnyClub){
        return exits.unauthorized({code: 1006, message: 'You are not authorized to add a default club'});
      }
      defaultStatus = true;
      pendingStatus = 'active';
    }

    var canCreateClub = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 2);
    var isAdmin = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 1);
    if(!canCreateClub && !isAdmin){
      return exits.unauthorized({code: 1006, message: 'You are not authorized to create a club'});
    }

    var newClub = await Club.create({
      clubName: inputs.clubName,
      profileText: inputs.profileText,
      clubDetails: inputs.clubDetails,
      default: defaultStatus,
      status: pendingStatus,
      abfNumber: inputs.abfNumber
    }).fetch();

    await Club.addToCollection(newClub.id, 'owners', this.req.user.id);
    await Club.addToCollection(newClub.id, 'members', this.req.user.id);

    //We've created a club, time to verify image (if exists) and then add to newClub
    this.req.file('image').upload({ maxBytes: 6000000 }, function (err, uploadedFiles) {
      if (err){
        console.log(err);
        return exits.tooLarge({code: 2003, message: "File is too large"});
      }

      if(uploadedFiles.length <= 0){ //Exit as success here!
        return exits.success({
          message: 'New club successfully created',
          newClub
        });
      }

      if(uploadedFiles.length > 1){
        for(var i = 0; i < uploadedFiles.length; i++){
            fs.unlinkSync(uploadedFiles[i].fd);
        }
        return exits.tooManyFiles({code: 2006, message: "Too many files uploaded"});
      }

      var image = uploadedFiles[0];

      if(image.type !== 'image/jpeg'){
        //Delete the file we uploaded
        fs.unlinkSync(image.fd);
        return exits.unsupportedType({code: 2005, message: "Filetype incorrect"});
      }

      //Read the image
      const reader = new FileReader();
      reader.readAsDataURL(new File(image.fd));

      reader.addEventListener('load', async function(event) {
        newClub = await Club.update({id: newClub.id}).set({
          clubImage: event.target.nodeBufferResult
        }).fetch();

        //Delete the file we uploaded
        fs.unlinkSync(image.fd);

        return exits.success({
          message: 'New club successfully created with image attached',
          newClub: newClub[0]
        });
      });

      reader.addEventListener('error', () => {
          //Delete the file we uploaded
          fs.unlinkSync(image.fd);
          return exits.unsupportedType({code: 2007, message: "Unknown read error"});
      });
    });

  }
};

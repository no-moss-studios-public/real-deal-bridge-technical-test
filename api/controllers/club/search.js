module.exports = {


    friendlyName: 'Find clubs based on their name',


    description: 'Find clubs based on their name',


    inputs: {
      query: {
        type: 'string',
        required: false,
        description: 'The query to search'
      }
    },


    exits: {

      success: {
        description: 'Returned all data',
      },

    },


    fn: async function (inputs, exits) {
      var query = { where: {} };

      var isAdmin = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 1);

      if(!isAdmin){
        query.where.status = "active";
      }

      if(inputs.query){
        query.where.clubName = { contains: inputs.query };
      }

      var clubs = await Club.find(query).populate('members', {omit: ['password', 'emailAddress']});
      for(var i = 0; i < clubs.length; i++){
        clubs[i].memberCount = clubs[i].members.length;
        clubs[i].members = null;
      }

      return exits.success({
        message: 'Clubs found',
        clubs: clubs,
      });
    }

  };

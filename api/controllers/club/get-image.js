var moment = require('moment');
moment().format();

module.exports = {


    friendlyName: 'Get the image of a club',


    description: 'Get the image of a club',


    inputs: {

      clubId: {
        required: false,
        type: 'number',
        description: 'ID of specific club'
      },

      maxSize: {
        description: 'The maximum resolution for a square image',
        type: 'number',
        required: false
      }

    },


    exits: {

      success: {
        description: 'Returned all data',
      },

      badNumber: {
        description: `The provided id doesn't match any thing in the database`,
        responseType: 'unauthorized',
        message: 'ID is not valid'
      },

    },


    fn: async function (inputs, exits) {
      var maxSize = 256;
      if(inputs.maxSize){
        maxSize = inputs.maxSize;
      }

      var club = await Club.findOne({id: inputs.clubId});

      if(!club){
        return exits.badNumber({code: 1001, message: "That club doesn't exist"});
      }

      if(!club.clubImage){
        return exits.badNumber({code: 2001, message: "That club has no profile picture"});
      }

      //Scale the image to the appropriate max size
      try{
        var image = await sharp(club.clubImage)
        .resize((maxSize, maxSize))
        .toFormat('jpeg')
        .toBuffer();

        return exits.success(
          image
        );
      }catch(err){
        console.log("Caught error: " + err);
        return exits.badNumber({code: 2001, message: "That club has an invalid profile picture"});
      }
    }

  };

var _ = require('lodash');
var moment = require('moment');
moment().format();
const fastcsv = require('fast-csv');
const fs = require('fs');

module.exports = {

  friendlyName: 'Export members of a club',

  description: 'Export members of a club by creating CSV file, and sending it to email provided (only if they are a club-owner/admin)',

  inputs: {

    email: {
      required: false,
      type: 'string',
      isEmail: true,
      description: 'The email address for the account, e.g. m@example.com. Used for testing purpose, otherwise will pull from this.req.user.emailAddress',
    },

    clubId: {
      type: 'number',
      required: true,
    },

  },

  exits: {

    success: {
      description: '',
    },

    invalid: {
      responseType: 'unauthorized',
    }
  },

  fn: async function (inputs, exits) {

    //Check if we are authorized
    var isAdmin = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 1);
    var isClubOwner = await sails.helpers.isClubOwner(this.req.user.rdbNumber, inputs.clubId);

    if(!isAdmin && !isClubOwner){
      return exits.invalid({code: 3009, message: "You cannot do this!"});
    }

    var club = await Club.findOne({id: inputs.clubId})
    .populate('members', { //Sort by ascending firstName
      sort: 'firstName ASC'
    })
    .populate('owners', { //Sort by ascending firstName
      sort: 'firstName ASC'
    })

    if(!club){
      return exits.invalid({code: 1006, message: "That club doesn't exist"});
    }

    //Loop through both members & owners --> create CSV from this
    var data = []
    for(member of club.members){
      //Get ABF Number
      var abfNumber = await Abf.findOne({owner: member.id})
      if(!abfNumber){
        abfNumber = ''
      }
      data.push({
        'First Name': member.firstName,
        'Last Name': member.lastName,
        'Email Address': member.emailAddress,
        'RDB Number': member.rdbNumber,
        'ABF Number': abfNumber.uniqueNumber,
      });
    }

    //Create CSV with members above
    var fileName = 'exportMembers-'+ club.id + '-' + moment.utc().toISOString() + ".csv";

    return new Promise(function(resolve, reject) {
      var existsAlready = fs.existsSync(fileName);
      var ws = fs.createWriteStream(fileName, {flags:'a'});
      var csvStream = fastcsv.write(data, {
          headers: !existsAlready,
          includeEndRowDelimiter: true,
      });
      csvStream
        .on('finish', function(err){
          if(err){
            reject(new Error("something went wrong with writing"))
          }
          resolve('done');
        })
        .pipe(fs.createWriteStream(fileName, {flags:'a'}));
    })
    .then( async () => { //Send the email here, only after the original CSV has been processed
      //Send email with CSV attached
      var email = this.req.user.emailAddress;
      if(inputs.email){
        email = inputs.email;
      }
      var options = {
        from: 'support@realdealbridge.com',
        to: email,
        subject: 'Real Deal Bridge Club Members Export',
        template: 'export-club-members-csv',
        context: {
          name: this.req.user.firstName,
          clubName: club.clubName
        },
        attachments: [
          {
            path: fileName
          }
        ]
      }
      var x = await sails.helpers.sendEmail(options)
      if(!x.success){
        throw new Error("Email failed to send");
      }
      return exits.success({
        message: "sent email to " + email,
      })
    })
    .catch( (err) => { //Send
      console.log(err);
      return exits.invalid({code: 1009, message: 'Email failed to send', error: err})
    })
    .finally(()=>{ //Finished up, delete file
      fs.unlinkSync(fileName);
    })

  }

};

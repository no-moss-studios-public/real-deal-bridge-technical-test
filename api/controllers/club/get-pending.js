var moment = require('moment');
moment().format();

module.exports = {


    friendlyName: 'Get pending clubs',


    description: 'Get a list of all pending clubs',


    inputs: {

    },


    exits: {

      success: {
        description: 'Returned all data',
      },

      unauthorized: {
        description: 'You are not authorized to create this tournament',
        responseType: 'unauthorized',
        message: 'You are not authorized to create this tournament',
      },
    },


    fn: async function (inputs, exits) {
      var isAdmin = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 1);

      if(!isAdmin){
        return exits.unauthorized({code: 1001, message: "You are not allowed to do this"});
      }

      var clubs = await Club.find({status: 'pending'})
      .populate('owners', {
        omit: ['password', 'emailAddress']
      })
      .populate('members', {
        omit: ['password', 'emailAddress']
      })
      .populate('pending', {
        omit: ['password', 'emailAddress']
      });

      return exits.success({clubs: clubs});
    }
};

module.exports = {


    friendlyName: 'Update the status of a club',


    description: 'Change the status of a club',


    inputs: {
      clubId: {
        required: true,
        type: 'number',
        description: 'ID of specific club'
      },

      newStatus: {
        required: true,
        type: 'string',
        description: 'The status you want to change this club to (pending / active / inactive)',
      },
    },


    exits: {

      success: {
        description: 'Returned all data',
      },

      badNumber: {
        description: `The provided id doesn't match any thing in the database`,
        responseType: 'unauthorized',
        message: 'The provided id doesn\'t match any thing in the database',
      },

      unauthorized: {
        description: 'You are not authorized to change status',
        responseType: 'unauthorized',
        message: 'Unauthorized',
      },

      invalidStatus: {
        description: 'That is not a valid status',
        responseType: 'unauthorized',
        message: 'That is not a valid status',
      },

    },


    fn: async function (inputs, exits) {
      var isAdmin = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 1);

      if(!isAdmin){
        return exits.unauthorized({code: 1006, message: 'Unauthorized'});
      }

      var acceptableStatuses = ['pending', 'active', 'inactive'];
      if(!acceptableStatuses.includes(inputs.newStatus)){
        return exits.invalidStatus({code: 4004, message: 'That is not a valid status'});
      }

      var club = await Club.findOne({id: inputs.clubId});
      console.log(club);
      if(!club){
        return exits.badNumber({code: 1005, message: "The provided id doesn't match any thing in the database"});
      }

      await Club.updateOne({id: inputs.clubId}).set({status: inputs.newStatus});
      club = await Club.findOne({id: inputs.clubId});

      return exits.success({
        message: 'Successfully updated status',
        club: club,
      })
    }

  };

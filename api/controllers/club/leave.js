module.exports = {


  friendlyName: 'Leave a specific club',


  description: 'Leave a specific club',


  inputs: {

    clubId: {
      required: true,
      type: 'number',
      description: 'ID of specific club'
    },

  },


  exits: {

    success: {
      description: 'Returned all data',
    },

    badNumber: {
      description: `The provided id doesn't match any thing in the database`,
      responseType: 'unauthorized',
      message: 'ID is not valid'
    },

  },


  fn: async function (inputs, exits) {

    var tmp = await Club.findOne({id: inputs.clubId})
    .populate('members', {
      omit: ['password', 'emailAddress']
    })
    .populate('pending', {
      omit: ['password', 'emailAddress']
    });

    if(!tmp){
      return exits.badNumber({code: 1005, message: "The provided id doesn't match any thing in the database"});
    }

    var memberFound = false;
    for(var member of tmp.members){
      if(member.rdbNumber === this.req.user.rdbNumber){
        memberFound = true;
        break;
      }
    }

    var pendingFound = false;
    for(var pending of tmp.pending){
      if(pending.rdbNumber === this.req.user.rdbNumber){
        pendingFound = true;
        break;
      }
    }

    if(memberFound){
      await Club.removeFromCollection(inputs.clubId, 'members', this.req.user.id);

      return exits.success({
        message: 'Successfully removed member from club',
      });
    }else if(pendingFound){
      await Club.removeFromCollection(inputs.clubId, 'pending', this.req.user.id);

      return exits.success({
        message: 'Successfully removed member from club',
      });
    }
    else{
      return exits.validationFailed({code: 4003, message: 'You are not an member of that club'});
    }
  }

};

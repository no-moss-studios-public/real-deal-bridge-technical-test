var moment = require('moment');
moment().format();

module.exports = {


    friendlyName: 'Get information about a specific club, or return all clubs',


    description: 'Get information about a specific club, or return all clubs',


    inputs: {

      clubId: {
        required: false,
        type: 'number',
        description: 'ID of specific club'
      },

    },


    exits: {

      success: {
        description: 'Returned all data',
      },

      badNumber: {
        description: `The provided id doesn't match any thing in the database`,
        responseType: 'unauthorized',
        message: 'ID is not valid'
      },

    },


    fn: async function (inputs, exits) {
      var result;
      if(inputs.clubId){
        //Only populate with upcoming tournament instances
        var isAdmin = await sails.helpers.hasAdminPermission(this.req.user.rdbNumber, 1);
        var isClubOwner = await sails.helpers.isClubOwner(this.req.user.rdbNumber, inputs.clubId);

        var omit = ['password'];
        if(!(isAdmin || isClubOwner)){
          omit.push('emailAddress');
        }

        result = await Club.findOne({id: inputs.clubId})
        .populate('owners', {
          omit: omit
        })
        .populate('members', {
          omit: omit
        })
        .populate('pending', {
          omit: omit
        });

        if(!result){
          return exits.badNumber({code: 1005, message: "The provided id doesn't match any thing in the database"});
        }
      }
      else{
        result = await Club.find()
        .populate('owners', {
          omit: ['password', 'emailAddress']
        })
        .populate('members', {
          omit: ['password', 'emailAddress']
        })
        .populate('pending', {
          omit: ['password', 'emailAddress']
        });
      }

      if(!result){
        return exits.badNumber({code: 1005, message: "The provided id doesn't match any thing in the database"});
      }

      return exits.success(
        result
      )
    }

  };

/**
 * isAuthenticated
 *
 * @module      :: Policy
 * @description :: Simple policy to require an authenticated user, or else redirect to login page
 *                 Looks for an Authorization header bearing a valid JWT token
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
var jwt = require('jsonwebtoken')

module.exports = async function(req, res, next) {
	sails.helpers.verifyToken(req)
	.switch({
		error: function(err) {
			return res.serverError(err)
		},
		invalid: function(err) {
      return res.unauthorized('Unsupplied or incorrect JWT');
		},
		banned: function (err) {
			return res.unauthorized({code: 1008, message: "User is banned" });
		},
		oldToken: function (err) {
			return res.unauthorized({code: 1010, message: "Current JWT is older than most recent JWT for user" });
		},
		serverNotLive: function (err) {
			return res.unauthorized({code: 901, message: "Server is currently undergoing maintenance, only admins are allowed" });
		},
		success: function() {
			// user has been attached to the req object (ie logged in) so we're set, they may proceed
			return next()
		}
	})
}


# Real Deal Bridge Technical Test

This is the repository for the Real Deal Bridge Technical Test.
To seee the list of tasks to be completed, check [this link](https://trello.com/b/2SXgeeRz/real-deal-bridge-technical-test).

a [Sails v1](https://sailsjs.com) application

## Installation
To install the repository, do the following:
1 - Clone the repository
2 - Ensure you have node.js installed. If you do not, you can do so following instructions at [this link](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm). This repository was generated and tested with Node vx.x.x, so it may be safest to install that version.
3 - Navigate to the directory you cloned the repository to, and run `npm install`.
4 - That's it! Try running `npm test` to ensure the installation has worked. Note that some tests will fail, as they are relating to functionality that has not yet been completed!

## Installation Errors
- If you are having any installation errors, feel free to reach out to reuben@nomoss.co and ask for help! We want to test your node.js / backend skill, not keep you stuck on installation errors with our repository.

## Running Tests
To run all tests, simply run `npm test` in your installation directory. Note that some tests will fail, as they are relating to functionality that has not yet been completed!

To run a specific test, simply run `node ./node_modules/mocha/bin/mocha --recursive test/lifecycle.test.js test/integration/[PATH]`, where `[PATH]` is the path to the test file in the integration folder (e.g. `node ./node_modules/mocha/bin/mocha --recursive test/lifecycle.test.js test/integration/controllers/profile/update.test.js`).

## Server Architecture
This section describes the basics of how the server works.

**Folder Structure**
The API endpoints have been created in the `api/controllers` folders. This is where you will need to change code to adjust endpoint functionality.

The tests are stored in the `test/integration` folder. This is where you will need to change code to adjust test functionality.

You should not need to change code outside of these two locations - all endpoints for new functionality have already been created and scaffolded, so you wont need to understand the full server architecture to get up and running.

**Endpoint Groups**
Admin - Endpoints that relate to admin specific permissions and capabilities
Auth - Endpoints relating to authentication
Club - Endpoints relating to clubs and club management
Profile - Endpoints relating to users and profile management

**General Structure**
While the full Real Deal Bridge repository has a lot to do with playing games, this simplified repository has stripped all of that out for simplicity. In this repository, the main structures are:
- *User* is somebody who has registered for the application. Users can join/leave clubs, and manage their profile information.
- *Club* is a group of users. Clubs are created by a user, who then is responsible for managing user membership.

## General Bridge Terminology and Rules
This is a list of general bridge terminology to help you get onboarded.

|Word|Meaning|
|--|--|
|Bidding|The phase of the game where the two teams have an auction of sorts, gambling points on whether they can achieve a specific number of tricks in a given trump suit|
|Board|A single hand (e.g. 13 cards) of bridge|
|Declarer|The player who made the first bid of a certain suit amongst the bid-winning team|
|Dummy|The partner of the declarer. Doesn't play their own cards (the declarer plays their hand for them|
|Opening Lead|The player who plays the first card at a board|
|Session|A bridge playing session|
|Trick|A single set of cards played by all 4 players (will contain 4 cards)|
|Trump Suit|A suit of cards that is "worth more" than other cards. Will beat a non-trump card in a trick. The trump suit is determined by the bidding.|

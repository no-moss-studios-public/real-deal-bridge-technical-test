// ./test/integration/models/Club.test.js

var util = require('util');

describe('Club (model)', function() {

  describe('#find()', function() {
    it('should return 1 default club', function (done) {
      Club.find({default: true})
      .then(function(clubs) {

        if (clubs.length !== 1) {
          return done(new Error(
            'Should return exactly 1 Club. ' +
            'But instead, got: '+util.inspect(clubs, {depth:null})+''
          ));
        }//-•
        return done();
      })
      .catch(done);
    });
  });

  /*
    //This test is no longer valid as we have a non-default club in our startup script
  describe('#find()', function() {
    it('all clubs in start up script must be default clubs', function (done) {
      Club.find()
      .then(function(clubs) {
        for(let c of clubs){
          if(c.default === false){
            return done(new Error(
              'All clubs MUST be default clubs, instead got: ' + c
            ));
          }
        }
        return done();
      })
      .catch(done);
    });
  });
  */

});

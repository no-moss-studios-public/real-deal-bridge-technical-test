// ./test/integration/models/User.test.js

var util = require('util');

describe('User (model)', function() {

  /*
    This test is a bit too strange to do properly
  describe('#find()', function() {
    it('should return all users', function (done) {
      User.find()
      .then(function(users) {

        if (users.length !== 66) {
          return done(new Error(
            'Should return exactly 66 users. ' +
            'But instead, got: '+util.inspect(users, {depth:null})+''
          ));
        }//-•

        return done();

      })
      .catch(done);
    });
  });
  */

  describe('#findOne({rdbNumber: 0})', function() {
    it('should return 1 user with name admin', function (done) {
      User.findOne({rdbNumber: 0})
      .then(function(user) {
        if(!user){
          return done(new Error(
            'The admin user does not exist, when they should'
          ));
        }

        if(user.firstName !== 'Real'){
          return done(new Error(
            'The rdb user exists, but their name is not "Real", instead it is ' + user.firstName
          ));
        }

        return done();

      })
      .catch(done);
    });
  });

});

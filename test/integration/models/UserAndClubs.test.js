// ./test/integration/models/UserAndClubs.test.js

describe('User and Clubs (model)', function() {

  describe('#find()', function() {
        
    it('all 65 users must be part of all default clubs', async function () {  
      var clubsWithUsers = await Club.find().populate('members');
      for(let c of clubsWithUsers){
        if(c.members.length !== 65){
          return new Error(
            'There exists a default club without all memebers as seen with ', + c
          );
        }
      }
      return;
    });

  });

});
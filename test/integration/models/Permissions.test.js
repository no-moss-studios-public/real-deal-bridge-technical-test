// ./test/integration/models/Permissions.test.js

var util = require('util');

describe('Permissions (model)', function() {

  describe('#find()', function() {
    it('should return 1 admin', function (done) {
      Permissions.find()
      .then(function(admins) {

        if (admins.length !== 1) {
          return done(new Error(
            'Should return exactly 1 admin (user with permissions). ' +
            'But instead, got: '+util.inspect(admins, {depth:null})+''
          ));
        }//-•

        return done();

      })
      .catch(done);
    });
  });

});
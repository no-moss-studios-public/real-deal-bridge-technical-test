// test/integration/controllers/club.test.js
var supertest = require('supertest');
var expect = require('chai').expect;
var moment = require('moment');
moment().format();

var agent;
var newClub;
var club;

describe('Clubs', function() {
  before( async function() {
    agent = supertest.agent(sails.hooks.http.app);
  })

  describe('Create club flow', function (){
    it('create a normal club', async function () {
      var tmp = await agent
      .post('/api/club/create')
      .set('authorization', 'Bearer ' + tokens[0])
      .query({clubName: 'New Club', profileText: 'Sample text', default: false, clubDetails: 'Some details'})
      expect(tmp.body).to.have.property('message', 'New club successfully created')
      expect(tmp.body.newClub).to.have.property('status', 'pending')
      expect(tmp.body.newClub).to.have.property('default', false)
      newClub = tmp.body.newClub;
    })
  })

  describe('Approval flow', function() {
    it('User can apply to join', async function () {
      //Have user with RDB 15 apply to join
      var res = await agent
      .post('/api/club/join')
      .set('authorization', 'Bearer ' + tokens[15])
      .query({clubId: newClub.id})

      expect(res.body).to.have.property('code', 200);

      var user = await User.findOne({rdbNumber: "15"}).populate('clubPendingFor').populate('clubMembersOf');

      expect(user.clubPendingFor).to.have.length(1);
      expect(user.clubPendingFor[0]).to.have.property("id", newClub.id);
      expect(user.clubMembersOf).to.have.length(1);

      var club = await Club.findOne({id: newClub.id}).populate('pending').populate('members');
      expect(club.pending).to.have.length(1);
      expect(club.members).to.have.length(1);

      //Have user with RDB 16 apply to join
      res = await agent
      .post('/api/club/join')
      .set('authorization', 'Bearer ' + tokens[16])
      .query({clubId: newClub.id})

      expect(res.body).to.have.property('code', 200);

      user = await User.findOne({rdbNumber: "16"}).populate('clubPendingFor').populate('clubMembersOf');

      expect(user.clubPendingFor).to.have.length(1);
      expect(user.clubPendingFor[0]).to.have.property("id", newClub.id);
      expect(user.clubMembersOf).to.have.length(1);

      club = await Club.findOne({id: newClub.id}).populate('pending').populate('members');
      expect(club.pending).to.have.length(2);
      expect(club.members).to.have.length(1);

      //Have us add an owner
      res = await agent
      .post('/api/club/add-owner')
      .set('authorization', 'Bearer ' + tokens[0])
      .query({clubId: newClub.id, rdb: 1})
      expect(res.body).to.have.property('code', 200);

      club = await Club.findOne({id: newClub.id}).populate('owners');
      expect(club.owners).to.have.length(2);
    })

    it('Non-admin cannot approve user', async function () {
      var res = await agent
      .post("/api/club/approve-member")
      .set('authorization', 'Bearer ' + tokens[4])
      .query({rdb: "15", clubId: newClub.id});

      expect(res.body).to.have.property('code', 4000)
    })

    it('Admin cannot approve user who doens\'t exist', async function() {
      var res = await agent
      .post("/api/club/approve-member")
      .set('authorization', 'Bearer ' + tokens[0])
      .query({rdb: "31243214123512345", clubId: newClub.id});

      expect(res.body).to.have.property('code', 3000)
    })

    it('Admin cannot approve user who isn\'t applying to club', async function() {
        var res = await agent
        .post("/api/club/approve-member")
        .set('authorization', 'Bearer ' + tokens[0])
        .query({rdb: "4", clubId: newClub.id});

        expect(res.body).to.have.property('code', 4003)
    })

    it('Admin can approve user who is applying', async function(){
        var res = await agent
        .post("/api/club/approve-member")
        .set('authorization', 'Bearer ' + tokens[0])
        .query({rdb: "15", clubId: newClub.id});

        expect(res.body).to.have.property('code', 200)

        //Confirm the user is part of the club
        var user = await User.findOne({rdbNumber: "15"}).populate('clubPendingFor').populate('clubMembersOf');
        expect(user.clubPendingFor).to.have.length(0);
        expect(user.clubMembersOf).to.have.length(2);
        expect(user.clubMembersOf[1]).to.have.property("id", newClub.id);

        var club = await Club.findOne({id: newClub.id}).populate('pending').populate('members');
        expect(club.pending).to.have.length(1);
        expect(club.members).to.have.length(3);
    })

    it('Club owner can approve user who is applying', async function(){
        var res = await agent
        .post("/api/club/approve-member")
        .set('authorization', 'Bearer ' + tokens[1])
        .query({rdb: "16", clubId: newClub.id});

        expect(res.body).to.have.property('code', 200)

        //Confirm the user is part of the club
        var user = await User.findOne({rdbNumber: "16"}).populate('clubPendingFor').populate('clubMembersOf');
        expect(user.clubPendingFor).to.have.length(0);
        expect(user.clubMembersOf).to.have.length(2);
        expect(user.clubMembersOf[1]).to.have.property("id", newClub.id);

        var club = await Club.findOne({id: newClub.id}).populate('pending').populate('members');
        expect(club.pending).to.have.length(0);
        expect(club.members).to.have.length(4);
    })
  })

  after(async function(){
    await Club.destroyOne({id: newClub.id});
  })
})

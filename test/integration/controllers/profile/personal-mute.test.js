// test/integration/controllers/tournament.test.js
var supertest = require('supertest');
var expect = require('chai').expect;
var moment = require('moment');
moment().format();

var agent;
var tournament;
var round;
var board;
var tableInst;


describe('personal-mute', function() {
  this.timeout(10000)

  before( async function() {
      agent = supertest.agent(sails.hooks.http.app);
  });

  it('can mute user', function (done) {
    agent
    .post('/api/profile/personal-mute')
    .set('authorization', 'Bearer ' + tokens[1])
    .field('rdbNum', 2)
    .field('muteStatus', 'true')
    .end((err, res) => {
      expect(err).to.be.null

      done();
    });
  });

  it('mutes should be 1', function (done) {
    agent
    .post('/api/auth/login')
    .send({ uniqueNum: 1, password: 1 })
    .expect(200)
    .end((err, res) => {
      expect(err).to.be.null

      expect(res.body).to.have.property('user');
      expect(res.body.user).to.have.property('mutedUsers');
      expect(res.body.user.mutedUsers.length).to.equal(1);

      done();
    });
  });

  it('can unmute user', function (done) {
    agent
    .post('/api/profile/personal-mute')
    .set('authorization', 'Bearer ' + tokens[1])
    .field('rdbNum', 2)
    .field('muteStatus', 'false')
    .end((err, res) => {
      expect(err).to.be.null

      done();
    });
  });

  it('mutes should be 0', function (done) {
    agent
    .post('/api/auth/login')
    .send({ uniqueNum: 1, password: 1 })
    .expect(200)
    .end((err, res) => {
      expect(err).to.be.null

      expect(res.body).to.have.property('user');
      expect(res.body.user).to.have.property('mutedUsers');
      expect(res.body.user.mutedUsers.length).to.equal(0);

      done();
    });
  });

  after(async function(){

  });
})

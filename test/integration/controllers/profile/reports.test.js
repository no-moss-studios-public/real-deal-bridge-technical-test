// test/integration/controllers/tournament.test.js
var supertest = require('supertest');
var expect = require('chai').expect;
var moment = require('moment');
moment().format();

var agent;

var reportId;

describe('reports', function() {
  before(function(done) {
    agent = supertest.agent(sails.hooks.http.app);
    done();
  })

  describe('Checking Reporting', function() {

    it('should be able to report a user', function (done) {
      agent
      .post('/api/profile/report')
      .set('authorization', 'Bearer ' + tokens[1])
      .field('uniqueNumber', 2)
      .field('reportReason', 'Testing the reports')
      .end((err, res) => {
        expect(err).to.be.null
        expect(res).to.have.property('status', 200)

        done();
      });
    });

    it('report should have been created', async function() {
      var report = await Report.findOne({reportReason: 'Testing the reports'});

      expect(report).not.to.be.null;
      expect(report).to.have.property('resolved', false);
      reportId = report.id;
    });

    it('report should appear in list', function(done) {
      agent
      .get('/api/admin/get-reports')
      .set('authorization', 'Bearer ' + tokens[0])
      .end((err, res) =>{
        expect(err).to.be.null;
        expect(res).to.have.property('status', 200);

        var reports = res.body.reports;
        expect(reports.length).to.eql(1);

        var report = reports[0];
        expect(report.resolved).to.eql(false);
        expect(report.reportingUser.rdbNumber).to.eql('1');
        expect(report.reportedUser.rdbNumber).to.eql('2');

        done();
      });
    });

  });

  describe('Check Resolve Report', function() {
    it('should be able to resolve report', function (done){
      agent
      .post('/api/admin/resolve-report')
      .set('authorization', 'Bearer ' + tokens[0])
      .field('reportId', reportId)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.property('status', 200);
        done();
      });
    });

    it('report list should be empty', function(done) {
      agent
      .get('/api/admin/get-reports')
      .set('authorization', 'Bearer ' + tokens[0])
      .end((err, res) =>{
        expect(err).to.be.null;
        expect(res).to.have.property('status', 200);

        var reports = res.body.reports;
        expect(reports.length).to.eql(0);

        done();
      });
    });
  });

  after( function(done) {
    return done();
  });

});

// test/integration/controllers/tournament.test.js
var supertest = require('supertest');
var expect = require('chai').expect;

var agent;


describe('mute', function() {
  this.timeout(10000);

  before( async function() {
      agent = supertest.agent(sails.hooks.http.app);
  });

  it('non-admins cannot mute users', function (done) {
    agent
    .post('/api/profile/set-mute-status')
    .set('authorization', 'Bearer ' + tokens[1])
    .field('rdbNum', "2")
    .field('muteStatus', true)
    .end((err, res) => {
      expect(err).to.be.null
      expect(res.body).to.have.property('code', 1006);
      done();
    });
  });

  it('admins can mute users', function (done) {
    agent
    .post('/api/profile/set-mute-status')
    .set('authorization', 'Bearer ' + tokens[0])
    .field('rdbNum', "2")
    .field('muteStatus', true)
    .end((err, res) => {
      expect(err).to.be.null
      expect(res.body).to.have.property('user');
      done();
    });
  });
  it('user should be muted', async function () {
    var user = await User.findOne({rdbNumber: '2'});

    expect(user).to.not.be.null;
    expect(user).to.have.property('isMuted', true);
  });
  it('admins can unmute users', function (done) {
    agent
    .post('/api/profile/set-mute-status')
    .set('authorization', 'Bearer ' + tokens[0])
    .field('rdbNum', "2")
    .field('muteStatus', false)
    .end((err, res) => {
      expect(err).to.be.null
      expect(res.body).to.have.property('user');
      done();
    });
  });
  it('user should be unmuted', async function () {
    var user = await User.findOne({rdbNumber: '2'});

    expect(user).to.not.be.null;
    expect(user).to.have.property('isMuted', false);
  });
  it('admins cant mute users that dont exist', function (done) {
    agent
    .post('/api/profile/set-mute-status')
    .set('authorization', 'Bearer ' + tokens[0])
    .field('rdbNum', "21324513690139069061906390609639036")
    .field('muteStatus', false)
    .end((err, res) => {
      expect(err).to.be.null
      expect(res.body).to.have.property('code', 1005);
      done();
    });
  });

  after(async function(){

  });
})

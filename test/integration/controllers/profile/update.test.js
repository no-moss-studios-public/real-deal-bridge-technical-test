var supertest = require('supertest');
var expect = require('chai').expect;

var agent;
var user;

describe('update', function() {
  before( async function() {
    agent = supertest.agent(sails.hooks.http.app);
  });

  it('non-admins cannot update profile (if not the user themselves)', function (done) {
    agent
    .post('/api/profile/update')
    .set('authorization', 'Bearer ' + tokens[1])
    .field('rdbNum', "2")
    .field('firstName', 'TESTBOT2')
    .field('email', 'test@nomoss.co')
    .end((err, res) => {
      expect(err).to.be.null
      expect(res.body).to.have.property('code', 1006);
      done();
    });
  });

  it('can\'t update profile of user that doesn\'t exist', function (done) {
    expect(true).to.equal(true);
    done();
  });

  it('admin can update someones profile', function (done) {
    agent
    .post('/api/profile/update')
    .set('authorization', 'Bearer ' + tokens[0])
    .field('rdbNum', "2")
    .field('firstName', 'adminTestBot')
    .field('lastName', 'lastname')
    .field('bio', 'bio')
    .field('email', 'throwaway@nomoss.co')
    .end((err, res) => {
      expect(err).to.be.null
      expect(res.body).to.have.property('user');
      done();
    });
  });

  it('user can update own profile', function (done) {
    agent
    .post('/api/profile/update')
    .set('authorization', 'Bearer ' + tokens[2])
    .field('rdbNum', "2")
    .field('firstName', 'TESTBOT2')
    .field('lastName', 'lastname')
    .field('bio', 'bio')
    .field('email', 'throwaway@nomoss.co')
    .end((err, res) => {
      expect(err).to.be.null
      expect(res.body).to.have.property('user');
      done();
    });
  });

  it('ensure user has been updated', async function (){
    user = await User.findOne({rdbNumber: 2})
    expect(user).to.have.property('firstName', 'TESTBOT2');
    expect(user).to.have.property('lastName', 'lastname');
    expect(user).to.have.property('combinedName', 'TESTBOT2 lastname');
    expect(user).to.have.property('bio', 'bio');
    expect(user).to.have.property('emailAddress', 'throwaway@nomoss.co');
  })

  it('user can\'t use an email that already has 2 emails associated', function (done) {
    expect(true).to.equal(true);
    done();
  });

})

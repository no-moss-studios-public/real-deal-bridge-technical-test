// test/integration/controllers/tournament.test.js
var supertest = require('supertest');
var expect = require('chai').expect;

var agent;
var bannedUser;

describe('ban', function() {
  // this.timeout(10000);

  before( async function() {
    agent = supertest.agent(sails.hooks.http.app);
  });

  it('non-admins cannot ban users', function (done) {
    agent
    .post('/api/profile/set-ban-status')
    .set('authorization', 'Bearer ' + tokens[1])
    .field('rdbNum', "2")
    .field('banStatus', true)
    .end((err, res) => {
      expect(err).to.be.null
      expect(res.body).to.have.property('code', 1006);
      done();
    });
  });

  it('admins cant ban users that dont exist', function (done) {
    agent
    .post('/api/profile/set-ban-status')
    .set('authorization', 'Bearer ' + tokens[0])
    .field('rdbNum', "21324513690139069061906390609639036")
    .field('banStatus', true)
    .end((err, res) => {
      expect(err).to.be.null
      expect(res.body).to.have.property('code', 1005);
      done();
    });
  });

  describe('ban flow', function (){
    before( async function() {
      await agent
      .post('/api/profile/report')
      .set('authorization', 'Bearer ' + tokens[1])
      .field('uniqueNumber', 2)
      .field('reportReason', 'Testing the ban')
    });

    it('admins can ban users', function (done) {
      agent
      .post('/api/profile/set-ban-status')
      .set('authorization', 'Bearer ' + tokens[0])
      .field('rdbNum', "2")
      .field('banStatus', true)
      .end((err, res) => {
        expect(err).to.be.null
        expect(res.body).to.have.property('code', 200);
        done();
      });
    });

    it('user should be banned, all reports with user should be gone', async function () {
      bannedUser = await User.findOne({rdbNumber: '2'});
      expect(bannedUser).to.not.be.null;
      expect(bannedUser).to.have.property('isBanned', true);
      var report = await Report.find({reportedUser: bannedUser.id, resolved: false})
      expect(report).to.not.be.null;
      expect(report).to.be.of.length(0)
    });

    it('banned user can\'t send any API requests', function (done) {
      agent
      .post('/api/auth/login')
      .send({ uniqueNum: '2', password: '2' })
      .end((err, res) => {
        expect(err).to.be.null
        expect(res.body).to.have.property('code', 1008);
        done();
      })
    });

    it('admins can unban users', function (done) {
      agent
      .post('/api/profile/set-ban-status')
      .set('authorization', 'Bearer ' + tokens[0])
      .field('rdbNum', "2")
      .field('banStatus', false)
      .end((err, res) => {
        expect(err).to.be.null
        expect(res.body).to.have.property('code', 200);
        done();
      });
    });

    it('user should be unbanned', async function () {
      var bannedUser = await User.findOne({rdbNumber: '2'});
      expect(bannedUser).to.not.be.null;
      expect(bannedUser).to.have.property('isBanned', false);
    });

    it('user can send API requests', function (done) {
      agent
      .post('/api/auth/login')
      .send({ uniqueNum: '2', password: '2' })
      .end((err, res) => {
        expect(err).to.be.null
        expect(res.body).to.have.property('user');
        done();
      });
    });

  })

  after(async function(){

  });
})

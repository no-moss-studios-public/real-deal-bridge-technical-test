var supertest = require('supertest');
var expect = require('chai').expect;

var agent;
var user;

describe('find', function() {
  before( async function() {
    agent = supertest.agent(sails.hooks.http.app);
  });

  it('Searching user with invalid string finds nothing', async function () {
    var res = await agent
    .get('/api/profile/find')
    .set('authorization', 'Bearer ' + tokens[2])
    .query({query: "2534y5eat63fawe5faw45f3w"})

    expect(res.body).to.have.property('users')
    expect(res.body.users).to.have.length(0);
  });

  it('Searching user with RDB finds them', async function () {
    var res = await agent
    .get('/api/profile/find')
    .set('authorization', 'Bearer ' + tokens[2])
    .query({query: "4"})

    expect(res.body).to.have.property('users')
    expect(res.body.users).to.have.length(1);
    expect(res.body.users[0]).to.have.property('rdbNumber', '4');
  });

  it('Searching user with firstname finds them', async function () {
    var res = await agent
    .get('/api/profile/find')
    .set('authorization', 'Bearer ' + tokens[2])
    .query({query: "TestBot9"})

    expect(res.body).to.have.property('users')
    expect(res.body.users).to.have.length(1);
    expect(res.body.users[0]).to.have.property('firstName', 'TestBot9');
  });

  it('Searching user with lastname finds them', async function () {
    var res = await agent
    .get('/api/profile/find')
    .set('authorization', 'Bearer ' + tokens[2])
    .query({query: "TestBot8"})

    expect(res.body).to.have.property('users')
    expect(res.body.users).to.have.length(1);
    expect(res.body.users[0]).to.have.property('lastName', 'TestBot8');
  });

  it('Searching user with combined name finds them', async function () {
    var res = await agent
    .get('/api/profile/find')
    .set('authorization', 'Bearer ' + tokens[2])
    .query({query: "TestBot9 TestBot"})

    expect(res.body).to.have.property('users')
    expect(res.body.users).to.have.length(1);
    expect(res.body.users[0]).to.have.property('combinedName', 'TestBot9 TestBot9');
  });

  it('Search is limited to 20', async function () {
    var res = await agent
    .get('/api/profile/find')
    .set('authorization', 'Bearer ' + tokens[2])
    .query({query: "Test"})

    expect(res.body).to.have.property('users')
    expect(res.body.users).to.have.length(20);
  });

})

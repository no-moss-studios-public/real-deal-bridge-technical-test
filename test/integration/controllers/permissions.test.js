// test/integration/controllers/auth.test.js
var supertest = require('supertest');
var expect = require('chai').expect;

var agent;

describe('permissions', function() {
  var targetUserRDB = 2;

  before(function(done) {
    agent = supertest.agent(sails.hooks.http.app);
    done();
  })

  it('admin should be able to make another admin', function (done) {
    agent
    .post('/api/profile/add-admin-permission')
    .set('authorization', 'Bearer ' + tokens[0])
    .send({
      rdbNum: targetUserRDB,
      adminPermission: 1,
    })
    .expect(200, done);
  });

  it('second admin should have admin permissions', async function () {
    var user = await User.findOne({rdbNumber: targetUserRDB}).populate('adminPermissions');

    expect(user).to.have.property('adminPermissions');
    expect(user.adminPermissions).to.have.property('length', 1);
    expect(user.adminPermissions[0]).to.have.property('permission', 1);

    var isAdmin = await sails.helpers.hasAdminPermission(targetUserRDB, 1);
    expect(isAdmin).to.equal(true);
  });


  it('first admin should be able to remove admin permissions', function (done) {
    agent
    .post('/api/profile/remove-admin-permission')
    .set('authorization', 'Bearer ' + tokens[0])
    .send({
      rdbNum: targetUserRDB,
      adminPermission: 1,
    })
    .expect(200, done);
  });

  it('second admin shouldn\'t have admin permissions', async function () {
    var user = await User.findOne({rdbNumber: targetUserRDB}).populate('adminPermissions');

    expect(user).to.have.property('adminPermissions');
    expect(user.adminPermissions).to.have.property('length', 0);

    var isAdmin = await sails.helpers.hasAdminPermission(targetUserRDB, 1);
    expect(isAdmin).to.equal(false);
  });

  after( function(done) {
    //Remove 2 newly registered users
    User.destroy({
      id: { in: [ 65, 66 ] }
    })
    .then( () => {
      return done();
    });
  });

});

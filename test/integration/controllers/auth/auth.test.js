// test/integration/controllers/auth.test.js
var supertest = require('supertest');
var expect = require('chai').expect;

var agent;

describe('auth', function() {
  before(function(done) {
    agent = supertest.agent(sails.hooks.http.app);
    done();
  })

  describe('POST /api/auth/register', function() {
    var pw;

    before(async function() {
      pw = await sails.helpers.passwords.hashPassword('test');
    })

    it('should create a user and return relevant keys in body', function (done) {
      agent
      .post('/api/auth/register')
      .send({
        emailAddress: 'mocha@coffee.com',
        firstName: 'Mocha',
        lastName: 'Test',
        overThirteen: true,
        acceptedPrivacy: true,
        acceptedTOS: true,
        rdbNumber: '999999a',
        password: pw,
        sendEmail: false,
      })
      .expect(hasAuthKeys)
      .expect(function(res){
        expect(res.body.user).to.have.property('firstName', 'Mocha')
        expect(res.body.user).to.have.property('lastName', 'Test')
        expect(res.body.user).to.have.property('combinedName', 'Mocha Test')
        expect(res.body.user).to.have.property('willSubstitute', true);
      })
      .expect(200, done);
    });

    it('should have set JWT as cookie', function (done) {
      agent
      .post('/api/auth/register')
      .send({
        emailAddress: 'mocha2@coffee.com',
        firstName: 'Mocha2',
        lastName: 'Test2',
        overThirteen: true,
        acceptedPrivacy: true,
        acceptedTOS: true,
        rdbNumber: '9999992b',
        password: pw,
        sendEmail: false,
      })
      .expect(function(res){
        expect(res.header).to.exist;
        expect(res.header).to.have.property('set-cookie');
        expect(res.header['set-cookie'].length === 2); //JWT and Sails Cookie MUST be present
      })
      .end(done);
    });


    it('should have joined ALL default clubs', function (done) {
      Club.find({default: true}).populate('members')
      .then((clubsWithUsers) =>{
        for(let c of clubsWithUsers){
          if(c.members.length !== 2){
            return done(new Error('A default club wasn\'t populated with new user ', + c));
          }
        }
        return done();
      })
    });

  });

  describe('GET /api/auth/logout', function() {

    it('should remove JWT cookie', function (done) {
      agent
      .get('/api/auth/logout')
      .expect(function(res){
        expect(res.header).to.exist;
        expect(res.header).to.have.property('set-cookie');
        expect(res.header['set-cookie'].length === 1); //Only Sails Cookie MUST be present, JWT removed
      })
      .end(done);
    });

  });

  describe('POST /api/auth/login', function() {

    it('should return relevant return keys in body', function (done) {
      agent
      .post('/api/auth/login')
      .send({ uniqueNum: '0', password: 'Vks*@W%4' })
      .expect(hasAuthKeys)
      .expect(200, done);
    });

    it('should have set JWT as cookie', function (done) {
      agent
      .post('/api/auth/login')
      .send({ uniqueNum: '0', password: 'Vks*@W%4' })
      .expect(function(res){
        // user-provided function can include Chai assertions
        expect(res.header).to.exist;
        expect(res.header).to.have.property('set-cookie');
        expect(res.header['set-cookie'].length === 2); //JWT and Sails Cookie MUST be present
      })
      .end(done);
    });

  });

  function hasAuthKeys(res) {
    // console.log(res);
    if (!('user' in res.body)) throw new Error("missing user key!");
    if (!('id' in res.body.user)) throw new Error("missing user.id key!");
    if (!('abfNumber' in res.body.user)) throw new Error("missing user.abfNumber key!");
    if (!('token' in res.body)) throw new Error("missing token key!");
  }

  after( function(done) {
    //Remove 2 newly registered users
    User.destroy({
      id: { in: [ 65, 66 ] }
    })
    .then( () => {
      return done();
    });
  });

});

var sails = require('sails');
var jwt = require('jsonwebtoken')
var fs = require('fs');
var moment = require('moment');
moment().format();

// Before running any tests...
before(function(done) {

  // Increase the Mocha timeout so that Sails has enough time to lift, even if you have a bunch of assets.
  this.timeout(100000);

  process.env.JWT_SECRET = 'test';

  //Create .env file
  if(!fs.existsSync('.env')){
    console.log("Adding .env file")
    fs.writeFileSync('.env', 'Hey there!');
  }

  sails.lift({
    // Your Sails app's configuration files will be loaded automatically,
    // but you can also specify any other special overrides here for testing purposes.

    // For example, we might want to skip the Grunt hook,
    // and disable all logs except errors and warnings:
    hooks: { grunt: false },
    log: { level: 'warn' },
    datastores: {
      default: {
        // adapter: 'sails-disk', //Default
        inMemoryOnly: true, //Data is stored in memory (not in a file or DB)
      }
    },
    models: {
      migrate: 'drop'
    }

  }, async function(err) {
    if (err) { return done(err); }

    // here you can load fixtures, etc.
    // (for example, you might want to create some records in the database)

    //Creating global variable tokens to use as authorization for tests
    if(sails.hooks.cron){
      sails.hooks.cron.jobs.createTournamentRounds.stop();
    }

    tokens = {}
    var allUsers = await User.find();
    for(let u of allUsers){
      tokens[u.rdbNumber] = jwt.sign({user: u}, process.env.JWT_SECRET, {expiresIn: sails.config.custom.jwtExpires});
    }

    await Live.create({status: true});
    await DataCall.create({weeklyExport: moment.utc().toISOString(), monthlyExport: moment.utc().toISOString()});

    console.log();
    console.log("######################################################");
    console.log("Startup Script has run - populated in-memory database");
    console.log();
    console.log("BEGIN TESTS")
    console.log("######################################################");
    console.log();
    return done();
  });
});

// After all tests have finished...
after(function(done) {

  // here you can clear fixtures, etc.
  // (e.g. you might want to destroy the records you created above)

  sails.lower(done);

});

/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: 'pages/homepage' },
  '/download': { view: 'pages/download'},


  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/

  //Cron endpoint
  'GET /api/cron':                              {action: 'cron'},

  //Authentication endpoints
  '/api/auth/logout':                           {action: 'auth/logout'},
  'POST /api/auth/register':                    {action: 'auth/register'},
  'POST /api/auth/login':                       {action: 'auth/login'},
  'POST /api/auth/forgot-password':             {action: 'auth/forgot-password'},
  'GET  /auth/reset-password':                  {view: 'pages/reset-password'},
  'POST /api/auth/reset-password':              {action: 'auth/reset-password'},

  //Sample endpoints
  'GET /public':                                {action: 'public'},
  'GET /private':                               {action: 'private'},

  //Profile endpoints
  'POST /api/profile/upload':                   {action: 'profile/upload'},
  'POST /api/profile/get':                      {action: 'profile/get'},
  'GET /api/profile/get-user':                  {action: 'profile/get-user'},
  'POST /api/profile/add-admin-permission':     {action: 'profile/add-admin-permission'},
  'POST /api/profile/remove-admin-permission':  {action: 'profile/remove-admin-permission'},
  'POST /api/profile/set-ban-status':           {action: 'profile/set-ban-status'},
  'POST /api/profile/update':                   {action: 'profile/update'},
  'POST /api/profile/report':                   {action: 'profile/report'},
  'POST /api/profile/personal-mute':            {action: 'profile/personal-mute'},
  'POST /api/profile/set-mute-status':          {action: 'profile/set-mute-status'},
  'GET /api/profile/find':                      {action: 'profile/find'},
  'POST /api/profile/set-substitute-status':    {action: 'profile/set-substitute-status'},

  //Admin endpoints
  'GET /api/admin/get-reports':                 {action: 'admin/get-reports'},
  'POST /api/admin/resolve-report':             {action: 'admin/resolve-report'},
  'POST /api/admin/export-clubs':               {action: 'admin/export-clubs'},

  //Club endpoints
  'POST /api/club/create':                      {action: 'club/create'},
  'GET /api/club/info':                         {action: 'club/info'},
  'GET /api/club/user':                         {action: 'club/user'},
  'POST /api/club/join':                        {action: 'club/join'},
  'POST /api/club/leave':                       {action: 'club/leave'},
  'POST /api/club/add-owner':                   {action: 'club/add-owner'},
  'POST /api/club/remove-owner':                {action: 'club/remove-owner'},
  'POST /api/club/remove-member':               {action: 'club/remove-member'},
  'POST /api/club/change-status':               {action: 'club/change-status'},
  'GET /api/club/search':                       {action: 'club/search'},
  'POST /api/club/approve-member':              {action: 'club/approve-member'},
  'POST /api/club/update':                      {action: 'club/update'},
  'GET /api/club/get-image':                    {action: 'club/get-image'},
  'GET /api/club/get-pending':                  {action: 'club/get-pending'},
  'GET /api/club/export-members':               {action: 'club/export-members'},
  'POST /api/club/set-mute-status':             {action: 'club/set-mute-status'},

};

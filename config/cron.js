const { sampleSize } = require("lodash");

module.exports.cron = {
  createTournamentRounds: {
    schedule: '*/60 * * * * *',
    onTick: async function () {
      //Call sails helper to complete etc. 
      console.log('You will see this every 60 seconds');
      await sails.helpers.roundDone();
    },
    start: false //true //Turned to false for AWS LAMBDA //turn this on but TURN OFF in test 
  },
};
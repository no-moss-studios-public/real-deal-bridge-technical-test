/**
 * HTTP Server Settings
 * (sails.config.http)
 *
 * Configuration for the underlying HTTP server in Sails.
 * (for additional recommended settings, see `config/env/production.js`)
 *
 * For more information on configuration, check out:
 * https://sailsjs.com/config/http
 */

const express = require('express');

module.exports.http = {

  /****************************************************************************
  *                                                                           *
  * Sails/Express middleware to run for every HTTP request.                   *
  * (Only applies to HTTP requests -- not virtual WebSocket requests.)        *
  *                                                                           *
  * https://sailsjs.com/documentation/concepts/middleware                     *
  *                                                                           *
  ****************************************************************************/

  middleware: {

    /***************************************************************************
    *                                                                          *
    * The order in which middleware should be run for HTTP requests.           *
    * (This Sails app's routes are handled by the "router" middleware below.)  *
    *                                                                          *
    ***************************************************************************/

     order: [
      'cookieParser',
      'session',
    //   'bodyParser',
      'unity',
      'compress',
      'poweredBy',
      'skipper',
      'router',
      'www',
      'favicon',
     ],


    /***************************************************************************
    *                                                                          *
    * The body parser that will handle incoming multipart HTTP requests.       *
    *                                                                          *
    * https://sailsjs.com/config/http#?customizing-the-body-parser             *
    *                                                                          *
    ***************************************************************************/

    // bodyParser: (function _configureBodyParser(){
    //   var skipper = require('skipper');
    //   var middlewareFn = skipper({ strict: true });
    //   return middlewareFn;
    // })(),

    //Configure Skipper
    skipper: require('skipper')({
      maxWaitTimeBeforePassingControlToApp: 1000,
    }),

    //Configure unity
    unity: (function (){
      console.log("Initializing Unity middleware");
      express.static.mime.define({'application/wasm': ['wasm']});

      return function(req, res, next){

        if(req.path.endsWith("unityweb") || req.path.endsWith("wasm")){
          res.set("Content-Encoding", "gzip");
          res.set("Cache-Control", "max-age=0, no-cache, must-revalidate");
          res.set("Large-Allocation", "0");
        }

        if(req.path.endsWith("zip")){
          res.set("Cache-Control", "max-age=0, no-cache, must-revalidate");
        }

        return next();
      };
    })(),

  },

};
